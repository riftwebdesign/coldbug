package com.riftwebdesign.coldbug.models;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.riftwebdesign.coldbug.controllers.WatchController;
import com.riftwebdesign.coldbug.pojo.interfaces.ivirtualmachine;
import com.riftwebdesign.coldbug.util.Helpers;
import com.sun.jdi.ThreadReference;


/**
 * Created by sid on 11/17/2017.
 */
public class Application {

    //todo public static variables, these can be injected otherwise the defaults can be used
    private static Application instance= null;
    private static Object mutex= new Object();
    private Gson gson = new Gson();

    public ArrayList<WatchController> filewatches = new ArrayList<WatchController>();
    public ProjectSettings[] settings = null;
    public Thread t = null;
    public ivirtualmachine vm = null;
    public int activesettingindex = -1;
    public ProjectSettings getActiveSettings() {
        if(this.activesettingindex == -1) return null;
        return this.settings[this.activesettingindex];
    }
    public String gethost(){
        ProjectSettings ps = getActiveSettings();
        if(ps == null) return null;
        return ps.host;
    }
    public Integer getport(){
        ProjectSettings ps = getActiveSettings();
        if(ps == null) return null;
        return ps.port;
    }
    public HashMap<String,String> config = new HashMap<String,String>(){
        private static final long serialVersionUID = 1L;
    {
        put("endpoint","https://getcoldbug.com/service");
        put("port","4567");
        put("log","false");
    }};
    public boolean loggedin = false;

	private Application(){
        //load config and overwrite config properties  
        Helpers helper = new Helpers();    
        String json = helper.readjson("environment");
        if (json.length() == 0) {
            json = "{}";
        }
        JsonObject obj = gson.fromJson(json, JsonObject.class);
        for(String key : this.config.keySet()){
            if( obj.has(key) ){
                config.put(key, obj.get(key).getAsString());
            }
        }  
        //System.out.println(getconfigprop("endpoint"));
    }
    
    public String getconfigprop(String key){
        if(config.containsKey(key)) return this.config.get(key);
        return "";
    }

	public static Application getInstance(){
		if(instance==null){
			synchronized (mutex){
				if(instance==null) instance = new Application();
			}
            instance = new Application();
		}
		return instance;
    }
    
    public static boolean IsConnected(){
        boolean connected = true;
        try{
            for(ThreadReference tr : Application.getInstance().vm.allThreads()){
                tr.isSuspended();
            }
        } catch(Exception e){
            connected = false;
            //Application.getInstance().vm = null;
        }
        return connected;
    }

}
