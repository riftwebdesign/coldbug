package com.riftwebdesign.coldbug.models;

import java.util.concurrent.TimeUnit;

import com.riftwebdesign.coldbug.controllers.EventController;
import com.riftwebdesign.coldbug.handlers.clientsidehandler;
import com.riftwebdesign.coldbug.pojo.event;
import com.riftwebdesign.coldbug.pojo.interfaces.ievent;
import com.riftwebdesign.coldbug.pojo.interfaces.ieventset;
import com.riftwebdesign.coldbug.util.Helpers;
import com.riftwebdesign.coldbug.util.Enums;
import com.sun.jdi.Location;
import com.sun.jdi.event.*;
import com.sun.jdi.event.Event;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.event.LocatableEvent;

import org.apache.bcel.generic.LNEG;

	/*
		EventQueue 
			sends event to a new thread for processing
			threads are stored in array, which can be killed
				see if its possible to process 1 thread at a time?
			
		Thread object needs:
			Event
			LastThread
			IsBreak
			LastSourcePath
			LastSourceLine
			LastPC
			LastThread
			UpdateVars thread
	*/
	public class EventThreadHandler extends Thread {
          public Event event = null;
          public ThreadReference threadref = null; 
          public ieventset es = null;
          public EventHandlerInfo eventinfo = new EventHandlerInfo();
          public boolean interrupt = false;
          public boolean isWorking = true;
          public boolean wasBreak = false;
          public boolean wasBreakRequest = false;

        public EventThreadHandler(ieventset e){
            this.es = e;
        }

        public void Cleanup(){            
            Helpers.log("Stopping UI thread","cleanup");
            eventinfo.UpdateUIThread = null;
        }

        @Override
        public void run() {            

            if(this.interrupt) {
                this.Cleanup();
                return;
               }	

               int lastlinenum = -100;
               String lastsourcename = "";

			try{
				for (ievent event : this.es.getEvents()) {
					this.event = ((event)event).event;		//todo use interface	

					if(this.event instanceof LocatableEvent) this.threadref = ((LocatableEvent)this.event).thread();
								
					if (this.event instanceof StepEvent || this.event instanceof BreakpointEvent) {	
                              this.wasBreakRequest = true;
                              EventController.currentbreakthread = this;	
                              LocatableEvent le = ((LocatableEvent)this.event);
                              Location location = le.location();
                              String refname = location.declaringType().name();		
                              int linenum = location.lineNumber();
                              String sourcename = location.sourceName();

                              if(lastlinenum == linenum && sourcename == lastsourcename){
                                   continue;
                              }
                              lastlinenum = linenum;
                              lastsourcename = sourcename;

						Helpers.log( "BREAKSTART: " + refname + "  (" + (this.threadref == null ? "" : this.threadref.name()) + ")" );
                              this.wasBreak = EventController.handleGenericEvent(this);

                              /*if(!wasBreak) {
                                   Helpers.log( "BREAK RESULT: " + wasBreak );
                                   this.eventinfo.OpFlag = Enums.NextOperation.None;
                              }*/

						while(this.eventinfo.Break)
						{
							synchronized(this) {
                                        this.wait(1000);
                                        this.eventinfo.PlayTimer.reset(); //keep reseting our timer during a break because we are still waiting
							}
						}						
                              this.eventinfo.PlayTimer.reset();
                              Helpers.log("BREAKEND");
					} else if (this.event instanceof ClassPrepareEvent) {
						Helpers.log( "PREPARING: " + ((ClassPrepareEvent)this.event).referenceType().name() + "  (" + (this.threadref == null ? "" : this.threadref.name()) + ")" );
						//EventController.vmtypes.add(((ClassPrepareEvent)this.event).referenceType()); //= Application.getInstance().vm.allClasses();
						// Application.getInstance().vm.allClasses().forEach((c) -> { 
						// 	if( c.name().contains("login") ) System.out.println( c.name() );
						// });
						clientsidehandler c = new clientsidehandler();
						c.registersteps_w_clean(false);	
					} else if (this.event instanceof VMStartEvent) {
						//System.out.println("VMSTARTEVENT");

					} else if (this.event instanceof ThreadStartEvent) {

					} else if (this.event instanceof ExceptionEvent) {
						Location l = ((ExceptionEvent)event).catchLocation();	
						Helpers.log("VMException: " + Integer.toString(l.lineNumber()) + " " + l.sourceName(),"eventthreadhandler.run");							
					}																				
				}
			} catch (Exception e){
				Helpers.log(e,"EventThreadHandler");
			}
			EventController.currentbreakthread = null;
			this.es.resume();	               

               if(!this.wasBreakRequest || this.wasBreak) this.isWorking = false;

			/*
			if (JavaBridge.PlayTimer.time(TimeUnit.SECONDS) >= 3){
				CFObjectGenerated = false;
				JavaBridge.OpFlag = Enums.NextOperation.None;
				JavaBridge.PlayTimer.reset();
				timer.reset();
				JavaBridge.LastSourceLine = -1;
				JavaBridge.LastSourcePath = "";
				EventController.REQUESTOBJECTSHOULDEXIST = false;
				EventController.SETTIMEOUT = true;
				skipfiles = new ArrayList<String>();
			}

			if(old_op == null || old_op != JavaBridge.OpFlag){							
				old_op = JavaBridge.OpFlag;
				if(JavaBridge.OpFlag == Enums.NextOperation.None) Helpers.log("OpCode NONE", "");
				else if(JavaBridge.OpFlag == Enums.NextOperation.Play) Helpers.log("OpCode PLAY", "");
				else if(JavaBridge.OpFlag == Enums.NextOperation.StepInto) Helpers.log("OpCode STEP INTO", "");
				else if(JavaBridge.OpFlag == Enums.NextOperation.StepOut) Helpers.log("OpCode STEP OUT", "");
				else if(JavaBridge.OpFlag == Enums.NextOperation.StepOver) Helpers.log("OpCode STEP OVER", "");
			}						
			*/           
        }

    }