package com.riftwebdesign.coldbug.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DirTree {
    public String text = null;
    public String icon = null;
    public ArrayList<DirTree> children = new ArrayList<DirTree>();
    public Map<String, String> a_attr = new HashMap<String,String>();
    public boolean isdirectory = false;
}
