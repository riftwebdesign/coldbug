package com.riftwebdesign.coldbug.models;

import com.sun.jdi.ObjectReference;
import com.sun.jdi.Value;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class KVPFillable{
    public HashMap<String,CustomValue> kvp = new HashMap<String,CustomValue>();
    public ArrayList<Value> keys = new ArrayList<Value>();
    public ObjectReference object = null;
    public String value = null;
}