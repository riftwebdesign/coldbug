package com.riftwebdesign.coldbug.models;

import com.riftwebdesign.coldbug.util.*;
import com.google.gson.Gson;
import com.sun.jdi.ThreadReference;

public class EventHandlerInfo {

    public Gson gson = new Gson();
    public boolean Break = false;
    public Enums.NextOperation OpFlag = Enums.NextOperation.None;
    public String LastSourcePath = "";
    public int LastSourceLine = 0;
    public Thread UpdateUIThread = null;
    public String activedump = null;
    public PageContext lastpc = null;
    public ThreadReference lastthread = null;
    public TimeHelper PlayTimer = TimeHelper.start();


	public EventHandlerInfo(){

	}
}
