package com.riftwebdesign.coldbug.models;

import com.riftwebdesign.coldbug.util.Enums;
import com.sun.jdi.Location;
import com.sun.jdi.ObjectReference;

import java.util.ArrayList;


public class PageContext {
    public ObjectReference pc;
    public ArrayList<Scope> scopes = new ArrayList<>();
    public ArrayList<WatchKVP> watches = new ArrayList<>();
    public Location location;

    public PageContext(ObjectReference or){
        this.pc = or;
        //this.scopes.add(new Scope(Enums.ScopeType.UndefinedScope, "undefinedScope"));
        this.scopes.add(new Scope(Enums.ScopeType.Local, "localScope"));
        this.scopes.add(new Scope(Enums.ScopeType.Variables, "variablesScope"));
        this.scopes.add(new Scope(Enums.ScopeType.URL, "urlScope"));
        this.scopes.add(new Scope(Enums.ScopeType.Form, "formScope"));
        this.scopes.add(new Scope(Enums.ScopeType.CGI, "cgiScope"));
        this.scopes.add(new Scope(Enums.ScopeType.Application, "applicationScope"));
        this.scopes.add(new Scope(Enums.ScopeType.Arguments, "argumentsScope"));
        this.scopes.add(new Scope(Enums.ScopeType.Session, "sessionScope"));
        this.scopes.add(new Scope(Enums.ScopeType.Client, "clientScope"));
        this.scopes.add(new Scope(Enums.ScopeType.Server, "serverScope"));
        this.scopes.add(new Scope(Enums.ScopeType.Cookie, "cookieScope"));
        this.scopes.add(new Scope(Enums.ScopeType.Request, "requestScope"));
        this.scopes.add(new Scope(Enums.ScopeType.This, "thisScope"));
        this.scopes.add(new Scope(Enums.ScopeType.Caller, "callerScope"));
        this.scopes.add(new Scope(Enums.ScopeType.Thread, "threadScope"));
        this.scopes.add(new Scope(Enums.ScopeType.Attributes, "attributeScope"));
        this.scopes.add(new Scope(Enums.ScopeType.Cluster, "clusterScope"));
    }
}
