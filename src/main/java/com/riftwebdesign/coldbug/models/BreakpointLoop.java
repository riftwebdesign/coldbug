package com.riftwebdesign.coldbug.models;

import com.sun.jdi.ReferenceType;

import java.util.ArrayList;

public class BreakpointLoop {

    public ArrayList<BreakpointLoopItem> items = new ArrayList<>();

    public class BreakpointLoopItem {

        public ReferenceType type = null;
        public String path = null;
        public ArrayList<Integer> lines = new ArrayList<>();

    }

    public static boolean hasLine(BreakpointLoopItem item, int line){
        boolean r = false;
        for(int i : item.lines){
            if(i == line) {
                r = true;
                break;
            }
        }
        return r;
    }

    public BreakpointLoopItem addItem(ReferenceType t, String path, ArrayList<Integer> lines){
        BreakpointLoopItem i = new BreakpointLoopItem();
        i.path = path;
        i.lines = lines;
        i.type = t;

        this.items.add(i);
        return i;
    }

    public boolean hasItem(ReferenceType t, String path){
          boolean r = false;

          for(BreakpointLoopItem bli : this.items){
               if(bli.path.equals(path )&& bli.type.name().equals(t.name())){
                    r = true;
                    break;
               }
          }

          return r;
     }

    public boolean hasItem(String path){
        boolean r = false;

        for(BreakpointLoopItem bli : this.items){
            if(bli.path == path){
                r = true;
                break;
            }
        }

        return r;
    }

    public boolean hasItem(ReferenceType type){
        boolean r = false;

        for(BreakpointLoopItem bli : this.items){
            if(bli.type == type){
                r = true;
                break;
            }
        }

        return r;
    }

    public BreakpointLoopItem getItem(ReferenceType type){
        BreakpointLoopItem r = null;

        for(BreakpointLoopItem bli : this.items){
            if(bli.type == type){
                r = bli;
                break;
            }
        }

        return r;
    }

    public BreakpointLoopItem getItem(String path){
        BreakpointLoopItem r = null;

        for(BreakpointLoopItem bli : this.items){
            if(bli.path == path){
                r = bli;
                break;
            }
        }

        return r;
    }

}
