package com.riftwebdesign.coldbug.models;

import java.util.ArrayList;
import java.util.Arrays;

import com.riftwebdesign.coldbug.util.Helpers;

/**
 * Created by sid on 9/22/2017.
 */
public class ProjectSettings {
    public String name = null;
    public Integer port = null;
    public String host = null;
    public Integer suspend = 1;
    public Integer stoponchange = 1; 
    public Breakpoints[] breakpoints = new Breakpoints[]{};
    public DirectoryMap[] directorymap = new DirectoryMap[]{};
    public WatchExclusion[] watchexclusions = new WatchExclusion[]{};
    public OpenFile[] openfiles = new OpenFile[]{};    
    public OpenTree[] opentree = new OpenTree[]{}; 
    public ArrayList<String> excludescopes = new ArrayList<>();
    public ArrayList<WatchVar> watchvars = new ArrayList<>();

    public boolean hasbp(String path, int line){
        for (Breakpoints b : breakpoints) {

            //String sourceName = Helpers.removeroots(Helpers.normalize2(b.path), this, false);
            //if(path.equals(sourceName) && b.line == line){
          if(path.equals(b.path) && b.line == line){
                return true;
            }
        }
        return false;
    }
    public boolean hasfile(String path){
        for (Breakpoints b : breakpoints) {
            String sourceName = Helpers.removeroots(Helpers.normalize2(b.path), this, false);
            if(path.equals(sourceName)){
                return true;
            }
        }
        return false;
    }
    public boolean removeopenfile(String path){
        ArrayList<OpenFile> ao = new ArrayList<>(Arrays.asList(openfiles));

        int i = -1;
        for(OpenFile a : ao){
            if(a.path.equals(path)){
                i = ao.indexOf(a);
                break;
            }
        }
        if(i > -1) {
            ao.remove(i);
            openfiles = ao.toArray(new OpenFile[ao.size()]);
            return true;
        }
        return false;
    }
}
