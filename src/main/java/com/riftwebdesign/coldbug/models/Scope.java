package com.riftwebdesign.coldbug.models;

import com.riftwebdesign.coldbug.util.Enums;

/**
 * Created by sid on 11/17/2017.
 */
public class Scope extends KVPFillable {
    public Enums.ScopeType type = Enums.ScopeType.None;
    public String function = "";
    public Scope(Enums.ScopeType st, String func){
        this.type = st;
        this.function = func;
    }
}