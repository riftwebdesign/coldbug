package com.riftwebdesign.coldbug.models;

import com.riftwebdesign.coldbug.util.Enums;

/**
 * Created by sid on 11/26/2017.
 */
public class Event {
    public String message = null;
    public Enums.EventLevel eventlevel = Enums.EventLevel.success;
}
