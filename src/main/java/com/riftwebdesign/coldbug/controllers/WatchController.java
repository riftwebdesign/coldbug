package com.riftwebdesign.coldbug.controllers;

import java.nio.file.*;
import static java.nio.file.StandardWatchEventKinds.*;
import static java.nio.file.LinkOption.*;
import java.nio.file.attribute.*;
import java.io.*;
import java.util.*;

import com.google.gson.Gson;
import com.riftwebdesign.coldbug.engines.factory;
import com.riftwebdesign.coldbug.handlers.clientsidehandler;
import com.riftwebdesign.coldbug.models.Application;
import com.riftwebdesign.coldbug.models.DirectoryMap;
import com.riftwebdesign.coldbug.models.WatchExclusion;
import com.riftwebdesign.coldbug.util.Helpers;
import com.riftwebdesign.coldbug.pojo.*;


public class WatchController extends Thread {
    public final WatchService watcher;
    private final Map<WatchKey,Path> keys;
    private final boolean recursive;
    private boolean trace = false;
    private Gson gson = new Gson();
    private factory factory = new factory();

    @SuppressWarnings("unchecked")
    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>)event;
    }

    public void stop_watching() throws Exception {
        for(WatchController t : Application.getInstance().filewatches){
            try{
                t.watcher.close();
            } catch(java.nio.file.ClosedWatchServiceException e){

            }
            t.interrupt();
        }
        Application.getInstance().filewatches = new ArrayList<WatchController>(); 
    }

    public void start_watching() throws Exception {
        for(DirectoryMap dm : Application.getInstance().getActiveSettings().directorymap){
            Path dir = Paths.get(dm.root);
            WatchController t =  new WatchController(dir, true);
            Application.getInstance().filewatches.add(t);
            Helpers.setTimeout(t, 1000); 
        }
    }

    /**
     * Register the given directory with the WatchService
     */
    private void register(Path dir) throws IOException {
        WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        if (trace) {
            Path prev = keys.get(key);
            if (prev == null) {
                //System.out.format("register: %s\n", dir);
            } else {
                if (!dir.equals(prev)) {
                    //System.out.format("update: %s -> %s\n", prev, dir);
                }
            }
        }
        keys.put(key, dir);
    }

    /**
     * Register the given directory, and all its sub-directories, with the
     * WatchService.
     */
    private void registerAll(final Path start) throws IOException {
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                throws IOException
            {
                //we dont want to watch web-inf folders
                if(dir.toString().toLowerCase().contains("web-inf") == false) {
                    register(dir);
                    return FileVisitResult.CONTINUE;
                }
                return FileVisitResult.SKIP_SUBTREE;
            }
        });
    }

    /**
     * Creates a WatchService and registers the given directory
     */
    public WatchController(Path dir, boolean recursive) throws IOException {
        this.watcher = FileSystems.getDefault().newWatchService();
        this.keys = new HashMap<WatchKey,Path>();
        this.recursive = recursive;

        if (recursive) {
            System.out.format("Scanning %s ...\n", dir);
            registerAll(dir);
            //System.out.println("Done.");
        } else {
            register(dir);
        }

        // enable trace after initial registration
        this.trace = true;
    }
    public WatchController() {
        this.watcher = null;
        this.keys = null;
        this.recursive = false;
    }

    public void run(){
        while(!Thread.interrupted()){
            processEvents(); 
        }
    }

    /**
     * Process all events for keys queued to the watcher
     */
    void processEvents() {
        // wait for key to be signalled
        WatchKey key;
        try {
            key = watcher.take();
        } catch (InterruptedException x) {
            return;
        }

        if(Thread.interrupted()) return;

        Path dir = keys.get(key);
        if (dir == null) {
            System.err.println("WatchKey not recognized!!");
            return;
        }        

        for (WatchEvent<?> event: key.pollEvents()) {
            WatchEvent.Kind<?> kind = event.kind();

            // TBD - provide example of how OVERFLOW event is handled
            if (kind == OVERFLOW) {
                continue;
            }

            // Context for directory entry event is the file name of entry
            WatchEvent<Path> ev = cast(event);
            Path name = ev.context();
            Path child = dir.resolve(name);

            //get active settings and loop watch exclusions
            //loop exclusions to make sure child does not container the exclusion
            try{
                boolean isExcluded = false;
                for(WatchExclusion w : Application.getInstance().getActiveSettings().watchexclusions){
                    if(child.toString().contains(w.path) ){
                        isExcluded = true;
                    }
                }
                if(isExcluded){
                    continue;
                }
            }catch(Exception e){
                continue;
            }

            Helpers.log_event("File change: " + child,2);
            clientsidehandler cc = new clientsidehandler();

            if( Application.IsConnected() && Application.getInstance().getActiveSettings().stoponchange == 1 ){
                Helpers.log_event("Forcing stop from change",2);
                cc.stop_vm();
            }

            SocketController sc = new SocketController();
            socketresponse sr = new socketresponse();
            Helpers.log("FILES CHANGED - " + child,"filewatcher");
            filechangeresponse fcr = factory.createfilechangeresponse();        
            sr = new socketresponse();
            
            fcr.tree = cc.getfullprojecttree();
            fcr.folder = dir.toString();
            sr.callback = "updateprojecttree_folder";
            sr.json = gson.toJson(fcr);
            sc.send(sr);

            // print out event
            //System.out.format("%s: %s\n", event.kind().name(), child);

            // if directory is created, and watching recursively, then
            // register it and its sub-directories
            if (recursive && (kind == ENTRY_CREATE)) {
                try {
                    if (Files.isDirectory(child, NOFOLLOW_LINKS)) {
                        registerAll(child);
                    }
                } catch (IOException x) {
                    // ignore to keep sample readbale
                }
            }
        }

        // reset key and remove from set if directory no longer accessible
        boolean valid = key.reset();
        if (!valid) {
            keys.remove(key);

            // all directories are inaccessible
            if (keys.isEmpty()) {
                return;
            }
        }
    }
}