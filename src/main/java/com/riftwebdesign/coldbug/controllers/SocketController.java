package com.riftwebdesign.coldbug.controllers;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.riftwebdesign.coldbug.models.Application;
import com.riftwebdesign.coldbug.pojo.*;
import com.riftwebdesign.coldbug.util.Helpers;

import org.eclipse.jetty.websocket.api.CloseStatus;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WriteCallback;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

/**
 * Created by sid on 11/17/2017.
 */
@WebSocket(maxIdleTime=0)
public class SocketController {

    public Application app = Application.getInstance();
    private Gson gson = new Gson();
    private static boolean wasstarted = false;
    public static volatile Session user = null;
    public static boolean lastconnected = false;
    private static volatile boolean cansend = true;
    public com.riftwebdesign.coldbug.engines.factory factory = new com.riftwebdesign.coldbug.engines.factory();
    private static volatile ArrayList<socketresponse> queue = new ArrayList<socketresponse>();
    private final Object nameMapLock = new Object();
    private WriteCallback callback = new WriteCallback() {
        @Override
        public void writeSuccess() {
            cansend = true;
            processnext();
        }

        @Override
        public void writeFailed(Throwable error) {
            cansend = true;            
            Helpers.log("SOCKET - message undelivered","writefailed");
            processnext();
        }
    };

	public SocketController(){

    }

    public void send(socketresponse sr){
        try {
            if(sr.json == null){
                sr.json = "{}";
            }
            queue.add(sr);
            processnext();
		} catch (Exception e) {
			Helpers.log(e, "SocketController.send");
		}
    }

    public void processnext(){
        try {
            synchronized(nameMapLock){
                if(cansend && queue.size() > 0) {
                    cansend = false;
                    socketresponse sr = queue.get(0);
                    queue.remove(0);
                    if(sr == null) {
                        cansend = true;
                        return;
                    }
                    if(SocketController.user == null) {
                        cansend = true;
                        return;
                    }
                    SocketController.user.getRemote().sendString(gson.toJson(sr),this.callback);                    
                }
            }
		} catch (Exception e) {            
			Helpers.log(e, "SocketController.processnext");
		}
    }

    public void serverstatemanager(){
        socketresponse sr = new socketresponse();

        //connection state
        sr.callback = "vmconnected";
        connectresponse cr = this.factory.createconnectresponse();
        cr.statechange = Application.IsConnected() != lastconnected;
        lastconnected = Application.IsConnected();
        cr.connected = Application.IsConnected();        
        sr.json = gson.toJson(cr);
        send(sr);

        Helpers.setTimeout(() -> {
            serverstatemanager();
        }, 3000);                
    }
    
    @OnWebSocketConnect
    public void onConnect(Session user) throws Exception {

        if(SocketController.user != null){
            socketresponse sr = new socketresponse();
            sr.callback = "kill";
            sr.json = "{}";
            user.getRemote().sendString(gson.toJson(sr), null);
            user.close(new CloseStatus(120, "multiconnect"));
            return;        
        }

        queue = new ArrayList<socketresponse>();
        SocketController.user = user; 
        SocketController.user.setIdleTimeout(0);    
        Helpers.log("SOCKET - user connected");
        if(!wasstarted) serverstatemanager(); 
    }

    @OnWebSocketClose
    public void onClose(Session user, int statusCode, String reason) {
        if(statusCode == 120) return;

        SocketController.user.close();
        SocketController.user = null;
        Helpers.log("SOCKET - user lost");
    }

    @OnWebSocketMessage
    public void onMessage(Session user, String message) {
        //switch
    }

    @OnWebSocketError
    public void throwError(Throwable e) {
        if(e.getClass().toString().contains("ProtocolException")) return;
        e.printStackTrace();
        Helpers.log("^^^^^", "SocketController.throwerror");
    }

}
