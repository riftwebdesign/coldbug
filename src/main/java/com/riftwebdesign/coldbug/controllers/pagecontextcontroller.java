package com.riftwebdesign.coldbug.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.riftwebdesign.coldbug.handlers.clientsidehandler;
import com.riftwebdesign.coldbug.handlers.heartbeathandler;
import com.riftwebdesign.coldbug.models.Application;
import com.riftwebdesign.coldbug.models.CustomValue;
import com.riftwebdesign.coldbug.models.EventThreadHandler;
import com.riftwebdesign.coldbug.models.KVPFillable;
import com.riftwebdesign.coldbug.models.PageContext;
import com.riftwebdesign.coldbug.models.Scope;
import com.riftwebdesign.coldbug.models.WatchKVP;
import com.riftwebdesign.coldbug.models.WatchVar;
import com.riftwebdesign.coldbug.pojo.pagecontextmonitor;
import com.riftwebdesign.coldbug.util.Enums;
import com.riftwebdesign.coldbug.util.Helpers;
import com.sun.jdi.InvocationException;
import com.sun.jdi.Location;
import com.sun.jdi.Method;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.StringReference;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.Value;
import com.sun.tools.jdi.ArrayReferenceImpl;
import com.sun.tools.jdi.StringReferenceImpl; 

/**
 * Created by sid on 11/17/2017.
 */
public class pagecontextcontroller {

    public volatile int threadcompletecount = 0;
    public heartbeathandler pcm = new heartbeathandler();

    public Scope getScope(PageContext pc, Enums.ScopeType type){
        Scope r = null;
        for(Scope s : pc.scopes){
            if(s.type == type ){
                r = s;
                break;
            }
        }
        return r;
    }

    public WatchKVP getWatch(PageContext pc, String name){
        WatchKVP r = null;
        for(WatchKVP s : pc.watches){
            if(s.name.equals(name)){
                r = s;
                break;
            }
        }
        return r;
    }

    public void setAll(PageContext pc, ThreadReference thread){
        pagecontextcontroller pcc = new pagecontextcontroller();        

        //todo change to loop
        pcc.setScope(pc,Enums.ScopeType.Application, thread);
        pcc.setScope(pc,Enums.ScopeType.Arguments, thread);
        pcc.setScope(pc,Enums.ScopeType.Attributes, thread);
        pcc.setScope(pc,Enums.ScopeType.Caller, thread);
        pcc.setScope(pc,Enums.ScopeType.Cluster, thread);
        pcc.setScope(pc,Enums.ScopeType.Variables, thread);
        pcc.setScope(pc,Enums.ScopeType.URL, thread);
        pcc.setScope(pc,Enums.ScopeType.Form, thread);
        pcc.setScope(pc,Enums.ScopeType.CGI, thread);  
        pcc.setScope(pc,Enums.ScopeType.Local, thread);
        pcc.setScope(pc,Enums.ScopeType.Session, thread);
        pcc.setScope(pc,Enums.ScopeType.Client, thread);
        pcc.setScope(pc,Enums.ScopeType.Server, thread);
        pcc.setScope(pc,Enums.ScopeType.Cookie, thread);
        pcc.setScope(pc,Enums.ScopeType.Request, thread);
        pcc.setScope(pc,Enums.ScopeType.This, thread);
        pcc.setScope(pc,Enums.ScopeType.Thread, thread);

        clientsidehandler c = new clientsidehandler();
        c.UpdateVariablesBar(pc);
    }

    public void setScope(PageContext pc, Enums.ScopeType type, ThreadReference thread){
        try {
            Scope s = this.getScope(pc, type);
            //List<Method> ms = pc.pc.referenceType().methods();
            Method m = pc.pc.referenceType().methodsByName(s.function).get(0);
            s.object = (ObjectReference) pc.pc.invokeMethod(thread, m, new ArrayList<>(), ObjectReference.INVOKE_SINGLE_THREADED);
            this.getKeys(pc, s, thread, 0);
        }catch (Exception e){
            Helpers.log( e.toString(), "PageContextController.setScope" );
        }
    }

    public void getKVP(PageContext pc, KVPFillable fillable, ThreadReference thread) {
        try {
            fillable.kvp = new HashMap<>();
            for(Value val : fillable.keys) {
                pagecontextcontroller pcc = this;
                pcc.fillKey(pc,fillable,thread,val);
            }
        }catch (Exception e){
            Helpers.log(e.toString(), "PageContextController.getKVP");
        }
    }

    public void fillKey(PageContext pc, KVPFillable fillable, ThreadReference thread, Value val) {
        try{
            String key = ((StringReferenceImpl) val).value();
            ArrayList<Value> args = new ArrayList<Value>();
            args.add(fillable.object);
            args.add(val);
            //List<Method> kvms = fillable.object.referenceType().methods();
            Method kvm = pc.pc.referenceType().methodsByName("get").get(0);
            Value kvd = pc.pc.invokeMethod(thread, kvm, args, ObjectReference.INVOKE_SINGLE_THREADED);
            if (kvd != null) {
                CustomValue cv = new CustomValue();
                cv.object = ((ObjectReference) kvd);
                if (kvd instanceof StringReference) {
                    cv.type = Enums.CustomValType.String;
                    cv.value = ((StringReferenceImpl) kvd).value().toString();
                } else if (kvd instanceof ObjectReference) {
                    cv.type = Enums.CustomValType.Object;
                    List<Method> keycheck = cv.object.referenceType().methodsByName("keys");
                    List<Method> stringcheck = cv.object.referenceType().methodsByName("toString");
                    if (keycheck.size() > 1) {
                        cv.type = Enums.CustomValType.CFObject;
                        pagecontextcontroller pcc = this;
                        pcc.getKeys(pc, cv, thread, 1);
                       /* Thread t = new Thread() { public void run() { pcc.getKeys(pc, cv, thread, 1); } };
                        t.start();
                        t.join();*/
                    } else if (stringcheck.size() > 0) {
                        Integer usei = 0;
                        if (stringcheck.size() > 1) usei = 1;
                        cv.type = Enums.CustomValType.String;
                        Method m = stringcheck.get(usei);
                        StringReference temps = (StringReference) cv.object.invokeMethod(thread, m, new ArrayList<>(), ObjectReference.INVOKE_SINGLE_THREADED);
                        cv.value = temps.value();
                    }
                }

                fillable.kvp.put(key, cv);
            }
        }catch (Exception e){
            Helpers.log(e.toString(), "PageContextController.getKVP");
        }
    }

    public void getKeys(PageContext pc, KVPFillable fillable, ThreadReference thread, Integer index){
        try {
            ObjectReference or = fillable.object;
            //List<Method> ms = or.referenceType().methods();
            Method m = or.referenceType().methodsByName("keys").get(index);
            ObjectReference d = (ObjectReference) or.invokeMethod(thread, m, new ArrayList<>(), ObjectReference.INVOKE_SINGLE_THREADED);
            ArrayReferenceImpl a = Helpers.castObject(ArrayReferenceImpl.class,d);
            List<Value> vals = Helpers.castList(Value.class, a.getValues());
            fillable.keys.clear();
            for (Value v : vals) {
                pagecontextcontroller pcc = this;
                pcc.keytostring(fillable,thread,v);
                /*t.start();
                t.join();*/
            }
            getKVP(pc, fillable, thread);
        }catch (Exception e){
            Helpers.log(e.toString(), "PageContextController.getKeys");
        }
    }

    public void keytostring(KVPFillable fillable, ThreadReference thread, Value v){
        try {
            ObjectReference r = ((ObjectReference) v);
            //List<Method> vms = r.referenceType().methods();
            Method vm = r.referenceType().methodsByName("toString").get(0);
            Value vd = r.invokeMethod(thread, vm, new ArrayList<>(), ObjectReference.INVOKE_SINGLE_THREADED);
            if (((StringReferenceImpl) vd).value() != null) {
                fillable.keys.add(vd);
            }
        }catch (Exception e){
            Helpers.log(e.toString(), "PageContextController.keytostring");
        }
    }

    /*
    public String getClientObj(){
        String val = "setvariable(\"request.ColdBugObject\",createObject(\"component\",\"coldbug.ColdbugObject\"))";
        return val;
    }

    public String setEval(String val){
        String r = " request.ColdBugObject.eval('"+val+"') ";
        return r;
    }


    public String setDump(String val){
        String r = "request.ColdBugObject.dump('"+val+"')";
        return r;
    }

    public String setSimpleEval(String val){
        String r = "request.ColdBugObject.simpleeval('"+val+"')";
        return r;
    }

    public void makeObject(PageContext pc, ThreadReference thread){
        if(!EventController.REQUESTOBJECTSHOULDEXIST){
            try {
                evaluate(filldump(getClientObj()),pc,thread, true); 
                EventController.REQUESTOBJECTSHOULDEXIST = true;              
            }catch(Exception e){
                Helpers.log( e.toString(), "ERROR LOADING CLIENT SIDE OBJECT" );
            }
        }
    }
    
    public String filldump( String val){
        return "getpagecontext().getConfig().getDumpWriter(\"html\",createObject(\"java\",\"lucee.runtime.dump.HTMLDumpWriter\").DEFAULT_RICH).toString(getpagecontext(),createObject(\"java\",\"lucee.runtime.dump.DumpUtil\").toDumpData(JavaCast(\"Object\","+ val +"),getpagecontext(),20,createObject(\"java\",\"lucee.runtime.dump.DumpProperties\").DEFAULT),true)";
    }

    public ObjectReference simpleeval(String val, PageContext pc, ThreadReference thread){
        ObjectReference r = null;
        try {
            Method m = pc.pc.referenceType().methodsByName("evaluate").get(0);
            ArrayList<Value> args = new ArrayList<Value>();
            args.add(Application.getInstance().vm.mirrorOf(setSimpleEval(val)));            
            r = (ObjectReference) pc.pc.invokeMethod(thread, m, args, ObjectReference.INVOKE_SINGLE_THREADED);
        }catch(java.lang.IllegalMonitorStateException | com.sun.jdi.IncompatibleThreadStateException te){
            try{ wait(1000); } catch(Exception c){};
            r = simpleeval(val,pc,thread);
        }catch(Exception e){
            Helpers.log( e.toString() + "   " + thread.status() + "          Simple EVAL:" + (setSimpleEval(val)), "PageContextController.simpleeval" );
        }

        return r;
    }

    public ObjectReference evaluate(String val, PageContext pc, ThreadReference thread, boolean raweval){
        ObjectReference r = null;
        try {
            Method m = pc.pc.referenceType().methodsByName("evaluate").get(0);
            ArrayList<Value> args = new ArrayList<Value>();
            args.add(Application.getInstance().vm.mirrorOf(raweval ? val : setEval(val)));            
            r = (ObjectReference) pc.pc.invokeMethod(thread, m, args, ObjectReference.INVOKE_SINGLE_THREADED);
        }catch(java.lang.IllegalMonitorStateException | com.sun.jdi.IncompatibleThreadStateException te){
            try{ wait(1000); } catch(Exception c){};
            r = evaluate(val,pc,thread, raweval);
        }catch(Exception e){
            Helpers.log( e.toString() + "   " + thread.status() + "          EVAL:" + (raweval ? val : setEval(val)), "PageContextController.evaluate" );
        }

        return r;
    }

    public String dump(String val, PageContext pc, ThreadReference thread){
        makeObject(pc,thread);
        String r = null;
        ObjectReference o = null;

        try {
            Method m = pc.pc.referenceType().methodsByName("evaluate").get(0);
            ArrayList<Value> args = new ArrayList<Value>();
            args.add(Application.getInstance().vm.mirrorOf(setDump(val)));            
            o = (ObjectReference) pc.pc.invokeMethod(thread, m, args, ObjectReference.INVOKE_SINGLE_THREADED); 
            if(o instanceof StringReference){
                r = ( (StringReference) o).value();
            } else if (o != null){
                r = o.referenceType().name();
            }
        }catch(java.lang.IllegalMonitorStateException | com.sun.jdi.IncompatibleThreadStateException te){
            try{ wait(1000); } catch(Exception c){};
            r = dump(val,pc,thread);
        }catch(Exception e){
            Helpers.log( e.toString() + "          DUMP:" + setDump(val), "PageContextController.dump" );
        }

        return r;
    }
    */

    public boolean setClientObj(PageContext pc, ThreadReference thread){
        if(EventController.CFObjectGenerated) return true;

        boolean flag = false;
        try {           
            String val = "request.coldbugobject = createobject(\"component\",\"coldbug.coldbugobject\")";
            Method m = pc.pc.referenceType().methodsByName("Evaluate").get(0);
            ArrayList<Value> args = new ArrayList<Value>();
            args.add(Application.getInstance().vm.mirrorOf(val));
            pagecontextmonitor mo = this.pcm.addmonitor(val, pc.pc, thread);    
            pc.pc.invokeMethod(thread, m, args, ObjectReference.INVOKE_SINGLE_THREADED); 
            this.pcm.removemonitor(mo);
            flag = true;
            EventController.CFObjectGenerated = true;
        } catch(Exception e){
            Helpers.log( e.toString() + "    " + EventController.threadstatus(thread), "PageContextController.setclientobj" );
        }
        return flag;
    }

    public String convertToString(ObjectReference o){
        String r = "";

        if(o instanceof StringReference){
            r = ( (StringReference) o).value();
        } else if (o != null){
            r = o.referenceType().toString();
        }

        return r;
    }    

    public String old_output(String val, PageContext pc, ThreadReference thread){
        String r = null;
        try {
            ObjectReference o = old_evaluate(val, pc, thread);
            r = convertToString(o);
        }catch(Exception e){
            Helpers.log( e.toString(), "PageContextController.output" );
        }

        return r;
    }

    public boolean setrequesttimeout(long val, PageContext pc, ThreadReference thread){ 
        if(EventController.ISCF){
            old_evaluate("createObject(\"java\", \"coldfusion.runtime.RequestMonitor\").overrideRequestTimeout("+ val +")", pc, thread);
            return true;
        }

        boolean flag = false;  
        toggle(thread,1);         
        try {           
            Method m = pc.pc.referenceType().methodsByName("setRequestTimeout").get(0);
            ArrayList<Value> args = new ArrayList<Value>();
            args.add(Application.getInstance().vm.mirrorOf(val));
            pagecontextmonitor mo = this.pcm.addmonitor(Long.toString(val), pc.pc, thread);                    
            pc.pc.invokeMethod(thread, m, args, ObjectReference.INVOKE_SINGLE_THREADED);                       
            this.pcm.removemonitor(mo);
            flag = true;
        }catch(Exception e){
            Helpers.log( e.toString() + "    " + EventController.threadstatus(thread), "PageContextController.setrequesttimeout" );
        }
        toggle(thread,0); 
        return flag;
    }

    public void toggle(ThreadReference thread, int toggle){
        try {    
            /* Thread t = new Thread(){ public void run(){
                boolean error = true;
                while(error){
                    try{
                        try{ */
                            //EventController.toggleeventrequests(toggle,"toggle");
                     /*        error = false;                        
                       } catch(Exception e){
                            Helpers.log( e.toString(), "eval error" );
                            sleep(1000);
                        }
                    } catch (Exception e){
                        Helpers.log( e.toString(), "toggle event - eval" );
                    }
                }
            }}; 
            t.run(); 
            t.join(); */
        }catch(Exception e){
            Helpers.log( e.toString() + "    " + EventController.threadstatus(thread), "PageContextController.toggle" );
        }
    }

    public ObjectReference old_evaluate(String val, PageContext pc, ThreadReference thread){
        ObjectReference r = null;
        //Helpers.log(EventController.threadstatus(thread), "Evaluate");       
        //if(EventController.INVOKEHOLD) return r;

        /*
        for(Method m : pc.pc.referenceType().allMethods()){
            System.out.println(m.name());
        }
        */

        toggle(thread,1);
        try {           
            Method m = null;
            if(!EventController.ISCF) m = pc.pc.referenceType().methodsByName("evaluate").get(0);
            else {
                //setClientObj(pc, thread);
                m = pc.pc.referenceType().methodsByName("Evaluate").get(0);
            }
            ArrayList<Value> args = new ArrayList<Value>();
            args.add(Application.getInstance().vm.mirrorOf(val));
            pagecontextmonitor mo = this.pcm.addmonitor(val, pc.pc, thread);            	
                 
            r = (ObjectReference) pc.pc.invokeMethod(thread, m, args, ObjectReference.INVOKE_SINGLE_THREADED); 
            this.pcm.removemonitor(mo);
        } catch (InvocationException iex) {
            if(!EventController.ISCF) {
                ObjectReference er = iex.exception();
                ArrayList<Value> args = new ArrayList<Value>();
                Method m = er.referenceType().methodsByName("getStackTraceAsString").get(0);
                //StringReference s = null;
                try {
                    r = (ObjectReference) er.invokeMethod(thread, m, args, ObjectReference.INVOKE_SINGLE_THREADED); 
                } catch (Exception e) {
                    e.printStackTrace();
                }     
            }        
        }catch(Exception e){
            Helpers.log( e );
            thread.resume();
        }
        toggle(thread,0);
        return r;        
    }

    public ObjectReference invoke(ObjectReference ref, ThreadReference thread, Method m, ArrayList<Value> args){
        ObjectReference r = null;
        //todo make all methods call this function   
        
        toggle(thread,1);
        try {           
            if(args == null) args = new ArrayList<Value>();
            pagecontextmonitor mo = this.pcm.addmonitor(m.name(), ref, thread); 
            r = (ObjectReference) ref.invokeMethod(thread, m, args, ObjectReference.INVOKE_SINGLE_THREADED); 
            this.pcm.removemonitor(mo);
        } catch(Exception e){
            Helpers.log( e.toString() + "    " + EventController.threadstatus(thread), "PageContextController.invoke" );
        }
        toggle(thread,0);
        return r;        
    }

    public ObjectReference getpagecontext(ObjectReference ref, ThreadReference thread){
        ObjectReference r = null;
        //Helpers.log(EventController.threadstatus(thread), "Evaluate");       
        
        toggle(thread,1);
        try {           
            Method m = ref.referenceType().methodsByName("GetPageContext").get(0);
            ArrayList<Value> args = new ArrayList<Value>();
            pagecontextmonitor mo = this.pcm.addmonitor("GetPageContext", ref, thread); 
            r = (ObjectReference) ref.invokeMethod(thread, m, args, ObjectReference.INVOKE_SINGLE_THREADED); 
            this.pcm.removemonitor(mo);
        } catch(Exception e){
            Helpers.log( e.toString() + "    " + EventController.threadstatus(thread), "PageContextController.getpagecontext" );
        }
        toggle(thread,0);
        return r;        
    }

    public String old_dump(String val, PageContext pc, ThreadReference thread){
        String r = null;
        //Helpers.log( "", "Dumping");
        try {
            StringReference o = null;
            //System.out.println(pc.pc.referenceType());
            if (!EventController.ISCF) o = (StringReference) old_evaluate("getpagecontext().getConfig().getDumpWriter(\"html\",createObject(\"java\",\"lucee.runtime.dump.HTMLDumpWriter\").DEFAULT_RICH).toString(getpagecontext(),createObject(\"java\",\"lucee.runtime.dump.DumpUtil\").toDumpData(JavaCast(\"Object\","+val+"),getpagecontext(),20,createObject(\"java\",\"lucee.runtime.dump.DumpProperties\").DEFAULT),true)",pc,thread);
            else o = (StringReference) old_evaluate("request.coldbugobject.dump('"+val+"')", pc, thread);
            if(o != null){
                r = o.value();
                if(EventController.ISCF) r = appendcfdumpstyle(r);
            }
        }catch(Exception e){
            Helpers.log( e.toString(), "PageContextController.old_dump" );
        }

        return r;
    }

    public static boolean isJSONValid(String jsonInString) {
        try {
            Gson gson = new Gson();
            gson.fromJson(jsonInString, Object.class);
            return true;
        } catch(com.google.gson.JsonSyntaxException ex) { 
            return false;
        }
    }

    public void setAllWatches(PageContext pc, ThreadReference thread){

        ArrayList<WatchVar> watches = Application.getInstance().getActiveSettings().watchvars;
        for(WatchVar w : watches){
            setWatch(pc, w, thread);
        }

    }

    public void setWatch(PageContext pc, WatchVar watch, ThreadReference thread){        
        try {
            WatchKVP s = this.getWatch(pc, watch.text);

            if(s != null) return;

            s = new WatchKVP();
            s.name = watch.text;

            s.object = null;
            //todo add another thing here for evaluations
            ObjectReference ref = null;
            String val = null;
            if(!watch.isEval()) ref = old_evaluate("serializejson(" + watch.text + ")", pc, thread);
            else ref = old_evaluate(watch.text, pc, thread);
            if(ref != null) {
                try{
                    val = convertToString(ref);
                    JsonParser parser = new JsonParser();
                    JsonObject object = null;
                    String cleaned = val.replaceAll("createTimeSpan\\((.*)\\)", "\"createTimeSpan($1)\"").trim();
                    try{  
                        if(isJSONValid(cleaned))   {
                            object = parser.parse("{\""+s.name+"\":" + cleaned + "}").getAsJsonObject();
                        } else {
                            object = parser.parse("{\""+s.name+"\":\"" + cleaned.replace("\"","") + "\"}").getAsJsonObject();
                        }                       
                    } catch(Exception e){
                        try{
                            object = parser.parse("{\"nokey\":\"" + cleaned + "\"}").getAsJsonObject();
                        } catch(Exception e2){
                            Helpers.log( cleaned, "setwatcherror" );
                        }
                    }
                    fillNative(object,s);
                }catch(Exception e){
                    throw e;
                }
            } else {
                try{
                    JsonParser parser = new JsonParser();
                    JsonObject object = parser.parse("{\"error\":\"unable to load\"}").getAsJsonObject();
                    fillNative(object,s);
                }catch(Exception e){
                    throw e;
                }
            }
            pc.watches.add(s);
        }catch (Exception e){
            Helpers.log( e.toString(), "PageContextController.setWatch" );
        }        
    }

    public void setScopeNative(PageContext pc, Enums.ScopeType type, ThreadReference thread){
        //makeObject(pc,thread);
        for(String s : Application.getInstance().getActiveSettings().excludescopes){
            if(Enum.valueOf(Enums.ScopeType.class,s) == type){
                return;
            }
        }
        
        try {
            Scope s = this.getScope(pc, type);
            s.object = null;
            //StringReference ref = (StringReference) simpleeval("serializejson(" + type.name() + ")", pc, thread);
            StringReference ref = (StringReference) old_evaluate("serializejson(" + type.name() + ")", pc, thread);
            if(ref != null) {
                try{
                    JsonParser parser = new JsonParser();
                    JsonObject object = null;
                    try{
                        //todo for better handling, wrap anything not in the acceptable list, IE numbers, booleans, etc should be wrapped with quotes
                        object = parser.parse(ref.value().replaceAll("createTimeSpan\\((.*)\\)", "\"createTimeSpan($1)\"").trim()).getAsJsonObject();
                    } catch(Exception e){
                        try{
                            object = parser.parse("{\"nokey\":\"" + ref.value().trim() + "\"}").getAsJsonObject();
                        } catch(Exception e2){
                            Helpers.log( ref.value().trim(), "setscopenative" );
                        }
                    }
                    fillNative(object,s);
                }catch(Exception e){
                    throw e;
                }
            } else {
                try{
                    JsonParser parser = new JsonParser();
                    JsonObject object = parser.parse("{\"error\":\"unable to load\"}").getAsJsonObject();
                    fillNative(object,s);
                }catch(Exception e){
                    throw e;
                }
            }
        }catch (Exception e){
            Helpers.log( e.toString(), "PageContextController.setScopeNative" );
        }
    }

    public void workjsonarray(JsonArray array,  KVPFillable fillable){

        for(int i = 0; i < array.size(); i++){
            JsonElement entry = array.get(i);
            CustomValue cv = new CustomValue();
            if(entry.isJsonArray()){

                JsonArray a = entry.getAsJsonArray();
                workjsonarray(a,cv);
                fillable.kvp.put(Integer.toString(i), cv);

            } else if(entry.isJsonObject()){

                JsonObject obj = entry.getAsJsonObject();
                fillNative(obj,fillable);
                fillable.kvp.put(Integer.toString(i), cv);

            } else if(entry.isJsonPrimitive()){

                JsonPrimitive prim = entry.getAsJsonPrimitive();
                cv.value = prim.toString();
                fillable.kvp.put(Integer.toString(i), cv);

            }
        }

    }

    public void fillNative(JsonObject object, KVPFillable fillable){

        for (Map.Entry<String,JsonElement> entry : object.entrySet()) {

            fillable.keys.add( Application.getInstance().vm.mirrorOf( entry.getKey() ) );
            CustomValue cv = new CustomValue();

            if(entry.getValue().isJsonArray()){

                JsonArray array = entry.getValue().getAsJsonArray();
                workjsonarray(array,cv);
                fillable.kvp.put(entry.getKey(), cv);

            } else if(entry.getValue().isJsonObject()){

                JsonObject obj = entry.getValue().getAsJsonObject();
                fillNative(obj,cv);
                fillable.kvp.put(entry.getKey(), cv);

            } else if(entry.getValue().isJsonPrimitive()){

                JsonPrimitive prim = entry.getValue().getAsJsonPrimitive();
                cv.value = prim.toString();
                fillable.kvp.put(entry.getKey(), cv);

            }

        }


    }

    public String appendcfdumpstyle(String val){
        String s = Helpers.getResourceFile("styles/cfdump.css");
        return "<style>" + s + "</style>" + val;
    }

    //set scopes thread
    public class VarThread extends Thread {
        PageContext pc = null;
        EventThreadHandler eth = null;
        ThreadReference thread = null;
        Location location = null;
        public boolean interrupt = false;
        public boolean isclean = false;
        public String filepath = null;

        public VarThread(EventThreadHandler eth, PageContext pc, ThreadReference thread, Location location, String filepath){
            this.pc = pc;
            this.thread = thread;
            this.location = location;
            this.eth = eth;
            this.filepath = filepath;
        }

        public void Cleanup(){            
            Helpers.log("CLEANUP UITHREAD");
            //this.eth.eventinfo.UpdateUIThread = null;
            this.isclean = true;
        }

        @Override
        public void run() {            

            clientsidehandler cs = new clientsidehandler();
            pagecontextcontroller pcc = new pagecontextcontroller();

            if(this.interrupt) {
                this.Cleanup();
                return;
            }

            cs.UpdateLine(location, filepath);

            if(this.interrupt) {
                this.Cleanup();
                return;
            }

            if(this.eth.eventinfo.activedump != null) {
                cs.UpdateDumpPane(pcc.old_dump(this.eth.eventinfo.activedump,pc,thread), "#dumppane");                
            }

            if(this.interrupt) {
                this.Cleanup();
                return;
            }            

            for(Scope s : pc.scopes){
                if(s.type == Enums.ScopeType.None) continue;  
                if(this.interrupt) {
                    this.Cleanup();
                    return;
                }        
                //setScopeNative(pc,s.type, thread);
            }            
            clientsidehandler c = new clientsidehandler();

            if(this.interrupt) {
                this.Cleanup();
                return;
            }

            c.UpdateVariablesBar(pc);            

            if(this.interrupt) {
                this.Cleanup();
                return;
            }

            ArrayList<WatchVar> watches = Application.getInstance().getActiveSettings().watchvars;
            for(WatchVar w : watches){
                if(this.interrupt) {
                    this.Cleanup();
                    return;
                }  
                setWatch(pc, w, thread);
            }

            if(this.interrupt) {
                this.Cleanup();
                return;
            }

            c.UpdateWatches();            

            this.eth.eventinfo.UpdateUIThread = null;
        }

    }
}
