package com.riftwebdesign.coldbug.controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.riftwebdesign.coldbug.models.Application;
import com.riftwebdesign.coldbug.models.BreakpointLoop;
import com.riftwebdesign.coldbug.models.Breakpoints;
import com.riftwebdesign.coldbug.models.EventThreadHandler;
import com.riftwebdesign.coldbug.models.PageContext;
import com.riftwebdesign.coldbug.models.ProjectSettings;
import com.riftwebdesign.coldbug.pojo.interfaces.ieventset;
import com.riftwebdesign.coldbug.pojo.interfaces.ivirtualmachine;
import com.riftwebdesign.coldbug.util.Enums;
import com.riftwebdesign.coldbug.util.Enums.NextOperation;
import com.riftwebdesign.coldbug.util.Helpers;
import com.riftwebdesign.coldbug.util.TimeHelper;
import com.riftwebdesign.coldbug.util.countertester;
import com.sun.jdi.AbsentInformationException;
import com.sun.jdi.Location;
import com.sun.jdi.Method;
import com.sun.jdi.ObjectCollectedException;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.ReferenceType;
import com.sun.jdi.StackFrame;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.VMDisconnectedException;
import com.sun.jdi.event.BreakpointEvent;
import com.sun.jdi.event.Event;
import com.sun.jdi.event.LocatableEvent;
import com.sun.jdi.request.BreakpointRequest;
import com.sun.jdi.request.ClassPrepareRequest;
import com.sun.jdi.request.EventRequestManager;
import com.sun.jdi.request.ExceptionRequest;
import com.sun.jdi.request.InvalidRequestStateException;
import com.sun.jdi.request.StepRequest;
import com.sun.jdi.request.ThreadDeathRequest;
import com.sun.jdi.request.ThreadStartRequest;

/**
 * Created by sid on 11/17/2017.
 */
public class EventController {

	public static com.riftwebdesign.coldbug.engines.factory factory = new com.riftwebdesign.coldbug.engines.factory();
     public Application app = Application.getInstance();
	public static boolean TRACE = true;
	public static boolean USELINESTEPPER = false;
	public static boolean REQUESTOBJECTSHOULDEXIST = false;
	public static boolean SKIPBREAK = false;
	public static NextOperation old_op = null;
	public static boolean SETTIMEOUT = true;
	public static boolean BUTTONLOCK = false;
	public static boolean ISCF = false;
	public static boolean UITHREADCOMPLETE = true;
	public static boolean CFObjectGenerated = false;
	public static EventThreadHandler currentbreakthread = null;
	public static ArrayList<String> skipfiles = new ArrayList<String>();
	public static ArrayList<EventThreadHandler> threadqueue = new ArrayList<EventThreadHandler>();
	public static TimeHelper timer = TimeHelper.start();
	public static List<ReferenceType> vmtypes;
	public static Integer breakcounter = 0;
	public static ArrayList<String> exclusions = new ArrayList<String>(){
		private static final long serialVersionUID = 1L;
		{
		add("sun.*");
		add("flex.*");
		add("jdk.*");
		add("railo.*");
		add("java.*");
		add("org.*");
		add("lucee.*");
		add("javax.*");
		add("com.*");
		add("net.*");
		add("dump_cfc$cf");
		add("dump");
		add("generated");
		add("web-inf");
		add("/web-inf/cftags/dump.cfm");
		add("component_cfc$cf");
		add("coldbugobject_cfc$cf");
		add("coldbugramfile_cfm$cf");
		add("coldfusion.*");
	}};

    public static Thread getthread(){
        ivirtualmachine vm = Application.getInstance().vm;
        return new Thread("Monitor") {
            public void run() {
               //start a monitor to watch for cases when nothing happens for 5 seconds
               threadmonitor();
                while (true) {
                    try {						
                         if( !Application.IsConnected() || Thread.interrupted() ) {
                              //break from loop to complete thread
                              return;
                         }
                         ieventset eventSet = vm.eventQueue().remove(1);													
                         if(eventSet.hasevent()) {				
                              EventThreadHandler ehandler = new EventThreadHandler(eventSet);   
                              //for the monitor                            
                              //EventController.threadqueue.add(ehandler);
                              ehandler.run();
                         }
                    } catch (VMDisconnectedException ex){
						//if the vm disconnected, then we should just return
						return;
					} catch (Exception e) {
                        Helpers.log(e, "EventController.getthread");
                    }
                }
            }
        };
     }
     
     public static void threadmonitor() {    
          //stop checking
          if(Application.getInstance().t == null) {
               //todo try to get things to run incase the even controller thread died for some reason
               return;
          }

          final countertester hasAliveThread = new countertester();
          EventController.threadqueue.forEach((et)->{
               if( et.isWorking || et.isAlive() ){
                    hasAliveThread.increment();
               }
          });
          //Helpers.log("MONITORING THREAD: " + hasAliveThread.getVal());
          
          Helpers.setTimeout(() -> {
               threadmonitor();
           }, 3000);  
     }

	public static String threadstatus(ThreadReference t) {
		String r = "State Not Listed";
		switch(t.status()){
			case -1:
				r = "THREAD_STATUS_UNKNOWN";
				break;
			case 0:
				r = "THREAD_STATUS_ZOMBIE";
				break;
			case 1:
				r = "THREAD_STATUS_RUNNING";
				break;
			case 2:
				r = "THREAD_STATUS_SLEEPING";
				break;
			case 3:
				r = "THREAD_STATUS_MONITOR";
				break;
			case 4:
				r = "THREAD_STATUS_WAIT";
				break;
			case 5:
				r = "THREAD_STATUS_NOT_STARTED";
				break;
		}
		return r;
	}

	public static void toggleeventrequests(int toggle, String info) {

		//boolean r = true;
		//if(r) return;

		EventRequestManager erm = Application.getInstance().vm.eventRequestManager();

		int size = 0;
		size = erm.stepRequests().size();
		for (int i = size - 1; i >= 0; i--) {
			StepRequest sr = erm.stepRequests().get(i);
			try{								
				int status = sr.thread().status();
                boolean invalid = (status == ThreadReference.THREAD_STATUS_NOT_STARTED ||
                                   status == ThreadReference.THREAD_STATUS_UNKNOWN ||
								   status == ThreadReference.THREAD_STATUS_ZOMBIE);
				if(invalid) continue;
				if(toggle == 1) sr.disable();
				else if(toggle == -1) erm.deleteEventRequest(sr);
				else sr.enable();
			} catch(ObjectCollectedException | VMDisconnectedException | InvalidRequestStateException e2) {

			} catch(Exception e){
				//erm.deleteEventRequest(sr);
				e.printStackTrace();
			}
		}
		size = erm.exceptionRequests().size();
		for (int i = size - 1; i >= 0; i--) {
			try{
				ExceptionRequest er = erm.exceptionRequests().get(i);			
				if(toggle == 1) er.disable();
				else if(toggle == -1) erm.deleteEventRequest(er);
				else er.enable();
			} catch(ObjectCollectedException | VMDisconnectedException | InvalidRequestStateException e2) {

			}catch(Exception e){
				e.printStackTrace();
			}
		}
		size = erm.threadDeathRequests().size();
		for (int i = size - 1; i >= 0; i--) {
			try{
				ThreadDeathRequest er = erm.threadDeathRequests().get(i);
				if(toggle == 1) er.disable();
				else if(toggle == -1) erm.deleteEventRequest(er);
				else er.enable();
			} catch(ObjectCollectedException | VMDisconnectedException | InvalidRequestStateException e2) {

			}catch(Exception e){
				e.printStackTrace();
			}
		}
		size = erm.threadStartRequests().size();
		for (int i = size - 1; i >= 0; i--) {
			try{
				ThreadStartRequest er = erm.threadStartRequests().get(i);
				if(toggle == 1) er.disable();
				else if(toggle == -1) erm.deleteEventRequest(er);
				else er.enable();
			} catch(ObjectCollectedException | VMDisconnectedException | InvalidRequestStateException e2) {

			}catch(Exception e){
				e.printStackTrace();
			}
		}
		size = erm.breakpointRequests().size();
		for (int i = size - 1; i >= 0; i--) {
			try{
				BreakpointRequest er = erm.breakpointRequests().get(i);
				if(toggle == 1) er.disable();
				else if(toggle == -1) er.disable(); //erm.deleteEventRequest(er);
				else er.enable();
			} catch(ObjectCollectedException | VMDisconnectedException | InvalidRequestStateException e2) {

			}catch(Exception e){
				e.printStackTrace();
			}
		}

		Helpers.log("EventRequests " + (toggle == -1 ? "Cleared" : toggle == 1 ? "Disabled" : "Enabled"), "toggleeventrequests : " + info);
	}

	public static void fillStepRequest(StepRequest request){	

		Application.getInstance().vm.allClasses().forEach((r) -> {
			if(r.name().contains("cfdump") || r.name().contains("generated")){
				request.addClassExclusionFilter( r.name() );
			}			
		});

		for(String s : exclusions){
			request.addClassExclusionFilter(s);
		}

		request.setSuspendPolicy(Application.getInstance().getActiveSettings().suspend);
		request.enable();

	}

	public static void fillOthers(){
		Application app = Application.getInstance();
		EventRequestManager erm = app.vm.eventRequestManager();
		if(erm.exceptionRequests().size() == 0){
			ExceptionRequest er = erm.createExceptionRequest(null,false,true);
			er.enable();
		}
		if(erm.threadDeathRequests().size() == 0){
			ThreadDeathRequest td = erm.createThreadDeathRequest();
			td.enable();
		}
		if(erm.threadStartRequests().size() == 0){
			ThreadStartRequest ts = erm.createThreadStartRequest();
			ts.enable();
		}
		if(erm.classPrepareRequests().size() == 0){	
			erm.createClassPrepareRequest().enable();
		}
	}

	
	public static void addLinesToThread(EventThreadHandler eth, ThreadReference ref){
		Application app = Application.getInstance();
		EventRequestManager erm = app.vm.eventRequestManager();

		int steptype = StepRequest.STEP_INTO;
		
		if(eth.eventinfo.OpFlag == Enums.NextOperation.StepOver){
			steptype = StepRequest.STEP_OVER;
		} else if(eth.eventinfo.OpFlag == Enums.NextOperation.StepOut){
			steptype = StepRequest.STEP_OUT;
		}

		StepRequest request = erm.createStepRequest(ref, StepRequest.STEP_LINE, steptype);
		fillStepRequest(request);
	}	

	public static void createLineStepperRequest(EventThreadHandler eth, boolean clean){
		//Application app = Application.getInstance();
		//EventRequestManager erm = app.vm.eventRequestManager();

          //if(clean) toggleeventrequests(-1, "createlinesteapper");
          toggleeventrequests(-1, "createlinesteapper");
		
		//fillOthers();
		// for (ThreadReference ref : app.vm.allThreads()) {	
		// 	try{	
		// 		addLinesToThread(eth, ref);
		// 	} catch (Exception e){

		// 	}
		// }

		addLinesToThread(eth, eth.threadref);

		Helpers.log("STEP REQUEST ADDED");
	
	}

	public static void createBreakPointRequests(boolean clean){
		Application app = Application.getInstance();
		//ivirtualmachine vm = app.vm;
		EventRequestManager erm = app.vm.eventRequestManager();

		if(clean) {
			toggleeventrequests(-1, "createbreakpointrequests");
			erm.deleteAllBreakpoints();
		}

		if(erm.classPrepareRequests().size() == 0){	
			ClassPrepareRequest cpr = erm.createClassPrepareRequest();			
			Application.getInstance().vm.allClasses().forEach((r) -> {
				if( r.name().contains("cfdump") || r.name().contains("generated") ){
					cpr.addClassExclusionFilter( r.name() );
				}			
			});
			for(String s : exclusions){
				cpr.addClassExclusionFilter(s);
			}
			cpr.setSuspendPolicy(Application.getInstance().getActiveSettings().suspend);
			cpr.enable();
		}

		//fillOthers();

		boolean hasbreaks = app.getActiveSettings().breakpoints != null;
		ArrayList<String> breakmap = new ArrayList<String>();

		if(hasbreaks) {

			BreakpointLoop bl = new BreakpointLoop();		

			for (Breakpoints b : app.getActiveSettings().breakpoints) {
				if(bl.hasItem(b.path)) {
					continue;
				}

				String sourceName = Helpers.removeroots(Helpers.normalize2(b.path), app.getActiveSettings(), false);

				/*
					possible bug if first type is taken, it appears that classes get appended IE 
						PREPARING: login_cfm$cf$7   [EventThreadHandler.run():73]
						PREPARING: login_cfm$cf$8   [EventThreadHandler.run():73]
					
					so need to use the most recent type defined

				*/
				//final booltester breakout = new booltester();
				Application.getInstance().vm.allClasses().forEach((t) -> {
					//if( t.name().contains("login_cfm") ) System.out.println(t.name());
					//if(!breakout.getVal()){
						try{
							String tsource = Helpers.removeroots( Helpers.normalize2(t.sourceName()), app.getActiveSettings(), true);
							if (tsource.equals(sourceName)){ //&& !breakmap.contains(t.sourceName())) {
								breakmap.add(t.sourceName());
								//add lines						
								ArrayList<Integer> lines = new ArrayList<>();
								for (Breakpoints b2 : app.getActiveSettings().breakpoints) {
									//if(!b2.path.equals(b.path)) breakout.setVal(true);
									lines.add(b2.line);
                                        }
                                        //if( !bl.hasItem(t,b.path) ) 
                                             
                                        bl.addItem(t,b.path,lines);

								//breakout.setVal(true);
							}

						} catch (AbsentInformationException e) {
							//String error = e.getMessage();
						} catch (VMDisconnectedException e){
							return;
						}
						catch (Exception e) {
							Helpers.log(e.toString(), "createBreakPoint", true);
						}
					//}
				});
			}

			for (BreakpointLoop.BreakpointLoopItem item : bl.items) {
				try{
					boolean linefound = false;
					ArrayList<Integer> foundlines = new ArrayList<>();
					for(Location loc : item.type.allLineLocations()){
						if(!BreakpointLoop.hasLine(item,loc.lineNumber())) continue; //|| foundlines.contains(loc.lineNumber())) continue;							
						//for (ThreadReference ref : app.vm.allThreads()) {
							//Helpers.log(item.path + " : " + loc.lineNumber(), "createBreakPoint", true);
							foundlines.add( loc.lineNumber() );
							BreakpointRequest request = erm.createBreakpointRequest( loc );
							request.setSuspendPolicy(Application.getInstance().getActiveSettings().suspend);
							request.enable();
							linefound = true;

							for (Breakpoints br : app.getActiveSettings().breakpoints) {
								if(!br.path.equals( item.path )) continue;
								if(br.line == loc.lineNumber()) {
									br.hasline = true;
								}
							}
						//}
					}
					if(!linefound) {
						//Helpers.log_event("No line number matched for " + item.path,1);
					}
				} catch (AbsentInformationException e) {
					//String error = e.getMessage();
				} catch (VMDisconnectedException e){
					return;
				} catch (Exception e) {
					Helpers.log(e.toString(), "createBreakPoint", true);
				}
			}

		}

	}

	public static boolean handleGenericEvent(EventThreadHandler eth) {
		try {
			Event event = eth.event;
			Application app = Application.getInstance();
			ProjectSettings ps = app.getActiveSettings();
			LocatableEvent genericevent = (LocatableEvent)event;			
			Location location = genericevent.location();               

			//log trace
               //Helpers.log("TRACE" + sourcepath + " : " + linenum);

               pagecontextcontroller pcc = new pagecontextcontroller();
			ThreadReference thread = genericevent.thread();
			ObjectReference or = null;
               for (StackFrame frame : thread.frames()) {		
                    if(or == null){		
                         ReferenceType dectype = frame.location().declaringType();
                         Method me = null;
                         for(Method m : dectype.methods()){
                              if( m.name().equals("getPageContext") ){
                                   me = m;
                                   break;
                              }					
                         }
                         if(dectype.name().equals("lucee.runtime.PageContextImpl") || dectype.name().contains("NeoPageContext")){
                              or = frame.thisObject();
                              break;					
                         } else if( dectype.fieldByName("pc") != null){
                              or = (ObjectReference)frame.getValue( frame.visibleVariableByName("pc") );
                              break;
                         } else if (me != null){					
                              or = pcc.invoke(frame.thisObject(), thread, me, null);
                              break;
                         } 
                    }
			}			
			if(or == null){
                    //or = pcc.getpagecontext( thread.frames().get(0).thisObject(), thread );
                    
				//or = thread.frames().get(0).thisObject();
				//if(or != null) EventController.ISCF = true;
			}	
			if(or == null) return false;		
               PageContext pc = new PageContext(or);      
               
               String cgisource = Helpers.findsourcecgi(pc, thread);
               String sourcepath = Helpers.removeroots(Helpers.normalize2(cgisource), ps, true);
			int linenum = location.lineNumber();						
			boolean islinestep = event instanceof BreakpointEvent;
               
               if(linenum == -1) {	
				return false;
			}
			boolean isbreak = false;
			if(islinestep) {	
				isbreak = ps.hasbp(cgisource,linenum);
			} else {
				isbreak = true;				
			}	
			if(!isbreak){
				return false;
			}
			//if it is a break, lets check if the file exists, if not let the system continue
			String path = cgisource; //Helpers.getSystemPath(EventController.currentbreakthread, location);
			File f = new File(path);			
			if(!f.exists()){
				Helpers.log("File not found: " + path);
				return false;
			}

			//todo make sure the file actually exists, otherwise do not break, and inform the user the program wanted to break at a line that didn't exist.

			

			/*
			System.out.println("");
			System.out.println("");
			System.out.println("-----------------Threads----------------------------");
			for(ThreadReference t : app.vm.allThreads()){
				System.out.println(t.name());
			}
			System.out.println("---------------------------------------------");
			System.out.println("");
			System.out.println("");

			for (StackFrame frame : thread.frames()) {				
				ReferenceType dectype = frame.location().declaringType();
				System.out.println("");
				System.out.println("");
				System.out.println("-----------------Object----------------------------");
				System.out.println(dectype.name());
				System.out.println("------------------Methods---------------------------");
				for(Method m : dectype.methods()){
					System.out.println(m.name());					
				}
				System.out.println("---------------------------------------------");
				System.out.println("-------------------Fields--------------------------");
				for(Field f : dectype.fields()){
					System.out.println(f.name());				
				}
				System.out.println("---------------------------------------------");
				System.out.println("-------------------Vars--------------------------");
				for(LocalVariable lv : frame.visibleVariables()){
					System.out.println(lv.name());				
				}
				System.out.println("---------------------------------------------");
				System.out.println("");
				System.out.println("");
			}
			*/
						
			
			//if (timer.time(TimeUnit.SECONDS) >= 15){
				boolean r = pcc.setrequesttimeout(900000000,pc,thread);
				if(r) {
					Helpers.log("TIMEOUT IS SET");
					timer.reset();
				} 
			//}

			if(EventController.SKIPBREAK) return false;
			if(!isbreak){
				return false;
			}

			Helpers.log("BREAK TRACE: " + event.getClass().getName() +" : "+  cgisource + " : " + location.lineNumber());			

			if(or != null){				
				pc.location = location;
				eth.eventinfo.Break = true;
				eth.eventinfo.LastSourcePath = cgisource;
				eth.eventinfo.LastSourceLine = location.lineNumber();
				eth.eventinfo.lastpc = pc;
				eth.eventinfo.lastthread = thread;	
						
				eth.eventinfo.UpdateUIThread = pcc.new VarThread(eth,pc,thread,location,cgisource); 			
                    eth.eventinfo.UpdateUIThread.start();
                    return true;
			}
		} catch (Exception e) {
			Helpers.log(e, "EventController.handleEvent");
          }
          return false;
	}

	public static void killthreads(EventThreadHandler eth){
          if(eth.eventinfo.UpdateUIThread != null) {
               ((pagecontextcontroller.VarThread)eth.eventinfo.UpdateUIThread).interrupt = true;
          }
		while( eth.eventinfo.UpdateUIThread != null ){ 
               if( ((pagecontextcontroller.VarThread)eth.eventinfo.UpdateUIThread).isclean ){
                    eth.eventinfo.UpdateUIThread = null;
               }
          }
		eth.eventinfo.UpdateUIThread = null;
	}

	public static void actionbuttonclick(String button){
		if(EventController.BUTTONLOCK) return;
		
		EventController.BUTTONLOCK = true;

          EventThreadHandler eth = EventController.currentbreakthread;
          
          //there is a bug here with step requests getting stuck because something isnt finishing

		//pagecontextcontroller pcc = new pagecontextcontroller();
          //EventController.factory.createheartbeathandler().clean();     
          //killthreads(eth);     
          
          eth.eventinfo.Break = false;

        try {
			NextOperation oldflag = eth.eventinfo.OpFlag;            		
            switch (button) {
                case "play":
					eth.eventinfo.OpFlag = Enums.NextOperation.Play;
                    break;
                case "stepin":
                         eth.eventinfo.OpFlag = Enums.NextOperation.StepInto;
                    break;
                case "stepover":
					eth.eventinfo.OpFlag = Enums.NextOperation.StepOver; //BUG stepover when returning to a different file will cause a play effect
                    break;
                case "stepout":
					eth.eventinfo.OpFlag = Enums.NextOperation.StepOut;
                    break;
			}

			Helpers.log("ACTIONBUTTON: " + button);
               registersteps(eth, oldflag != eth.eventinfo.OpFlag);	
               eth.eventinfo.Break = false;		

        } catch (Exception e){
            Helpers.log(e.toString(), "EventController.actionbuttonclick");
		}

		EventController.BUTTONLOCK = false;
	}
	
	public static void registersteps(EventThreadHandler eth, boolean clean){
		try {   
			// we need to setup handlers for trace, like step even but no breaking
			// if(EventController.TRACE) 
			boolean islinestep = eth != null && eth.eventinfo.OpFlag != Enums.NextOperation.Play && eth.eventinfo.OpFlag != Enums.NextOperation.None;
			if(EventController.USELINESTEPPER || islinestep) {
				EventController.createLineStepperRequest(eth, clean);
				EventController.createBreakPointRequests(false);
			}
			else {
				EventController.createBreakPointRequests(clean);
			}				
        } catch (Exception e){
            Helpers.log(e.toString(), "EventController.registersteps");
		}
		
	}


}
