package com.riftwebdesign.coldbug.controllers;

import com.riftwebdesign.coldbug.models.Application;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ThreadPool;

import spark.embeddedserver.jetty.EmbeddedJettyFactory;
import spark.embeddedserver.jetty.JettyServerFactory;

public class ServerFactoryController implements JettyServerFactory {
    public ServerFactoryController() { }

    public EmbeddedJettyFactory create() {
        ServerFactoryController embeddedJettyServerFactory = new ServerFactoryController();

        return new EmbeddedJettyFactory(embeddedJettyServerFactory);
    }

    @Override
    public Server create(int maxThreads, int minThreads, int threadTimeoutMillis) {
        Server server;
        if (maxThreads > 0) {
            int max = maxThreads > 0 ? maxThreads : 200;
            int min = minThreads > 0 ? minThreads : 8;
            int idleTimeout = threadTimeoutMillis > 0 ? threadTimeoutMillis : '\uea60';
            server = new Server(new QueuedThreadPool(max, min, idleTimeout));
        } else {
            server = new Server();
        }

        /*
        for (Connector c : server.getConnectors()) {
            c.getConnectionFactory(HttpConnectionFactory.class).getHttpConfiguration().setRequestHeaderSize(65535);
        }
        */
        HttpConfiguration config = new HttpConfiguration();
        config.setRequestHeaderSize(0);
        config.setIdleTimeout(0);
        ServerConnector http = new ServerConnector(server, new HttpConnectionFactory(config));
        http.setPort(Integer.parseInt(Application.getInstance().getconfigprop("port")));
        server.setConnectors(new Connector[] {http});

        server.setAttribute("org.eclipse.jetty.server.Request.maxFormContentSize", 500000);    

        return server;
    }

    @Override
    public Server create(ThreadPool threadPool) {
        return new Server(threadPool);
    }
}