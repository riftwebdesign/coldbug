package com.riftwebdesign.coldbug.handlers;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.riftwebdesign.coldbug.util.Enums;
import com.riftwebdesign.coldbug.util.Helpers;

public class systemtrayhandler {
    public boolean issupported() {
        return SystemTray.isSupported();
    }
    public void add(TrayIcon t) {
        try {
            SystemTray.getSystemTray().add(t);
        } catch (AWTException e) {
            Helpers.log(e);
        }
    }
    public TrayIcon gettrayicon(Image i, PopupMenu p){
        TrayIcon trayIcon = new TrayIcon(i, Enums.TextMessages.AppName, p);
        trayIcon.setImageAutoSize(true);
        return trayIcon;
    }    
    public MenuItem getclose(){
        MenuItem close = new MenuItem(Enums.TextMessages.TrayIconClose);
        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);             
            }
        });
        return close;
    }
    public PopupMenu getpopupmenu(){
        PopupMenu trayPopupMenu = new PopupMenu();        
        trayPopupMenu.add( this.getclose() );
        return trayPopupMenu;
    }
    public void addTrayIcon(){
        if(!this.issupported()){
            Helpers.log(Enums.ErrorMessages.UnsupportedTrayIcon);
            return ;
        }        
        Image image = Helpers.getImageFile(Enums.Paths.Images.TrayIcon);
        this.add( this.gettrayicon( image, getpopupmenu() ) );
    }
}