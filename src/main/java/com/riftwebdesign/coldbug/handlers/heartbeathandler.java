
package com.riftwebdesign.coldbug.handlers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.riftwebdesign.coldbug.handlers.apihandler;
import com.riftwebdesign.coldbug.models.Application;
import com.riftwebdesign.coldbug.pojo.pagecontextmonitor;
import com.riftwebdesign.coldbug.util.Helpers;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.ThreadReference;

public class heartbeathandler {

    public volatile static ArrayList<pagecontextmonitor> objlist = new ArrayList<pagecontextmonitor>();
    public apihandler apicontroller = new apihandler();

    public void callapiheartbeat(boolean loggedin){
        if(loggedin){
            try{
                //this.apicontroller.serviceRequest( "method=heartbeat" );
            } catch(Exception e) {
                Helpers.log(e);
            }
        }
    }

    public void removepagecontextmonitors(){
        Date ldt = Calendar.getInstance().getTime();
        for(pagecontextmonitor o : objlist){
            if(o.expires.before(ldt)){
                try{
                    removemonitor(o);   
                } catch(Exception e){
                    objlist.remove(o);
                    Helpers.log(e);
                }                     
            }
        }
    }

    public void monitor(){
        /*
        new Thread(){ public void run(){
            this.setDaemon(true);
            int heartbeatcycle = 0;
            while(true){
                try {
                    sleep(1000);
                    if(heartbeatcycle >= 3){
                        callapiheartbeat(Application.getInstance().loggedin);
                    }
                    removepagecontextmonitors();
                } catch (Exception e) {
                    Helpers.log(e);
                } 
                heartbeatcycle++;  
            }
        }}.run();
        */       
    }

    public pagecontextmonitor addmonitor(String val, ObjectReference o, ThreadReference tr){
        pagecontextmonitor mo = new pagecontextmonitor();
        mo.val = val;
        mo.tr = tr;
        mo.or = o;
        return null;
    }

    public void removemonitor(pagecontextmonitor o){
        objlist.remove(o);
    }

    public void clean(){
        try {
            //Date ldt = Calendar.getInstance().getTime();
            for(pagecontextmonitor o : objlist){
                try{
                    o.tr.stop(null);
                    removemonitor(o);   
                } catch(Exception e){
                    objlist.remove(o);
                    Helpers.log(e);
                }  
            }
        } catch (Exception e) {
            Helpers.log(e);
        }   
    }

}

