package com.riftwebdesign.coldbug.handlers;

import static spark.Spark.get;
import static spark.Spark.webSocket;

import java.lang.reflect.Method;

import com.google.gson.Gson;
import com.riftwebdesign.coldbug.handlers.clientsidehandler;
import com.riftwebdesign.coldbug.controllers.ServerFactoryController;
import com.riftwebdesign.coldbug.controllers.SocketController;
import com.riftwebdesign.coldbug.engines.factory;
import com.riftwebdesign.coldbug.pojo.sparkrequest;
import com.riftwebdesign.coldbug.pojo.sparkresponse;
import com.riftwebdesign.coldbug.pojo.interfaces.isparkrequest;
import com.riftwebdesign.coldbug.pojo.interfaces.isparkresponse;
import com.riftwebdesign.coldbug.util.Enums;
import com.riftwebdesign.coldbug.util.Helpers;

import spark.Spark;
import spark.embeddedserver.EmbeddedServers;

public class routehandler {

    factory factory = new factory();
    Gson gson = new Gson();  

    public String appendwildcard(String path){
        return path + "/*";
    }

    public String handleroot(isparkrequest req, isparkresponse res){
        return Helpers.getResourceFile(Enums.Paths.FileNames.index);
    }

    public String handlegeneric(isparkrequest req, isparkresponse res){
        Helpers.setResponseMime(req.uri(), res);
        return Helpers.getResourceFile(req.uri());
    }

    public String callclientmethod(isparkrequest req) throws Exception {
        clientsidehandler c = this.factory.createclientsidehandler();  
        Method method = c.getClass().getMethod(req.queryParams("method"),isparkrequest.class);
        return this.gson.toJson(method.invoke(c, req));
    }

    public String handleapi(isparkrequest req, isparkresponse res){
        String r = null;
        try{            
            Helpers.setResponseMime(Enums.Paths.Mime.jsonnoformat,res);                           
            r = this.callclientmethod(req);
        } catch (Exception e){
            Helpers.log(e);
        }
        return r;
    }

    public void register(){  
        EmbeddedServers.add(EmbeddedServers.Identifiers.JETTY, new ServerFactoryController().create());
        Spark.staticFileLocation(Enums.Paths.API.publicpath);
        webSocket(Enums.Paths.API.socket, SocketController.class);      
        get(Enums.Paths.API.root, (req, res) ->  {
            return this.handleroot(new sparkrequest(req), new sparkresponse(res));
        });
        get(this.appendwildcard(Enums.Paths.API.files), (req, res) ->  {
            return this.handlegeneric(new sparkrequest(req),new sparkresponse(res));
        });
        get(this.appendwildcard(Enums.Paths.API.scripts), (req, res) ->  {
            return this.handlegeneric(new sparkrequest(req),new sparkresponse(res));
        });
        get(this.appendwildcard(Enums.Paths.API.libs), (req, res) ->  {
            return this.handlegeneric(new sparkrequest(req), new sparkresponse(res));
        });
        get(this.appendwildcard(Enums.Paths.API.styles), (req, res) ->  {   
            return this.handlegeneric(new sparkrequest(req), new sparkresponse(res));
        });
        get(Enums.Paths.API.api, (req, res) ->  {   
            return this.handleapi(new sparkrequest(req), new sparkresponse(res));
        });
    }
}