package com.riftwebdesign.coldbug.handlers;

import java.io.IOException;
import java.util.Map;

import com.riftwebdesign.coldbug.pojo.virtualmachinemanager;
import com.riftwebdesign.coldbug.pojo.interfaces.ivirtualmachine;
import com.riftwebdesign.coldbug.pojo.interfaces.ivirtualmachinemanager;
import com.riftwebdesign.coldbug.pojo.interfaces.ivmconnector;
import com.riftwebdesign.coldbug.pojo.interfaces.ivmconnectorargument;
import com.riftwebdesign.coldbug.util.Helpers;
import com.sun.jdi.Bootstrap;
import com.sun.jdi.VirtualMachine;

public class virtualmachinehandler {

    public ivirtualmachine connect(String host, int port) throws IOException {
        String strport = Integer.toString(port);
        ivmconnector connector = this.getconnector();
        try {
            ivirtualmachine vm = this.connect(connector, host, strport);
            vm.resume();
            vm.dispose();

            // Thread.sleep(2000);

            // vm = connect(connector,host, strport);
            // vm.resume();
            // vm.dispose();

            // Thread.sleep(2000);

            vm = connect(connector,host, strport);

            vm.setDebugTraceMode(VirtualMachine.TRACE_EVENTS & VirtualMachine.TRACE_OBJREFS);
            //vm.setDebugTraceMode(30);
            return vm;
        } catch (Exception e) {
            Helpers.log(e);
            throw new IllegalStateException(e);
        }
    }

    public ivirtualmachinemanager getvmmanager(){
        virtualmachinemanager vmm = new virtualmachinemanager();
        vmm.vmmanager = Bootstrap.virtualMachineManager();
        return vmm;
    }

    public ivmconnector getconnector() {
        ivirtualmachinemanager vmManager = this.getvmmanager();
        for (ivmconnector connector : vmManager.attachingConnectors()) {
            if("com.sun.jdi.SocketAttach".equals(connector.name()))
                return connector;
        }
        throw new IllegalStateException();
    }

    public ivirtualmachine connect(ivmconnector connector,String host,String port) throws Exception {
        Map<String, ivmconnectorargument> args = connector.defaultArguments();
        ivmconnectorargument portArg = args.get("port");        
        ivmconnectorargument addressArg = args.get("hostname");
        portArg.setValue(port);
        addressArg.setValue(host);
        return connector.attach(args);
    }
}