package com.riftwebdesign.coldbug.handlers;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;

import com.google.gson.JsonObject;
import com.riftwebdesign.coldbug.controllers.EventController;
import com.riftwebdesign.coldbug.controllers.SocketController;
import com.riftwebdesign.coldbug.controllers.WatchController;
import com.riftwebdesign.coldbug.controllers.pagecontextcontroller;
import com.riftwebdesign.coldbug.models.Application;
import com.riftwebdesign.coldbug.models.Breakpoints;
import com.riftwebdesign.coldbug.models.CustomValue;
import com.riftwebdesign.coldbug.models.DirTree;
import com.riftwebdesign.coldbug.models.DirectoryMap;
import com.riftwebdesign.coldbug.models.EventThreadHandler;
import com.riftwebdesign.coldbug.models.PageContext;
import com.riftwebdesign.coldbug.models.ProjectSettings;
import com.riftwebdesign.coldbug.models.Scope;
import com.riftwebdesign.coldbug.models.WatchKVP;
import com.riftwebdesign.coldbug.models.WatchVar;
import com.riftwebdesign.coldbug.pojo.*;
import com.riftwebdesign.coldbug.pojo.interfaces.*;
import com.riftwebdesign.coldbug.util.Enums;
import com.riftwebdesign.coldbug.util.Helpers;
import com.riftwebdesign.coldbug.util.gson;
import com.sun.jdi.Location;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.request.EventRequestManager;

/**
 * Created by sid on 11/17/2017.
 */
public class clientsidehandler {

    private gson gson = new gson();
    public com.riftwebdesign.coldbug.engines.factory factory = new com.riftwebdesign.coldbug.engines.factory();
    public apihandler apicontroller = new apihandler();
    public Helpers helper = new Helpers();

    public String echo(isparkrequest r){
        return "echo";
    }

    //https://getcoldbug.com/user.cfc?method=login
    public Object login(isparkrequest r){
        //SETUP
        jsonresponse jr = this.factory.createjsonresponse();
        login l = this.factory.createlogin();
        JsonObject obj = null;
        String json = null;
        //API CALL
        try{
            //json = this.apicontroller.serviceRequest( r.queryString() );
            json = "{\"firstname\":\"Developer\",\"lastname\":\"Developer\",\"expired\":false,\"nextbillingdate\":null,\"email\":\"NA\",\"newsletter\":0,\"validated\":1,\"hascard\":true}";
            obj = this.gson.fromJson(json, JsonObject.class);
            if(obj!=null) {
                l.loggedin = true; //obj.get("success").getAsBoolean();
            }  
        } catch(Exception e){
            Helpers.log(e);
        }    
        //COMPLETE LOGIN  
        if(l.loggedin){
            jr.json = json;
            Application.getInstance().loggedin = true;
        } else {
            jr.json = gson.toJson( l );
        }
        jr.errorlevel = 0;
        jr.message = "Logged In";      
        //RESPOND  
        return jr;
    }

    public Object logout(isparkrequest r){
        try{
            Application.getInstance().loggedin = false;
            apicontroller.serviceRequestoJson( r.queryString() );
        } catch(Exception e){
            Helpers.log(e);
        }        
        return null;
    }

    public Object updateuser(isparkrequest res){
        jsonresponse jr = this.factory.createjsonresponse();
        jr.json = "{\"success\":false}";
        jr.errorlevel = 0;
        try{
            jr.json = this.apicontroller.serviceRequest( res.queryString() );
        } catch(Exception e){
            Helpers.log(e);
        }        
        return jr;
    }

    public Object register(isparkrequest res){
        jsonresponse jr = this.factory.createjsonresponse();
        jr.json = "{\"success\":false}";
        jr.errorlevel = 0;
        try{
            //jr.json = apicontroller.serviceRequest( res.queryString() );
        } catch(Exception e){
            Helpers.log(e);
        }        
        return jr;
    }

    public Object forgotpassword(isparkrequest res){
        jsonresponse jr = this.factory.createjsonresponse();
        jr.json = "{\"success\":false}";
        jr.errorlevel = 0;
        try{
            //jr.json = apicontroller.serviceRequest( res.queryString() );
        } catch(Exception e){
            Helpers.log(e);
        }        
        return jr;
    }

    public Object readjson(isparkrequest res){
        String file = res.queryParams("path");
        jsonresponse jr = this.factory.createjsonresponse();
        String r = "";
        try {
            r = this.helper.readjson(file);
            jr.message = "Opened File";
            if(file.equals("projectsettings")){
                Application.getInstance().settings = gson.fromJson(r, ProjectSettings[].class);
                jr.message = "Opened Project";
            }
            jr.errorlevel = 0;            
            jr.json = r;
		} catch (Exception e) {
            jr.errorlevel = 1;
            jr.message = e.toString();
			Helpers.log(e);
        }       
        
        return jr;
    }

    public Object savejson(isparkrequest res){
        String file = res.queryParams("file");
        String json = res.queryParams("json");
        jsonresponse jr = this.factory.createjsonresponse();
        try {
            jr.errorlevel = 0;
            jr.message = "File saved";
			this.savejsonfile(file,json);
		} catch (Exception e) {
            jr.errorlevel = 1;
            jr.message = e.toString();
			Helpers.log(e);
        }
        
        return jr;
    }

    public void savejsonfile(String file,String json){
        try {
            this.helper.savejsonfile(file, json);
            if(file.equals("projectsettings")){
                Application.getInstance().settings = gson.fromJson(json, ProjectSettings[].class);
            }
        } catch (Exception e) {
			Helpers.log(e);
        }
    }

    public Object openproject(isparkrequest res){
        Integer index = Integer.parseInt(res.queryParams("index"));
        jsonresponse jr = this.factory.createjsonresponse();
        try {
            Application.getInstance().activesettingindex = index;                      
            WatchController wc = this.factory.watchcontroller();
            wc.stop_watching();
            wc.start_watching();
        } catch (Exception e){
            Application.getInstance().activesettingindex = -1;
            jr.errorlevel = 1;
            jr.message = e.toString();
            Helpers.log(e);
        }

        return jr;
    }

    public Object connect(isparkrequest res){
        jsonresponse jr = this.factory.createjsonresponse();
        connectresponse cr = this.factory.createconnectresponse();
        ivirtualmachine vm = null;
        Application app = Application.getInstance();
        virtualmachinehandler vmh = this.factory.createvirtualmachinehandler();
        try {
            vm = vmh.connect(app.gethost(), app.getport());
            Helpers.log("name=" + vm.name());
            Helpers.log("description=" + vm.description());
            app.vm = vm;
            //EventController.vmtypes = vm.allClasses();
            //Helpers.log( Integer.toString( EventController.vmtypes.size() ), "type count" );
            app.t = EventController.getthread();
            app.t.start();
            this.registersteps();
            cr.connected = true;            
            jr.json = gson.toJson(cr);            
        } catch (Exception e) {
            jr.errorlevel = 1;
            jr.message = e.toString();
            jr.json = gson.toJson(cr);
            Helpers.log(e);
        }
        return jr;
    }

    // private void initbreakpoints(){
    //     clientsidehandler $this = this;
    //     $this.registersteps();
    //     boolean hasbreaks = Application.getInstance().getActiveSettings().breakpoints != null && Application.getInstance().getActiveSettings().breakpoints.length > 0;
    //     if( !(hasbreaks && Application.getInstance().vm.eventRequestManager().breakpointRequests().size() == 0) ) {
    //         return;
    //     }
    //     Helpers.setTimeout(new Thread() {
    //         @Override
    //         public void run() {
    //             System.out.println("another attempt");
    //             $this.initbreakpoints();
    //             super.run();
    //         }
    //     }, 1000);
    // }

    public void registersteps(){
        this.registersteps_w_clean(true);
    }

    public void registersteps_w_clean(boolean clean){
        Application app = Application.getInstance();
        EventThreadHandler eth = EventController.currentbreakthread;
        if(app != null && app.vm != null) {
            try{
                EventController.registersteps(eth, clean);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public Object setbreakpoints(isparkrequest res){
        String json = res.queryParams("json");
        jsonresponse jr = new jsonresponse();
        Application app = Application.getInstance();
        app.getActiveSettings().breakpoints = gson.fromJson(json, Breakpoints[].class);
        this.registersteps();
        return jr;
    }
    
    public Object loadfile(isparkrequest res){
        String path = res.queryParams("path");
        jsonresponse jr = new jsonresponse();
        fileresponse fr = new fileresponse();
        String r = "";
        try {
           r = new String(Files.readAllBytes(Paths.get(path)));
           fr.filecontents = new String(Base64.getEncoder().encode(r.getBytes()));
           fr.hash = Integer.toString(r.hashCode());
           fr.path = path;
           jr.json = gson.toJson(fr);
		} catch (Exception e) {                                   
            if(Application.getInstance().getActiveSettings().removeopenfile(path)){
                savejsonfile("projectsettings",gson.toJson(Application.getInstance().settings));
                Helpers.log("Removing missing file", "ClientSideController.loadfile");
                SocketController sc = new SocketController();
                socketresponse sr = new socketresponse();
                sr.callback = "refreshsettings";
                sc.send(sr);
            }
            jr.errorlevel = 1;
            jr.message = e.toString();
			Helpers.log(e.toString(), "ClientSideController.loadfile");
		}
        return jr;
    }
    
    public Object comparehash(isparkrequest res){
        String path = res.queryParams("path");
        String hash = res.queryParams("hash");
        jsonresponse jr = new jsonresponse();
        comparehashresponse fr = new comparehashresponse();
        String r = "";
        try {
           r = new String(Files.readAllBytes(Paths.get(path)));
           String newhash = Integer.toString(r.hashCode());
           fr.result = newhash.equals(hash) == false;
           fr.path = path;
           jr.json = gson.toJson(fr);
		} catch (Exception e) {
            jr.errorlevel = 1;
            jr.message = e.toString();
			Helpers.log(e.toString(), "ClientSideController.comparehash");
		}
        return jr;
    }

    public Object getwatch(isparkrequest res){
        jsonresponse jr = new jsonresponse();

        jr.json = gson.toJson(Application.getInstance().getActiveSettings().watchvars);

        EventThreadHandler eth = EventController.currentbreakthread;
        if(eth.eventinfo.Break && eth.eventinfo.lastpc != null && eth.eventinfo.lastthread != null){

            //todo dirty check, and check if item was removed so if it is readded later

            PageContext pc = eth.eventinfo.lastpc;
            ThreadReference thread = eth.eventinfo.lastthread;
            try {
                pagecontextcontroller pcc = new pagecontextcontroller();
                pcc.setAllWatches(pc,thread);
                ArrayList<WatchVar> watches = Application.getInstance().getActiveSettings().watchvars;

                ArrayList<DirTree> r = new ArrayList<>();
                for(WatchVar w : watches){
                    WatchKVP watch = pcc.getWatch(pc,w.text);
                    DirTree d = new DirTree();
                    d.text = watch.name;
                    d.icon = "false";
                    //d.a_attr.put("scope",s.name().toLowerCase());
                    generatedirtree(watch.kvp, d, "");
                    r.add(d);
                }
                String rs = gson.toJson(r);
                jr.json = rs;
            } catch (Exception e) {
                e.printStackTrace();
                Helpers.log(e.toString(), "ClientSideController.getwatch");
            }
        }


        //todo if the system is in the middle of a break, go fetch the values for all the watches

        /*
            [{
                "text":"application.mytestvar",
                "a_attr":{
                    "myid":"true"
                }
            },{
                "text":"testsubvar",
                "children":[
                    {
                        "text":"application.mytestvar",
                        "a_attr":{
                            "child":"true"
                        }
                    }
                ]
            }]
        */

        return jr;
    }

    public Object getprojecttree(isparkrequest res){
        jsonresponse jr = new jsonresponse();
        jr.json = getfullprojecttree();
        return jr;
    }

    public Object kill(isparkrequest res){
        System.exit(1);
        return null;
    }

    public String getfullprojecttree(){
        ArrayList<DirTree> arr = new ArrayList<>();
        for(DirectoryMap m : Application.getInstance().getActiveSettings().directorymap) {
            File curDir = new File(m.root);
            arr.add(getAllFiles(curDir));
        }
        sortdirtree(arr);
        return gson.toJson(arr);
    }
        private void sortdirtree(ArrayList<DirTree> arr){
            Collections.sort(arr, (DirTree a1, DirTree a2) -> {
                int r = -1;
                if(a1.isdirectory == a2.isdirectory && a1.text.compareTo(a2.text) > 0) {
                    r = 1;
                } else if(!a1.isdirectory && a2.isdirectory){
                    r = 1;
                }
                return r;
            });
        }
        private DirTree getAllFiles(File curDir) {
            DirTree t = new DirTree();
            t.icon = "fa fa-folder";
            t.text = curDir.getAbsolutePath().substring(curDir.getAbsolutePath().lastIndexOf("\\")+1);
            t.a_attr.put("path", Helpers.normalizefilepath(curDir.getAbsolutePath(), false));
            t.a_attr.put("isdirectory", "true");
            t.isdirectory = true;
            File[] filesList = curDir.listFiles();
            for(File f : filesList){
                if(f.isDirectory())
                    t.children.add(getAllFiles(f));
                    sortdirtree(t.children);
                if(f.isFile()){
                    DirTree d = new DirTree();
                    d.text = f.getAbsolutePath().substring(f.getAbsolutePath().lastIndexOf("\\")+1);
                    d.icon = "fa fa-file";
                    d.a_attr.put("path", Helpers.normalizefilepath(f.getAbsolutePath(), false));
                    d.a_attr.put("isdirectory", "false");
                    d.isdirectory = false;
                    t.children.add(d);
                }
            }
            return t;
        }

    public Object disconnect(isparkrequest res){
        jsonresponse jr = new jsonresponse();
        try {  
            this.stop_vm();
            jr.json = "{}";
        } catch (Exception e){
            jr.errorlevel = 1;
            jr.message = e.toString();
            Helpers.log(e.toString(), "ClientSideController.disconnect");
        }
        return jr;
    }

    public void stop_vm(){        
        Application app = Application.getInstance();
        if(app.t != null) app.t.interrupt();

        if (Application.IsConnected()) { 
            EventThreadHandler eth = EventController.currentbreakthread;
            if(eth != null) {
                if(eth.eventinfo.UpdateUIThread != null) ((pagecontextcontroller.VarThread)eth.eventinfo.UpdateUIThread).interrupt = true;		
                while( eth.eventinfo.UpdateUIThread != null && eth.eventinfo.UpdateUIThread.isAlive() ){ }
                eth.eventinfo.UpdateUIThread = null;
            }
            this.factory.createheartbeathandler().clean();
            EventController.toggleeventrequests(-1,"disconnect");
            app.vm.resume();
            app.vm.dispose();
            app.vm = null;                
        }
    }

    public void UpdateWatches() {
        SocketController sc = new SocketController();
        socketresponse sr = new socketresponse();
        sr.callback = "updatewatches";
        sc.send(sr);
        Helpers.log("UPDATED WATCHES");
    }
    
    public void UpdateLine(Location loc, String path) {
        SocketController sc = new SocketController();
        socketresponse sr = new socketresponse();
        try {
            //String path = Helpers.getSystemPath(EventController.currentbreakthread, loc);
            updatelineresponse ulr = new updatelineresponse();
            sr.callback = "updateline";
            ulr.path = path;
            ulr.linenumber = Integer.toString(loc.lineNumber());
            sr.json = gson.toJson(ulr);
            sc.send(sr);
            Helpers.log("LINEUPDATED");
        } catch (Exception e) {
            Helpers.log(e.toString(), "ClientSideController.UpdateLine");
        }              
    }

    public void UpdateVariablesBar(PageContext pc){
        SocketController sc = new SocketController();
        socketresponse sr = new socketresponse();
        try {
            pagecontextcontroller pcc = new pagecontextcontroller();
            ArrayList<DirTree> r = new ArrayList<>();
            for(Enums.ScopeType s : Enums.ScopeType.values()){
                if(s != Enums.ScopeType.None){
                    Scope scope = pcc.getScope(pc,s);
                    DirTree d = new DirTree();
                    d.text = s.name();
                    d.icon = "false";
                    d.a_attr.put("scope",s.name().toLowerCase());
                    generatedirtree(scope.kvp, d, s.name().toLowerCase());
                    r.add(d);
                }
            }
            String rs = gson.toJson(r);
            sr.callback = "updatevartree";
            sr.json = rs;
            sc.send(sr);
            Helpers.log("UPDATED VARS PANE");
        } catch (Exception e) {
            e.printStackTrace();
            Helpers.log(e.toString(), "ClientSideController.updatevariablesbar");
        }
    }
        private void generatedirtree(HashMap<String, CustomValue> fl, DirTree dt, String scopeprepend) {
            boolean usebrackets = scopeprepend.length() != 0;
            for(String key : fl.keySet()) {
                DirTree r = new DirTree();
                r.a_attr.put("child","true");
                CustomValue cv = fl.get(key);
                if(cv.value != null) {
                    r.icon = "fa fa-angle-right";
                    r.text = key + ":" + cv.value;
                    if(usebrackets) r.a_attr.put("scope",scopeprepend + "[\"" + key.toLowerCase() + "\"]" );
                    else r.a_attr.put("scope", key.toLowerCase() );
                } else {
                    r.text = key;
                    r.icon = "fa fa-angle-double-right";
                    if(usebrackets) {
                        r.a_attr.put("scope",scopeprepend + "[\"" + key.toLowerCase() + "\"]" );
                        generatedirtree(cv.kvp, r, scopeprepend + "[\"" + key.toLowerCase() + "\"]");
                    } else{
                        r.a_attr.put("scope", key.toLowerCase() );
                        generatedirtree(cv.kvp, r, key.toLowerCase() );
                    }
                }
                dt.children.add(r);
            }
        }

    public void actionbuttonclick(isparkrequest res){
        String button = res.queryParams("action");
        new Thread(){ public void run(){
            EventController.actionbuttonclick(button);
         }}.run();	        
    }

    public void dump(isparkrequest res){
        String val = res.queryParams("action");
        String element = res.queryParams("elem");
        try {
            new Thread(){ public void run(){
                EventThreadHandler eth = EventController.currentbreakthread;
                eth.eventinfo.activedump = val;
                pagecontextcontroller pcc = new pagecontextcontroller();
                String r = pcc.old_dump(eth.eventinfo.activedump, eth.eventinfo.lastpc, eth.eventinfo.lastthread);
                if(r == null) r = "Could not load";
                UpdateDumpPane(r, element);   
            }}.run();                    
        } catch (Exception e) {
            Helpers.log(e.toString(), "ClientSideController.UpdateDumpPane");
        }     
    }
       
    public void UpdateDumpPane(String dumpval, String element) {
        SocketController sc = new SocketController();
        socketresponse sr = new socketresponse();
        try {      
            updatedumppane u = new updatedumppane();
            u.dumpval = dumpval;
            u.element = element;
            sr.callback = "updatedumppane";
            sr.json = gson.toJson(u);
            sc.send(sr);  
            Helpers.log("UPDATED DUMP PANE");    
        } catch (Exception e) {
            Helpers.log(e.toString(), "ClientSideController.UpdateDumpPane");
        }
    }
}