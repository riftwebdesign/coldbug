package com.riftwebdesign.coldbug.handlers;

import java.net.URI;
import java.net.URL;

import com.riftwebdesign.coldbug.engines.factory;
import com.riftwebdesign.coldbug.util.Enums;
import com.riftwebdesign.coldbug.util.Helpers;

public class browserhandler {

    factory factory = new factory();

    public URI geturl() throws Exception {
        com.riftwebdesign.coldbug.models.Application app = com.riftwebdesign.coldbug.models.Application.getInstance();
        boolean isdebug = app.config.get(Enums.Arguments.endpoint).contains(Enums.TextMessages.TestWebsiteRoot);
        boolean islog = app.config.get(Enums.Arguments.log).contains(Enums.Stringbool.t);
        URI uri = new URL( "http://localhost:" + app.getconfigprop("port") + "/?" + (isdebug ? "&debug=1" : "") + (islog ? "&log=1" : "") ).toURI();
        return uri;
    }

    public void openbrowser() {
        try {            
            java.awt.Desktop.getDesktop().browse( this.geturl() );    
        } catch (Exception e) {
            Helpers.log(e);
        }
    }
}