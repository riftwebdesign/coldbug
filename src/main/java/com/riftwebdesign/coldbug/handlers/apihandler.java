package com.riftwebdesign.coldbug.handlers;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.riftwebdesign.coldbug.models.Application;
import com.riftwebdesign.coldbug.pojo.url;
import com.riftwebdesign.coldbug.pojo.interfaces.ihttpurlconnection;
import com.riftwebdesign.coldbug.pojo.interfaces.iurl;
import com.riftwebdesign.coldbug.util.Helpers;

public class apihandler {

    private Gson gson = new Gson();
    private final String COOKIES_HEADER = "Cookie";
    public static String cookie = "";

    public String getendpoint() {
        return Application.getInstance().getconfigprop("endpoint");
    }

    public iurl geturl(String url) {
        return new url(url);
    }

    public ihttpurlconnection getconnection(String query) {
        String endpoint = this.getendpoint();
        String u = endpoint + "?" + query;
        //Helpers.log(u);
        iurl url = this.geturl(u);
        ihttpurlconnection conn = url.openConnection();
        return conn;
    }

    public void setconnectionproperties(ihttpurlconnection conn) {
        conn.setRequestProperty(this.COOKIES_HEADER, "");
        if (cookie.length() > 0) {
            conn.setRequestProperty(this.COOKIES_HEADER, apihandler.cookie);
        }
        conn.setRequestMethod("GET");
    }

    public String readconnection(ihttpurlconnection conn) { //todo test this method, but it might be a pain in the ass
        StringBuilder result = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        try {
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (Exception e) {
            Helpers.log(e);
        } finally {
            try {
                rd.close();
            } catch (Exception e) {
                Helpers.log(e);
            }
        }   
        return result.toString();
    }

    public void setcookie(String result){
        try{
            if(result.trim().length() > 0){
                JsonObject j = gson.fromJson(result, JsonObject.class);
                if(j.has("cfid")) 
                    apihandler.cookie = "cfid=" + j.get("cfid").getAsString() + "; cftoken=" + j.get("cftoken").getAsString() + ";";                    
            }
        } catch (Exception e){
            Helpers.log(e);
        } 
    }

    public String serviceRequest(String query) throws Exception {
        String result = null;
        ihttpurlconnection conn = this.getconnection(query);        
        this.setconnectionproperties(conn);
        result = this.readconnection(conn);  
        this.setcookie(result);    
        return result;
     }

     public JsonObject serviceRequestoJson(String query) throws Exception {
        String json = this.serviceRequest(query);  
        try{
            if(json.trim().length() > 0)
                return gson.fromJson(json, JsonObject.class);
        } catch (Exception e){
            Helpers.log(e);
        } 
        return null;
     }
}