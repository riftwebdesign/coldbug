package com.riftwebdesign.coldbug.engines;

import com.riftwebdesign.coldbug.ColdBug;
import com.riftwebdesign.coldbug.handlers.*;
import com.riftwebdesign.coldbug.pojo.*;
import com.riftwebdesign.coldbug.controllers.*;

public class factory {

    private static factory _factory;

    public factory getinstance(){
        if(_factory == null) _factory = this;
        return _factory;
    }
    public void setinstance(factory v){
        _factory = v;
    }

    public systemtrayhandler createsystemtray(){
        return new systemtrayhandler();
    }

    public ColdBug createcoldbug(){
        return new ColdBug();
    }

    public pagecontextcontroller createpagecontextcontroller(){
        return new pagecontextcontroller();
    }

    public clientsidehandler createclientsidehandler(){
        return new clientsidehandler();
    }

    public routehandler createroutehandler(){
        return new routehandler();
    }

    public browserhandler createbrowserhandler(){
        return new browserhandler();
    }

    public heartbeathandler createheartbeathandler(){
        return new heartbeathandler();
    }

    public jsonresponse createjsonresponse(){
        return new jsonresponse();
    }

    public login createlogin(){
        return new login();
    }

    public WatchController watchcontroller(){
        return new WatchController();
    }

    public virtualmachinehandler createvirtualmachinehandler(){
        return new virtualmachinehandler();
    }

    public connectresponse createconnectresponse(){
        return new connectresponse();
    }

    public socketresponse createsocketresponse(){
        return new socketresponse();
    }

    public filechangeresponse createfilechangeresponse(){
        return new filechangeresponse();
    }
}