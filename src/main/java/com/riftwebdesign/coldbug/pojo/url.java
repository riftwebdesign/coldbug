package com.riftwebdesign.coldbug.pojo;

import java.net.URL;

import com.riftwebdesign.coldbug.pojo.interfaces.ihttpurlconnection;
import com.riftwebdesign.coldbug.pojo.interfaces.iurl;
import com.riftwebdesign.coldbug.util.Helpers;

public class url implements iurl {

    private URL url = null;

    public url(String url) {
        try {
            this.url = new URL(url);
        } catch (Exception e) {
            Helpers.log(e);
        }
    }

    @Override
    public ihttpurlconnection openConnection() {
        ihttpurlconnection r = null;
        try {
            r = new httpurlconnection(this.url.openConnection());
        } catch (Exception e) {
            Helpers.log(e);
        }
        return r;
    }

}