package com.riftwebdesign.coldbug.pojo;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URLConnection;

import com.riftwebdesign.coldbug.pojo.interfaces.ihttpurlconnection;
import com.riftwebdesign.coldbug.util.Helpers;

public class httpurlconnection implements ihttpurlconnection {
    private HttpURLConnection con = null;

    public httpurlconnection(URLConnection con) {
        try {
            this.con = (HttpURLConnection) con;
        } catch (Exception e) {
            Helpers.log(e);
        }
    }

    @Override
    public void setRequestProperty(String key, String val) {
        this.con.setRequestProperty(key, val);
    }

    @Override
    public void setRequestMethod(String method) {
        try {
            this.con.setRequestMethod(method);
        } catch (Exception e) {
            Helpers.log(e);
        }
    }

    @Override
    public InputStream getInputStream() {
        InputStream r = null;
        try {
            r = this.con.getInputStream();
        } catch (Exception e) {
            Helpers.log(e);
        }
        return r;
    }
}