package com.riftwebdesign.coldbug.pojo;

import java.util.HashMap;
import java.util.Map;

import com.riftwebdesign.coldbug.pojo.interfaces.ivirtualmachine;
import com.riftwebdesign.coldbug.pojo.interfaces.ivmconnector;
import com.riftwebdesign.coldbug.pojo.interfaces.ivmconnectorargument;
import com.sun.jdi.connect.AttachingConnector;
import com.sun.jdi.connect.Connector;
import com.sun.jdi.connect.Connector.Argument;

public class vmconnector implements ivmconnector {

    public AttachingConnector connector = null;

    @Override
    public Map<String, ivmconnectorargument> defaultArguments() {
        HashMap<String,ivmconnectorargument> r = new HashMap<String,ivmconnectorargument>();
        Map<String,Connector.Argument> m = this.connector.defaultArguments();
        for(String key : m.keySet()){
            Argument a = m.get(key);
            vmargument va = new vmargument();
            va.argument = a;
            r.put(key,va);
        }
        return r;
    }

    @Override
    public ivirtualmachine attach(Map<String, ivmconnectorargument> args) throws Exception {
        virtualmachine vm = new virtualmachine();
        Map<String,Argument> m = new HashMap<String,Argument>();
        for(String key : args.keySet()){
            Argument a = ((vmargument)args.get(key)).argument;
            m.put(key, a);
        }   
        vm.vm = this.connector.attach(m);
        return vm;
    }

    @Override
    public String name() {
        return this.connector.name();
    }
}