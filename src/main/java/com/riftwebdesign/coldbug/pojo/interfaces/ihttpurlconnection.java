package com.riftwebdesign.coldbug.pojo.interfaces;

import java.io.InputStream;

public interface ihttpurlconnection {
    void setRequestProperty(String key, String val);
    void setRequestMethod(String method);
    InputStream getInputStream();
}