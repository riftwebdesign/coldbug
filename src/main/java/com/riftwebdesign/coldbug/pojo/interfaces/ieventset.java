package com.riftwebdesign.coldbug.pojo.interfaces;

import java.util.ArrayList;

public interface ieventset {
    public ArrayList<ievent> getEvents();
    public void resume();
    public boolean hasevent();
}