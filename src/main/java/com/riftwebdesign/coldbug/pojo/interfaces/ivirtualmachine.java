package com.riftwebdesign.coldbug.pojo.interfaces;

import java.util.List;

import com.sun.jdi.ReferenceType;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.Value;
import com.sun.jdi.request.EventRequestManager;

public interface ivirtualmachine {
    public void setDebugTraceMode(int arg);
    public void resume();
    public void dispose();
    public ieventqueue eventQueue();
    public EventRequestManager eventRequestManager();
    public List<ThreadReference> allThreads();
    public Value mirrorOf(String arg);
    public Value mirrorOf(long arg);
    public String name();
    public String description();
    public List<ReferenceType> allClasses();
}