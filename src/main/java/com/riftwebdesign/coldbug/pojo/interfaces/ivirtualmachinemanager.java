package com.riftwebdesign.coldbug.pojo.interfaces;

import java.util.List;

public interface ivirtualmachinemanager {
    public List<ivmconnector> attachingConnectors();
}