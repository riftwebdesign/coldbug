package com.riftwebdesign.coldbug.pojo.interfaces;

public interface isparkrequest {
    String uri();
    String queryParams(String key);
    String queryString();
}