package com.riftwebdesign.coldbug.pojo.interfaces;

import java.util.Map;


public interface ivmconnector {
    public Map<String, ivmconnectorargument> defaultArguments();
    public ivirtualmachine attach(Map<String, ivmconnectorargument> args) throws Exception;
    public String name();
}