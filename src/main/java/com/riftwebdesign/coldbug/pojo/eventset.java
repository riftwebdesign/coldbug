package com.riftwebdesign.coldbug.pojo;

import java.util.ArrayList;

import com.riftwebdesign.coldbug.pojo.interfaces.ievent;
import com.riftwebdesign.coldbug.pojo.interfaces.ieventset;
import com.sun.jdi.event.Event;
import com.sun.jdi.event.EventSet;

public class eventset implements ieventset {

    EventSet es = null;

    @Override
    public ArrayList<ievent> getEvents() {
        ArrayList<ievent> al = new ArrayList<ievent>();
        for(Event e : this.es){
            event thisevent = new event();
            thisevent.event = e;
            al.add(thisevent);
        }
        return al;
    }

    @Override
    public void resume() {
        this.es.resume();
    }

    @Override
    public boolean hasevent() {
        return this.es != null;
    }
}