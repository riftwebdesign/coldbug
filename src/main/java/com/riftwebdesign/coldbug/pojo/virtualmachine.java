package com.riftwebdesign.coldbug.pojo;

import java.util.List;

import com.riftwebdesign.coldbug.pojo.interfaces.ieventqueue;
import com.riftwebdesign.coldbug.pojo.interfaces.ivirtualmachine;
import com.sun.jdi.ReferenceType;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.Value;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.request.EventRequestManager;

public class virtualmachine implements ivirtualmachine {

    VirtualMachine vm = null;

    @Override
    public void setDebugTraceMode(int arg) {
        vm.setDebugTraceMode(arg);
    }

    @Override
    public void resume() {
        vm.resume();
    }

    @Override
    public void dispose() {
        vm.dispose();
    }

    @Override
    public ieventqueue eventQueue() {
        eventqueue eq = new eventqueue();
        eq.eq = vm.eventQueue();
        return eq;
    }

    @Override
    public EventRequestManager eventRequestManager() {
        return vm.eventRequestManager();
    }

    @Override
    public List<ThreadReference> allThreads() {
        return vm.allThreads();
    }

    @Override
    public Value mirrorOf(String arg) {
        return vm.mirrorOf(arg);
    }

    @Override
    public Value mirrorOf(long arg) {
        return vm.mirrorOf(arg);
    }

    @Override
    public String name() {
        return vm.name();
    }

    @Override
    public String description() {
        return vm.description();
    }

    @Override
    public List<ReferenceType> allClasses() {
        return vm.allClasses();
    }
}