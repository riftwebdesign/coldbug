package com.riftwebdesign.coldbug.pojo;

import com.riftwebdesign.coldbug.pojo.interfaces.isparkrequest;

import spark.Request;

public class sparkrequest implements isparkrequest {

    Request req = null;

    public sparkrequest(Request r){
        this.req = r;
    }

    @Override
    public String uri() {
        return this.req.uri();
    }

    @Override
    public String queryParams(String key) {
        return this.req.queryParams(key);
    }

    @Override
    public String queryString() {
        return this.req.queryString();
    }
}