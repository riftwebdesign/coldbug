package com.riftwebdesign.coldbug.pojo;

import java.util.ArrayList;

import com.riftwebdesign.coldbug.pojo.interfaces.ivirtualmachinemanager;
import com.riftwebdesign.coldbug.pojo.interfaces.ivmconnector;
import com.sun.jdi.VirtualMachineManager;
import com.sun.jdi.connect.AttachingConnector;

public class virtualmachinemanager implements ivirtualmachinemanager {

    public VirtualMachineManager vmmanager = null;

    @Override
    public ArrayList<ivmconnector> attachingConnectors() {
        ArrayList<ivmconnector> a = new ArrayList<>();
        for( AttachingConnector c : this.vmmanager.attachingConnectors() ){
            vmconnector connector = new vmconnector();
            connector.connector = c;
            a.add(connector);
        }
        return a;
    }

}