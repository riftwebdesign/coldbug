package com.riftwebdesign.coldbug;

import com.riftwebdesign.coldbug.engines.factory;
import com.riftwebdesign.coldbug.util.Enums;
import com.riftwebdesign.coldbug.util.Helpers;



public class ColdBug {

    public factory factory = new factory();

    public static void main(String[] args) {       

        if(args.length > 0){
            com.riftwebdesign.coldbug.models.Application app = com.riftwebdesign.coldbug.models.Application.getInstance();
            for(String a : args){             
                if(a.contains(Enums.Arguments.testsite)){
                    app.config.put(Enums.Arguments.endpoint,Enums.Paths.URL.TestLocalHost);
                }
                if(a.contains(Enums.Arguments.log)){                    
                    app.config.put(Enums.Arguments.log,Enums.Stringbool.t);
                }
            }
            for(String a : args){    
                Helpers.log(a);     
            }
        }

        new factory().getinstance().createcoldbug().start();
    }    

    public void start() {
        //todo find existing instance and kill it....
        this.factory.createsystemtray().addTrayIcon();
        this.factory.createbrowserhandler().openbrowser();
        this.factory.createroutehandler().register();        
        this.factory.createheartbeathandler().monitor();   
    }
}
