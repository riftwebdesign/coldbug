package com.riftwebdesign.coldbug.util;

import java.awt.Image;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.StringReference;

import javax.imageio.ImageIO;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.riftwebdesign.coldbug.ColdBug;
import com.riftwebdesign.coldbug.controllers.SocketController;
import com.riftwebdesign.coldbug.controllers.pagecontextcontroller;
import com.riftwebdesign.coldbug.models.Application;
import com.riftwebdesign.coldbug.models.DirectoryMap;
import com.riftwebdesign.coldbug.models.EventThreadHandler;
import com.riftwebdesign.coldbug.models.PageContext;
import com.riftwebdesign.coldbug.models.ProjectSettings;
import com.riftwebdesign.coldbug.pojo.socketresponse;
import com.riftwebdesign.coldbug.pojo.interfaces.isparkresponse;
import com.sun.jdi.Location;

import org.apache.commons.io.FilenameUtils;

import spark.utils.IOUtils;

public class Helpers {

    //private Gson gson = new Gson();  
    public class countertester {
          private Integer _val = 0;
          public Integer getVal(){
          return _val;
          }
          public void increment(){
          _val++;
          }
     }  

    public static <T> List<T> castList(Class<? extends T> clazz, Collection<?> c) {
        List<T> r = new ArrayList<T>(c.size());
        for(Object o: c)
          r.add(clazz.cast(o));
        return r;
    }

    public static <T> T castObject(Class<T> clazz, Object o) {
        try {
            return clazz.cast(o);
        } catch(ClassCastException e) {
            return null;
        }
    }

    public static void gsongeneric(String json) {
        Type listType = new TypeToken<ArrayList<HashMap<String, String>>>() { }.getType();
        Gson gson = new Gson();
        ArrayList<Map<String, String>> myList = gson.fromJson(json,listType);
        for (Map<String, String> m : myList) {
            Helpers.log(m.get("property"),"gson generic");
        }
        return;
    }

    public static void servicepost(){
        String urlParameters = "param1=a&param2=b&param3=c";
        byte[] postData = urlParameters.getBytes( StandardCharsets.UTF_8 );
        int postDataLength = postData.length;
        String request = "http://example.com/index.php";
        URL url;
        try {
            url = new URL(request);
            HttpURLConnection conn= (HttpURLConnection) url.openConnection();           
            conn.setDoOutput( true );
            conn.setInstanceFollowRedirects( false );
            conn.setRequestMethod( "POST" );
            conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
            conn.setRequestProperty( "charset", "utf-8");
            conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
            conn.setUseCaches( false );
            try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream()) ) {
                wr.write( postData );
            }
        } catch (Exception e) {
            e.printStackTrace();
		}        
    }

    
    public static void log_event(String msg,int level){ //todo convert to enum
        SocketController sc = new SocketController();
        socketresponse sr = new socketresponse();
        sr.callback = "addevent";
        sr.message = msg;
        sr.errorlevel = level;
        sc.send(sr);
    }
    public static String log(String message){
        boolean islog = Application.getInstance().config.get("log").contains("true");
        String fullClassName = Thread.currentThread().getStackTrace()[2].getClassName();
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
        String fulllog = message + "   [" + className + "." + methodName + "():" + lineNumber + "]";
        if(islog) System.out.println(fulllog);
        return fulllog;
    }
    public static String log(Exception e) {
        boolean islog = Application.getInstance().config.get("log").contains("true");
        String fullClassName = Thread.currentThread().getStackTrace()[2].getClassName();
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
        String fulllog = e.getMessage() + "   [" + className + "." + methodName + "():" + lineNumber + "]";
        if(islog) System.out.println(fulllog);
        return fulllog;
    }

    public static void log(String message, String loc) {
        boolean islog = Application.getInstance().config.get("log").contains("true");
        if(islog) Helpers.log(message, loc, false);
    }

    public static void log(String message, String loc, boolean withconsole) {
        boolean islog = Application.getInstance().config.get("log").contains("true");
        if(islog) System.out.println(message + " " + loc);
        // Helpers.consolelog(message,loc);
    }

    public static void log(Exception e, String loc, boolean withconsole) {
        boolean islog = Application.getInstance().config.get("log").contains("true");
        if(islog) System.out.println(e.toString() + " " + loc);
        // Helpers.consolelog(e.toString(),loc);
    }
    public static void log(Exception e, String loc) {
        //System.out.println(loc);
        boolean islog = Application.getInstance().config.get("log").contains("true");
        if(islog) e.printStackTrace();
        //Helpers.consolelog(e.toString(),loc);
    }
    public static void consolelog(String message, String loc){
        //JavaBridgeController.getInstance().consolelog(message + " " + loc);
    }
    public static String normalizefilepath(String filename, boolean dbl){
        //String b = filename;
        //if(dbl) filename = filename.replace("\\","\\\\");
        //filename = filename.replace("/", "\\" + (dbl ? "\\" : ""));
        //return filename.toLowerCase();
        return FilenameUtils.normalize(filename);
    }
    public static String normalize2(String a){
        //a = a.toLowerCase().replace("\\", "/");
        return FilenameUtils.normalize(a);
    }

    public static String removeroots(String a, ProjectSettings ps, boolean useremote){
        for(DirectoryMap m : ps.directorymap){
           a = a.replaceAll("(?i)" + Pattern.quote(normalize2(useremote ? m.remoteroot : m.root)),"");
        }
        return a;
    }

    public static String merge_onto(String path1, String path2){
        String normalpath = Helpers.normalize2(path1);
        String[] np2 = Helpers.normalize2(path2).split("/");

        String r = "";
        String search = "";

        for(String s : np2){
            if(s.equals("")) continue;
            r += "/" + s;
            if(!normalpath.contains(r)){
                break;
            }
            search += "/" + s;
        }       

        return normalpath.replace(search,"") + Helpers.normalize2(path2);
    }

    public static String getSystemPath(EventThreadHandler eth, Location loc) {
        String path = null;
        try {
            String root = Helpers.findroot(eth, loc.sourceName(), Application.getInstance().getActiveSettings(), true);
            String remoteroot = Helpers.findroot(eth,root, Application.getInstance().getActiveSettings(), false); //given the local root, get the remote
            path = Helpers.normalizefilepath( Helpers.merge_onto(root, Helpers.normalize2(loc.sourceName()).replace(Helpers.normalize2(remoteroot), "")), false);
        } catch (Exception e) {
            Helpers.log(e,"getSystemPath");
		}         
        return path;
    }

    public static String findroot(EventThreadHandler eth, String path, ProjectSettings ps, boolean useremote){
        String a = null;

        if(path != null) {
            path = normalize2(path);
            for (DirectoryMap m : ps.directorymap) {
                String workpath = normalize2(m.root);
                String returnpath = normalize2(m.remoteroot);
                if (useremote){
                    workpath = normalize2(m.remoteroot);
                    returnpath = normalize2(m.root);
                }
                boolean passes = path.toLowerCase().startsWith(normalize2(workpath).toLowerCase());
                if (passes) {                    
                    a = returnpath;
                    break;
                }                  
            }
            if(a == null && ps.directorymap.length > 0) {
                a = ps.directorymap[0].root;
            } 
        } else {
            pagecontextcontroller pcc = new pagecontextcontroller();
            String d = pcc.old_dump("cgi.cf_template_path", eth.eventinfo.lastpc, eth.eventinfo.lastthread);
            for (DirectoryMap m : ps.directorymap) {
                if (d.contains(m.remoteroot)) {                    
                    a = m.root;
                    break;
                }
            }
        }

        return a;
    }

    public static String findsourcecgi(PageContext pc, ThreadReference thread){
          pagecontextcontroller pcc = new pagecontextcontroller();
          //String d = pcc.old_dump("cgi.cf_template_path", pc, thread);
          String d = ((StringReference)pcc.old_evaluate("getCurrentTemplatePath()", pc, thread)).value();
          return d;
     }

    public static String getResourceFile(String file){
        file = (file.trim().charAt(0) == '/') ? file.replaceFirst("/", "") : file;
        ClassLoader classLoader = ColdBug.class.getClassLoader();                
        String f = null;
        try{
            InputStream is = classLoader.getResourceAsStream(file);            
            f = IOUtils.toString(is);
            byte[] byteText = f.getBytes(Charset.forName("UTF-8"));
            f= new String(byteText , "UTF-8");
            f = f.replace("ï»¿", "");
            is.close();
        }catch(Exception e){
            f = e.toString();
        } 
        return f;
    }

    public static Image getImageFile(String file){
        file = (file.trim().charAt(0) == '/') ? file.replaceFirst("/", "") : file;
        ClassLoader classLoader = ColdBug.class.getClassLoader();                
        Image img = null;
        try{
            InputStream is = classLoader.getResourceAsStream(file);
            img = ImageIO.read(is);   
            is.close();
        }catch(Exception e){
            Helpers.log(e);
        } 
        return img;
    }

    public String readjson(String file) {
        String r = "";
        try {
            Path p = Paths.get(file+".json");
            r = new String( Files.readAllBytes(p) );
		} catch (Exception e) {
            if(!file.equals("environment"))
                Helpers.log(e);
        }  
        return r;
    }

    public void savejsonfile(String file,String json){
        try {
            FileWriter fileWriter = new FileWriter(file+".json");
			fileWriter.write(json);
            fileWriter.flush();
            fileWriter.close();
        } catch (Exception e) {
			Helpers.log(e);
        }
    }

    public static void setTimeout(Runnable runnable, int delay){
        new Thread(() -> {
            try {
                Thread.sleep(delay);
                runnable.run();
            }
            catch (Exception e){
                System.err.println(e);
            }
        }).start();
    }

    public static void setResponseMime(String file, isparkresponse r){
        String[] s = file.split("[.]");
        String ext = s[s.length-1];
        switch(ext){
            case "js":
                r.header("content-type", "text/javascript");
                break;
            case "css":
                r.header("content-type", "text/css");
                break;
            case "json":
                r.header("content-type", "application/json");
                break;
            case "woff2":
                r.header("content-type", "font/woff2");
                break;
            case "woff":
                r.header("content-type", "font/woff");
                break;
            case "ttf":
                r.header("content-type", "application/octet-stream");
                break;
            case "png":
                r.header("content-type", "image/png");
                break;
            case "gif":
                r.header("content-type", "image/gif");
                break;
            
        }
    }

    public static String makejssafe(String content){
        content = content.replace("'", "\\'");
        content = content.replace(System.getProperty("line.separator"), "\\n");
        content = content.replace("\n", "\\n");
        content = content.replace("\r", "\\n");
        content = content.replace("\\u", "\\\\u");
        return content;
    }

}




