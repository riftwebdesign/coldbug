package com.riftwebdesign.coldbug.util;

public class Enums {
    public static class ErrorMessages {
        public static String UnsupportedTrayIcon = "System tray is not supported.";
    }
    public static class Paths {
        public static class Images {
            public static String TrayIcon = "/Images/icon.png";
        }
        public static class URL {
            public static String TestLocalHost = "http://"+TextMessages.TestWebsiteRoot+":8889/service";            
        }    
        public static class API {
            public static String publicpath = "/public";            
            public static String socket = "/socket";  
            public static String root = "/";  
            public static String files = "/Files"; 
            public static String scripts = "/Scripts"; 
            public static String libs = "/Lib"; 
            public static String styles = "/styles"; 
            public static String api = "/api"; 
        }   
        public static class FileNames {
            public static String index = "index.html";   
        }   
        public static class Mime {
            public static String css = "text/css";   
            public static String jsonnoformat = "json"; 
        }   
        public static class Headers {
            public static String contenttype = "content-type";   
        }   
    }
    public static class TextMessages {
        public static String TrayIconClose = "Close";
        public static String TestWebsiteRoot = "coldzapper";
        public static String AppName = "Coldbug";
    }
    public static class Arguments {
        public static String testsite = "testsite";
        public static String log = "log";
        public static String endpoint = "endpoint";
    }
    public static class Stringbool {
        public static String t = "true";
        public static String f = "false";
    }
    public enum ScopeType { /* changing this must be updated in BuggerPage.ts for user settings */
        None
        //,UndefinedScope
        ,Local
        ,Application
        ,Arguments
        ,Attributes
        ,Caller
        ,CGI
        ,Client
        ,Cluster
        ,Cookie
        ,Form        
        ,Request
        ,Server
        ,Session
        ,This        
        ,Thread
        ,URL 
        ,Variables        
    }
    public enum CustomValType {
        None
        ,String
        ,Double
        ,CFObject
        ,Object
        ,Array
    }
    public enum NextOperation {
        None
        ,StepInto
        ,StepOver
        ,Play
        ,StepOut
    }
    public enum EventLevel {
        success
        ,error
        ,info
        ,warning
        ,debug
    }
}
