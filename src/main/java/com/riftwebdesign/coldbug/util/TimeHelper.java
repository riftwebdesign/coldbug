package com.riftwebdesign.coldbug.util;

import java.util.concurrent.TimeUnit;

//TIME UTILITIES
public class TimeHelper {
    long starts;

    public static TimeHelper start() {
        return new TimeHelper();
    }

    private TimeHelper() {
        reset();
    }

    public TimeHelper reset() {
        starts = System.currentTimeMillis();
        return this;
    }

    public long time() {
        long ends = System.currentTimeMillis();
        return ends - starts;
    }

    public long time(TimeUnit unit) {
        return unit.convert(time(), TimeUnit.MILLISECONDS);
    }
}