package com.riftwebdesign.coldbug.util;

import com.google.gson.Gson;

public class gson {
    Gson g = new Gson();
    public String toJson(Object cr){
        return this.g.toJson(cr); 
    }
    public <T extends Object> T fromJson(String cr, Class<T> clazz) {
        return this.g.fromJson(cr,clazz); 
    }
}