namespace Coldbug {
    export class WatchVar {
        public text : string = null;
        public icon : string = null;
        public children : DirTree[] = [];
        public a_attr: { [key: string] : string } = {};
    }
}