namespace Coldbug {
    export class Config {
        public static get isdebugmode() : boolean {
            return Util.getQueryStringValue("debug") == '1';
        }
        public static get islogmode() : boolean {
            return Util.getQueryStringValue("log") == '1';
        }
        public static get appid(): string {
            return this.isdebugmode ? 'sandbox-sq0idp-sx5PQ2Bqw0rmEFtBSR2ECw' : '';
        }
        public static get locationid(): string {
            return this.isdebugmode ? 'CBASEFt2SokcgthGg2AdOxEyyewgAQ' : '';
        }
        public static squareendpoint: string = '';
        public static loginpage : string = "LoginPage";
        public static settingsfile : string = 'projectsettings';
        public static settingsform : string = '#settingsform';
        public static settingstable : string = '#settingstable';
        public static signinform : string = '#signinform';
        public static mainview : string = "#view";
        public static lowerview : string = "#lower";
        public static top : string = '#top';
        public static bottom : string = '#bottom';
        public static left : string = '#left';
        public static right : string = '#right';
        public static dumppane : string = '#dumppane';
        public static jstreelib : string = "./Lib/jstree/themes/default/style.css";
        public static defaultfilemap : { [key:string]: string } = {
            "cfc":"coldfusion"
            ,"cfm":"coldfusion"
            ,"js":"javascript"
            ,"html":"html"
            ,"css":"css"
            ,"less":"less"
            ,"xml":"xml"
            ,"ts":"typescript"
        };
        public static cssfiles : string[] = [
            "./Lib/jquery/jquery-ui.min.css"
            ,"./Lib/bootstrap/bootstrap.min.css"
            //,"./Lib/fontawesome/css/font-awesome.css"
            ,"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
            //,"./Lib/misc/jstree.min.css"
            ,"https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css"
            ,"./styles/style.css"
        ];
    }
}
