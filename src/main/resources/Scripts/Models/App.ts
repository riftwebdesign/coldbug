
namespace Coldbug {
    export class App {
        public debug = false;
        public settings : Settings[] = [];
        public activesetting: number = null; //index in settings array
        public stepline : number = null;
        public editor : any = null;
        public editors : Editor[] = [];
        public editorfilepath : string = null;
        public uniqueid : number = 0;
        public currentpage : IPage;      


        public init() {
            
            this.debug = Config.isdebugmode;
            console.log(Config.islogmode);
            if(!Config.islogmode) {
                var $this = this;
                (<any>console) = {};
                console.log = function(){};
            }     
             
            this.loadcss();

            this.registersettings();

            var s : Socket = new Socket();
            s.connect();
            /*var $window : any = $( window );
            $window.on("unload", function(e) {
                bridgecontroller.logout();
                bridgecontroller.disconnect(null);
                bridgecontroller.kill();
                return 
            });*/

            Util.loadpage( new TemplatePage() );            
        }

        public registeractions():void{
            ActionManager.init();

            ActionManager.add(new SideBarButton());
            ActionManager.add(new ConnectButton());
            ActionManager.add(new StopButton());
            ActionManager.add(new StepInButton());
            ActionManager.add(new StepOverButton());
            ActionManager.add(new StepOutButton());
            ActionManager.add(new PlayButton());
            ActionManager.add(new BreakButton());

            ActionManager.register();
        }

        public registernavlinks() : void{
            NavLinkManager.init();

            NavLinkManager.add(new CloseLink());
            NavLinkManager.add(new ProjectLink());
            //NavLinkManager.add(new AccountLink());
            //NavLinkManager.add(new SupportLink());
            //NavLinkManager.add(new LogoutLink());

            NavLinkManager.register();
        }

        public getactiveeditor() : any {
            var r : any = null;

            $.each(app.editors, function (i, val : Editor) {
                if(Util.normalCompareString(app.editorfilepath,val.path)) {
                    r = val.editor;
                };
            });

            return r;
        }

        public haseditor() : boolean {
            var r : boolean = false;

            $.each(app.editors, function (i, val : Editor) {
                if(app.editorfilepath == val.path){
                    r = true;
                };
            });

            return r;
        }

        public geteditor(path:string) : Editor {
            path = path;
            var r : Editor = null;

            $.each(app.editors, function (i, val : Editor) {
                if(path == val.path){
                    r = val;
                };
            });

            return r;
        }

        public hasopenfile() : boolean {
            var r : boolean = false;

            $.each(app.getactivesetting().openfiles, function (i, val : OpenFile) {
                if(app.editorfilepath == val.path){
                    r = true;
                };
            });

            return r; 
        }

        public getopenfile(path:string) : OpenFile {
            path = path;
            var r : OpenFile = null;

            $.each(app.getactivesetting().openfiles, function (i, val : OpenFile) {
                if(path == val.path){
                    r = val;
                };
            });

            return r;
        }

        

        public getactivesetting() : Settings {
            return this.settings[this.activesetting];
        }

        public registersettings(){

            var $this = this;

            if(bridgecontroller.readjson) {
                this.refreshSettings();
            } else {
                setTimeout(function(){
                    $this.registersettings();
                },500);
            }
        }

        public setSettings(settings:any) : void {
            //console.log(settings);
            if (settings) {
                app.settings = settings;
            }
        }

        public refreshSettings() : void {
            Util.readjsonfile(Config.settingsfile, this.setSettings);
        }

        public loadcss() {
            for(var i in Config.cssfiles){
                Util.addcssfile(Config.cssfiles[i]);
            }
        }
    }
}