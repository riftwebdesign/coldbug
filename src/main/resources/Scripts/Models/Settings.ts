namespace Coldbug {
    export class Settings {
        public name : string = null;
        public root : string = null;
        public remoteroot : string = null;
        public host : string = null;
        public port : number = null;
        public suspend : number = 1;
        public stoponchange : number = 1;
        public breakpoints : Breakpoint[] = [];
        public directorymap : DirectoryMap[] = [];
        public watchexclusions : WatchExclusion[] = [];
        public events : NotifyEvent[] = [];
        public openfiles : OpenFile[] = [];
        public opentree : OpenTree[] = [];
        public excludescopes : string[] = [];
        public watchvars : WatchVar[] = [];
    }
}