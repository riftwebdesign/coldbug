declare var app: Coldbug.App;
declare var bridgecontroller: any;
declare var ace:any;


function var_dump(obj) {
    var obj_members = "";
    var sep = "";
    for (var key in obj) {
        obj_members += sep + key + ":" + obj[key];
        sep = ", ";
    }
    return ("[" + obj_members + "]");
}
function getMethods( obj ) {
var ret = [];
for ( var prop in obj ) {
    //  typeof is inconsitent, duck type for accuracy
    //  This could also be written to follow the internal IsCallable algorithm
    //  http://es5.github.com/#x9.11
    if ( obj[ prop ] && obj[ prop ].constructor &&
        obj[ prop ].call && obj[ prop ].apply ) {
    ret.push( prop );
    }
}
return ret;
}
function getOwnMethods( obj ) {
var props = Object.getOwnPropertyNames( obj );
return props.filter(function( prop ) {
    return obj[ prop ] && obj[ prop ].constructor &&
            obj[ prop ].call && obj[ prop ].apply;
});
}
/*
window.onerror = function(v, url, line, col, error) {
    console.log(v + " : " + line);
};
var console = <Console>(function(c : Console){
    return {
        error: function(v){
            if(app && app.debug) (<any>document.body).append(v + "      ");

            c.error('_original_');
            c.error(v);
        },
        log: function(v){
            if(app && app.debug) (<any>document.body).append(v + "      ");

            c.log('_original_');
            c.log(v);
        }
    };
}(window.console));
*/
function resizeeditor(){
    var containerheight = $('#center').height();
    var height = $('#editortabs ul').height();

    //console.log((height/containerheight)*100-100);
    //$('.editor').height( ((height/containerheight)*100-100) + "%");
    $('.editor').height(containerheight - height);
}



window.onresize = function(event) {
    resizeeditor();
};
