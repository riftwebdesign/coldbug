namespace Coldbug {
    export class User {
        public firstname : string = null;
        public lastname : string = null;
        public expired : boolean = true;
        public nextbillingdate : Date = null;
        public email : string = null;
        public newsletter : number = 0;
        public validated : number = 0;
        public hascard : boolean = false;
    }
}