namespace Coldbug{
    export class Breakpoint {
        public path : string = null;
        public line : number = null;
        public marker : number = null;
        public hasline : boolean = false;
    }
}