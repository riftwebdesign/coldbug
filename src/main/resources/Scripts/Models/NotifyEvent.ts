namespace Coldbug {
    export class NotifyEvent {
        public message : string = null;
        public eventlevel : EventLevel = EventLevel.success;
    }
}