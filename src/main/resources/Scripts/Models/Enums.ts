namespace Coldbug {
    export enum EventLevel {
        success
        ,error
        ,info
        ,warning
        ,debug
    }
}