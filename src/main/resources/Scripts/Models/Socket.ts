namespace Coldbug{
    export class SocketResponse{
        public message : string;
        public errorlevel : number; //0 success 1 error 2 info 3 warning
        public json : string;
        public callback : string;
        public jsonobj : any;
    }
    export class Socket{
        public webSocket : WebSocket;    
        public stopReconnect : boolean = false;
        public onclose(){
            var $this = this;
            if(this.stopReconnect) return;
            setTimeout($this.connect, 1000);
        }    
        
        public connect(){
            var $this = this;
            this.webSocket = new WebSocket("ws://" + location.hostname + ":" + location.port + "/socket");
            this.webSocket.onmessage = function (msg) { 
                var sr : SocketResponse = JSON.parse(msg.data);
                //console.log(sr.callback);
                sr.jsonobj = JSON.parse(sr.json);
                switch(sr.callback){
                    case "enablebuttons":
                         Util.enableBuggingButtons();
                         break;
                    case "vmconnected":
                        //console.log((<any>sr.jsonobj).statechange)
                        Coldbug.ConnectButton.handle_connect(sr.jsonobj);
                        break;
                    case "updateline":
                        Coldbug.BuggerPage.loadfile(sr.jsonobj.path,sr.jsonobj.linenumber,true);
                        break;
                    case "updatevartree":
                        Coldbug.BuggerPage.updatevartree(sr.jsonobj);                        
                        break;
                    case "updatewatches":
                        Util.getwatch(Coldbug.BuggerPage.staticloadwatch);
                        break;
                    case "updatedumppane":                        
                        Coldbug.BuggerPage.updatedumppane(sr.jsonobj.dumpval,sr.jsonobj.element);
                        break;
                    case "updateprojecttree":                       
                        Coldbug.BuggerPage.staticloadtree(JSON.parse(sr.jsonobj.tree));
                        break;
                    case "updateprojecttree_folder":                       
                        Coldbug.BuggerPage.staticloadtree(JSON.parse(sr.jsonobj.tree),sr.jsonobj.folder);
                        break;
                    case "refreshsettings":                        
                        app.refreshSettings();
                        break;
                    case "addevent":
                        //console.log(sr);
                        Util.addevent(sr.message, sr.errorlevel);
                        break;
                    case "kill":  
                        $this.stopReconnect = true;  
                        var $window : any = $( window );
                        $window.off("unload");          
                        $('body').empty().append("Only 1 debugging session can be active at a time.");
                        break;
                    
                }            
            };
            this.webSocket.onclose = this.onclose;           
        }
    }
}