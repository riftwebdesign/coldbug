namespace Coldbug {
    //UTIL
    // Create Base64 Object
    // @ts-ignore    
    var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
    
    export class Util {
        public static disableBuggingButtons() : void{
            $("#playbutton, #stepinbutton, #stepoutbutton, #stepoverbutton").addClass('disabled');
        }
        public static enableBuggingButtons() : void{
            $("#playbutton, #stepinbutton, #stepoutbutton, #stepoverbutton").removeClass('disabled');
        }
        public static normalCompareString(path1:string,path2:string){            
            return path1.replace(/\\\\/g, '\\') == path2.replace(/\\\\/g, '\\');
        }        
        public static normalPath(path:string) : string{
            return path;
            //return path.replace(/\\\\/g, '\\');
        }
        public static sendAjaxRequest(_params: Object, _callback: Function, _type?: string, _url?: string, returnfull? : boolean) {
            returnfull = returnfull || false;
            _type = _type || "GET";
            _url = _url || "/api";
            var request = $.ajax({
                type: _type,
                url: _url,                
                data: _params,
                contentType: 'json'
            });
            console.log("Started", _url);
            request.done(function(res) {
                console.log("Complete", this.url);
                if(_callback && returnfull)
                    _callback(JSON.parse(res));
                else if(_callback && res.json) 
                    _callback(JSON.parse(res.json));
                else if(_callback) _callback();
            });
            //console.log(_url);
            request.fail(function(jqXHR, textStatus) {
                console.log(jqXHR);
                //console.log(this.url);
                if(_callback) _callback({ err: true, message: "Request failed: " + textStatus });
            });        
        }

        public static addopentreeandsave(id:string,type:string) : void {
            var flag : boolean = false;
            var trees = app.getactivesetting().opentree;
            $.each(trees,function(i,a){
                if(this.id == id) flag = true;
            });
            if(!flag) {
                var ot = new OpenTree();
                ot.id = id;
                ot.type = type;
                app.getactivesetting().opentree.push(ot);
                Util.savejsonfile(Config.settingsfile,JSON.stringify(app.settings));
            }
        }

        public static deleteopentreeandsave(id:string,type:string) : void {
            var trees = app.getactivesetting().opentree;
            var index = -1;
            $.each(trees,function(i,a){
                if(this.id == id) index = i;
            });
            app.getactivesetting().opentree.splice(index,1);
            Util.savejsonfile(Config.settingsfile,JSON.stringify(app.settings));
        }

        public static base64encode(val:string) : string{
            //return btoa(val);
            //return Base64.encode(val);
            return val;
        }
        public static base64decode(val:string) : string{
            //return atob(val);
            return Base64.decode(val);
            return val;
        }

        public static togglesave(path:string){
            /*var editor = app.geteditor(path);
            var openfile = app.getopenfile(path);
            var code = editor.editor.getValue();
            var $savebutton = $(Config.top + ' #savebutton');
            var base64code = Util.base64encode(code);
            if(base64code == openfile.disccontents)
                $savebutton.addClass('disabled');
            else $savebutton.removeClass('disabled');

            openfile.changedcontents = base64code;
            Util.savejsonfile(Config.settingsfile,JSON.stringify(app.settings));*/
        }
        public static notify(msg : string, val : string) : void {
            var body = $('body');
            body.prepend('<div class="alertmessage alert alert-' + val + '">'+msg+'</div>');
            setTimeout(function(){ $('.alertmessage').last().remove() }, 5000);
        }
        public static addevent(msg : string, level : EventLevel) : void {
            var e = new NotifyEvent();
            e.message = new Date().toLocaleString() + " : " + msg;
            e.eventlevel = level;
            var es = app.getactivesetting().events;
            if(es.length >= 20) {
                es.splice(1,1);
            }
            es.push(e);
            Util.savejsonfile(Config.settingsfile,JSON.stringify(app.settings));
            Util.renderevents();
        }
        public static renderevents() : void {
            var $events = $('#events');
            $events.empty();
            if(app.getactivesetting().events == null){
                app.getactivesetting().events = [];
            }
            var es = app.getactivesetting().events;
            for(var i = es.length -1; i >= 0; i--){
                var val : NotifyEvent = es[i];
                $events.append('<div style="white-space: nowrap;" class="' + EventLevel[val.eventlevel] + '">' + val.message + '</div>');
            }
        }
        public static renderbreakpoints() : void {
            var $breaks = $('#breakpoints');
            $breaks.empty();
            if(app.getactivesetting().breakpoints == null){
                app.getactivesetting().breakpoints = [];
            }
            var es = app.getactivesetting().breakpoints;
            var orderedlist = {};

            for(var i = es.length -1; i >= 0; i--){
                var val : Breakpoint = es[i];
                if(!orderedlist[val.path]) orderedlist[val.path] = [];
                orderedlist[val.path].push(val.line);
            }

            for (var key in orderedlist) {
                if (orderedlist.hasOwnProperty(key)) {
                    var lines : number[] = orderedlist[key];
                    lines.sort();
                    for(let i in lines){
                        var line : number = lines[i];
                        $breaks.append('<div style="white-space: nowrap;" class="breakpoint" data-path="' + key + '" data-line="' + line + '">' + line + ' : ' + key + '</div>');
                    }
                }
            }
        }
        public static getresource(path:string) : string {
            var r = bridgecontroller.getresource(path);
            return r;
        }
        public static getprojecttree(callback:Function) : string {
            return bridgecontroller.getprojecttree(callback);
        }
        public static getwatch(callback:Function) : string {
            return bridgecontroller.getwatch(callback);
        }
        public static setbreakpoints(json: string, callback?:Function): void {
            bridgecontroller.setbreakpoints(json,callback);
        }
        public static registerhandler(handle: string, elem: string, func: Function): void {
            $('body').off(<any>handle,elem).on(<any>handle,elem,func);
        }
        public static savefile(path:string,contents:string) : void{
            bridgecontroller.savefile(path,contents);
        }
        public static savejsonfile(path:string,contents:string,callback?:Function) : void{
            bridgecontroller.savejson(path,contents,callback);
        }
        public static loadfile(path:string,callback:Function) : string{
            return bridgecontroller.loadfile(path,callback);
        }
        public static logout() : void{
            return bridgecontroller.logout();
        }
        public static deletejsonfile(path:string) : boolean{
            return bridgecontroller.deletejsonfile(path);
        }
        public static openproject(index:number,callback:Function) : boolean{
            var r = bridgecontroller.openproject(index,callback);
            return r;
        }
        public static readjsonfile(path:string,callback:Function) : string{
            return bridgecontroller.readjson(path,callback);
        }
        public static serializeForm($form) {
            var json = [];
            jQuery.map( $form.serializeArray(), function(n, i){
                json.push('"' + escape(n['name']) + '":"' + escape(n['value']) + '"');
            });
            return JSON.parse('{' + json.join(',') + '}');
        };
        public static addcssfile(file:string) : void {
            $('<link href="'+file+'" rel="stylesheet" type="text/css">').appendTo("head");
        }
        public static clearline(line:number) : void {
            if(line > 0)
                app.getactiveeditor().getSession().removeMarker(line);
        }
        public static login(email:string,password:string) : boolean {
            return bridgecontroller.login(email,password);
        }
        public static register(email:string,password:string,callback?:Function) : boolean {
            return bridgecontroller.register(email,password,callback);
        }
        public static loadpage(page : IPage, args? : any[]) : void {
            if (app.currentpage && typeof (<any>app.currentpage).unload !== "undefined") { 
                (<any>app.currentpage).unload();
            }
            page.init( args );
            app.currentpage = page;
            //(<any>$('[data-toggle="tooltip"]')).data("animation","false");
            try{
                (<any>$('[data-toggle="tooltip"]')).tooltip('destroy');
            }catch(e){}
            (<any>$('[data-toggle="tooltip"]')).tooltip({
                trigger : 'hover'
                ,animation: false
            });
            (<any>$('[data-toggle="tooltip"]')).off('click').on('click', function () {
                try{ 
                    (<any>$(this)).tooltip('hide');
                }catch(e){}
            });
        }
        public static loadview(page,container,callback?) : void { //depreicated for loadpage
            $('#'+container).load('Views/'+page+'.html', callback);
        }
        public static connect(callback:Function) : void {
            bridgecontroller.connect(callback);
        }
        public static disconnect(callback:Function) : boolean {
            var r : boolean = bridgecontroller.disconnect(callback);            
            return r;
        }
        public static getQueryStringValue (key) {  
            return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
        }  
        public static loadScript( url, callback ) {
            var script : any = document.createElement( "script" );
            script.type = "text/javascript";
            if(script.readyState) {
                script.onreadystatechange = function() {
                if ( script.readyState === "loaded" || script.readyState === "complete" ) {
                    script.onreadystatechange = null;
                    callback();
                }
                };
            } else {
                script.onload = function() {
                callback();
                };
            }

            script.src = url;
            document.getElementsByTagName( "head" )[0].appendChild( script );
        }
    }

}

var PageTitleNotification = {
    Vars:{
        OriginalTitle: document.title,
        Interval: null
    },    
    On: function(notification, intervalSpeed){
        var _this = this;
        _this.Vars.Interval = setInterval(function(){
             document.title = (_this.Vars.OriginalTitle == document.title)
                                 ? notification
                                 : _this.Vars.OriginalTitle;
        }, (intervalSpeed) ? intervalSpeed : 1000);
    },
    Off: function(){
        clearInterval(this.Vars.Interval);
        document.title = this.Vars.OriginalTitle;   
    }
}