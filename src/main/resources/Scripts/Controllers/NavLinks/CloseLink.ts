namespace Coldbug {
    export class CloseLink implements ILeftNav {
        public icon : string = "";
        public clickhandler : Function = () => {
            $('#leftsidenav').width(0);
        };
        public id : string = "closebtn";
        public name : string = "&times;";
        public href : string = "javascript:void(0)";
    }
}