namespace Coldbug {
    export class SupportLink implements ILeftNav {
        public icon : string = "";
        public clickhandler : Function = () => {
            Util.loadpage( new TemplatePage(  new SupportPage() ) );
        };
        public id : string = "support";
        public name : string = "Support";
        public href : string = "#";
    }
}