namespace Coldbug{
    export class NavLinkManager {
        private static links : ILeftNav[] = [];

        public static init() : void{
            this.links = [];
        }
        public static add(link:ILeftNav) : void{
            this.links.push(link);
        }
        public static register() : void{
            var r = "";

            Util.registerhandler('click','#leftsidenav a',function () {
                $('#leftsidenav').width(0);
            });

            for(var l of this.links){
                Util.registerhandler('click',"#leftsidenav #"+l.id,l.clickhandler);
                r += `<a id="${l.id}" href="${l.href}">${l.name}<a/>`;
            }

            $("#leftsidenav").empty().append(r);
        }
    }
}