namespace Coldbug {
    export class AccountLink implements ILeftNav {
        public icon : string = "";
        public clickhandler : Function = () => {
            Util.loadpage( new TemplatePage(  new AccountPage() ) );
        };
        public id : string = "account";
        public name : string = "Account";
        public href : string = "#";
    }
}