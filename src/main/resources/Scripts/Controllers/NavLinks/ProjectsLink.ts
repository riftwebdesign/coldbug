namespace Coldbug {
    export class ProjectLink implements ILeftNav {
        public icon : string = "";
        public clickhandler : Function = () => {
            Util.loadpage( new TemplatePage() );
        };
        public id : string = "projects";
        public name : string = "Projects";
        public href : string = "#";
    }
}