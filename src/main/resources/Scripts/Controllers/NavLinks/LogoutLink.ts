namespace Coldbug {
    export class LogoutLink implements ILeftNav {
        public icon : string = "";
        public clickhandler : Function = () => {
            Util.logout();
            Util.loadpage( new LoginPage() );
        };
        public id : string = "logout";
        public name : string = "Logout";
        public href : string = "#";
    }
}