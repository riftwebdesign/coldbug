

namespace Coldbug {   

    declare var user:User;

    export class SettingsPage implements IShowHide, IPage {        

        public template : string = `
            <div class="container">
                <br/>
                <div>
                    <a data-toggle="tooltip" data-placement="top" title="Click to add a new project setting." href="#" id="addsettingsbutton" class="addsettingbutton"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                </div>
                <div id="settingstable">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Actions</th>
                                <th>Name</th>
                                <th>Host</th>
                                <th>Port</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
            
                </div>
            </div>
        `;

        public init() : void {

            console.log(user);

            /*if(user.expired){
                Util.loadpage( new TemplatePage(  new AccountPage() ) );
                return;
            }*/

            var $this = this;

            $(Config.lowerview).empty().append(this.template);
            app.activesetting = null; //resets active setting
            app.editors = [];
            Util.disconnect(null);

            if (app.settings.length > 0) {
                //todo make a template manager that can inject variables, IE centralized/uniform templates
                $.each(app.settings, function (i, val : Settings) {
                    $(Config.settingstable + ' tbody').append(`
                    <tr>
                        <td>
                            ${ val.directorymap.length == 0 ? '' : '<a data-toggle="tooltip" data-placement="top" title="Click to open this project setting." href="#" class="openbutton"><i class="fa fa-folder-open" aria-hidden="true"></i></a> |' }
                            <a data-toggle="tooltip" data-placement="top" title="Click to edit this project setting." href="#" data-rowid="${i}" id="editbutton" class="editbutton"><i class="fa fa-pencil" aria-hidden="true"></i></a> |
                            <a data-toggle="tooltip" data-placement="top" title="Click to delete this project setting." href="#" class="deletebutton"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                        <td>${val.name} <input id="row_${i}" type="hidden" value='${JSON.stringify(val)}'></td>
                        <td>${val.host}</td>
                        <td>${val.port}</td>
                    </tr>
                `);
                });
            } else {
                $(Config.settingstable + ' tbody').append('<tr><td colspan="6">No settings found</td></tr>');
            }

            Util.registerhandler('click', '#editbutton', function(){
                var rowid = $(this).data('rowid');
                Util.loadpage( new AddEditSettingsPage(), [rowid] );
            });

            Util.registerhandler('click', '#addsettingsbutton', function(){
                Util.loadpage( new AddEditSettingsPage() );
            });

            Util.registerhandler('click',Config.settingstable + ' .openbutton',function () {
                var i = $(this).closest('tr').index();
                var obj = JSON.parse( $(Config.settingstable + ' #row_' + i ).val().toString() );
                var r = Util.openproject(i,$this.loadTemplate);
                app.activesetting = i;                    
            });
            Util.registerhandler('click',Config.settingstable + ' .deletebutton',function () {
                var i = $(this).closest('tr').index();
                app.settings.splice(i, 1);
                Util.savejsonfile(Config.settingsfile,JSON.stringify(app.settings));
                Util.loadpage( new TemplatePage() );
            });
            Util.registerhandler('click',Config.settingsform + ' .deletebutton',function () {
                Util.deletejsonfile(Config.settingsfile);
                Util.loadpage( new TemplatePage() );
            });
            Util.registerhandler('click','.breakpoint',function () {
                var path : string = $(this).data('path');
                var line : number = parseInt($(this).data('line'));
                BuggerPage.loadfile(path,line,false);
            });
            

            this.showhide();
        }

        public loadTemplate() : void {
            Util.loadpage( new BuggerPage() );
            Util.setbreakpoints( JSON.stringify(app.getactivesetting().breakpoints) );
        }

        public showhide(){
            $('.menuitem:not(:first)').hide();
        }
    }
}