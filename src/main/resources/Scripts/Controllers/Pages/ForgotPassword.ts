namespace Coldbug {
    export class ForgotPasswordPage implements IPage {

        public template : string = `
            <div class="centered">
                <form id="resetpasswordform">
                    <h2 class="form-signin-heading">Forgot Password</h2>
                    <label for="email" class="sr-only">Email Address</label>
                        <input type="email" id="email" class="form-control" placeholder="Email Address" autofocus>
                    <button id="forgotpassword" class="btn btn-lg btn-primary btn-block" type="button">Send</button>
                    <br /><br />
                    <button id="login" class="btn btn-lg btn-secondary btn-block" type="button">Login</button>
                </form>
            </div>
        `;

        constructor(){}

        public init(){
            var $this = this;
            $(Config.mainview).empty().append(this.template);

            Util.registerhandler('click','#resetpasswordform #forgotpassword',function () {
                var email = $('#resetpasswordform #email').val().toString();
                if(email.length > 0) bridgecontroller.forgotpassword(email);
                Util.loadpage( new LoginPage() );
            });
            Util.registerhandler('click','#resetpasswordform #login',function () {
                Util.loadpage( new LoginPage() );
            });
        }

    }
}