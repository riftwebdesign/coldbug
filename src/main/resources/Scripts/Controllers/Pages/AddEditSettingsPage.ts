namespace Coldbug {
    export class AddEditSettingsPage implements IPage {

        public directorymappinginfo = `Directory mappings are used to load local project files and map them to the local/remote server. When the server is running on your local system, the local and remote will be the same. Any 'virtual mappings' inside of the Administrator panel will need to be included so breakpoints, and line stepping will be able to load the file.`;
        public watchexclusionsinfo = `Exclusions are added for files and/or directories that are constantly modified, such as log files. This uses substring matching. IE, .log will match all files with the extension .log.`;
        public suspendinfo = `The suspend policy triggered on an occurence of a Breakpoint or Step event. None would cause no Break to happen. Active Thread is only the current thread, allowing other threads to complete, that are not interacting with a breakpoint. All will suspend the entire server.`;
        public stoponchangeinfo = `This is an optional feature which will stop an active connection to the server when a file change is detected. This feature is only beneficial when log, debug, and regular changing files unrelated to code changes are added to the watch exclusion list.`;
        public template : string = `
            <div class="container">
                <br />
                <div>
                    <a data-toggle="tooltip" data-placement="top" title="Back to projects page." href="#" id="backtosettings"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                </div>                
                <div style="margin: 0px auto; width: 75%;">
                    <form id="settingsform">
                        <div>
                            <h2 class="form-signin-heading">Settings</h2>
                            <label for="name" class="sr-only">Project Name</label> <input data-toggle="tooltip" data-placement="top" title="The name of your project." type="text" id="name" class="form-control" placeholder="Project Name" autofocus>
                            <label for="host" class="sr-only">Host</label> <input data-toggle="tooltip" data-placement="top" title="The 'target' host to connect to. This is the connection address of the server you want to debug. IE, 127.0.0.1 or localhost." type="text" id="host" class="form-control" placeholder="IP Address">
                            <label for="port" class="sr-only">Port</label> <input data-toggle="tooltip" data-placement="top" title="The 'target' port to connect to. This is the port used to connect to the server you want to debug. IE, 80 or 8888." type="text" id="port" class="form-control" placeholder="Port">
                            <label for="suspend" class="sr-only">Suspend</label> <select data-toggle="tooltip" data-placement="top" title="${this.suspendinfo}" id="suspend" class="form-control">
                                <option value="0">Suspend No Threads</option>
                                <option value="1">Suspend Active Thread</option>
                                <option value="2">Suspend All Threads</option>
                            </select>
                            <label for="stoponchange" class="sr-only">Stop On Change</label> <select data-toggle="tooltip" data-placement="top" title="${this.stoponchangeinfo}" id="stoponchange" class="form-control">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                            <input type="hidden" id="index" value="-1">
                        </div>
                        <br/>
                        <div id="directorymap">
                            <h2 class="form-signin-heading">Directory Map</h2>
                            <a href="#" class="adddirectorybutton"><i data-toggle="tooltip" data-placement="top" title="${this.directorymappinginfo}" class="fa fa-plus-circle" aria-hidden="true"></i></a>
                        </div>
                        <br/>
                        <div id="watchexclusions">
                            <h2 class="form-signin-heading">Watch Exclusions</h2>
                            <a href="#" class="addwatchexclusionbutton"><i data-toggle="tooltip" data-placement="top" title="${this.watchexclusionsinfo}" class="fa fa-plus-circle" aria-hidden="true"></i></a>
                        </div>
                        <div>
                            <br />
                            <br />
                            <button class="addbutton btn btn-lg btn-primary btn-block" type="button">Add</button>
                        </div>
                    </form>
                </div>

                <div id="directoryclone" style="display:none;">
                    <div class="input-group directoryrow">
                        <input type="text" class="form-control root" placeholder="Local Directory"/>
                        <span class="input-group-addon">&nbsp;</span>
                        <input type="text" class="form-control remoteroot" placeholder="Remote Directory"/>
                        <span class="input-group-addon"><a href="#" class="deletedirectorybutton"><i class="fa fa-trash-o" aria-hidden="true"></i></a></span>
                    </div>
                </div>

                <div id="watchexclusionclone" style="display:none;">
                    <div class="input-group watchexclusionrow">
                        <input type="text" class="form-control path" placeholder="Exclusion File or Directory"/>
                        <span class="input-group-addon"><a href="#" class="deletewatchexclusionbutton"><i class="fa fa-trash-o" aria-hidden="true"></i></a></span>
                    </div>
                </div>

                <div  style="margin: 0px auto; width: 75%; text-align: center;">
                    Java Opts: -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=<My Port>
                    <br/>
                    Example: -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=7999
                </div>

            </div>
        `;

        public init(args : any[]) {

            var index = args ? args[0] : -1;

            $(Config.lowerview).empty().append(this.template);

            if(index != -1){
                var settings : Settings = app.settings[index];
                if(typeof settings.stoponchange == "undefined") settings.stoponchange = 0;
                if(typeof settings.suspend == "undefined") settings.suspend = 0;

                //general settings
                $(Config.settingsform + ' #index').val(index);
                $(Config.settingsform + ' #name').val(settings.name);
                $(Config.settingsform + ' #host').val(settings.host);
                $(Config.settingsform + ' #port').val(settings.port);

                //directory map
                $.each(settings.directorymap,function(i,val : DirectoryMap){
                    $('#directoryclone').children().clone(true,true).appendTo(Config.settingsform + ' #directorymap');
                    var $row = $(Config.settingsform + ' #directorymap').find('.directoryrow').last();
                    $row.find('.root').val(val.root);
                    $row.find('.remoteroot').val(val.remoteroot);
                });

                //watch exclusion map
                $.each(settings.watchexclusions,function(i,val : WatchExclusion){
                    $('#watchexclusionclone').children().clone(true,true).appendTo(Config.settingsform + ' #watchexclusions');
                    var $row = $(Config.settingsform + ' #watchexclusions').find('.watchexclusionrow').last();
                    $row.find('.path').val(val.path);
                });
            } else {
                //this is a new project, setup some defaults here

            }

            if(typeof settings != "undefined"){
                $('#stoponchange').val(settings.stoponchange);
                $('#suspend').val(settings.suspend);
            }

            Util.registerhandler('click','#backtosettings', function(){
                Util.loadpage( new TemplatePage() );
            });
                      
            //button handlers for directory map
            Util.registerhandler('click',Config.settingsform + ' .adddirectorybutton', function(){
                $('#directoryclone').children().clone(true,true).appendTo(Config.settingsform + ' #directorymap');
            });
            Util.registerhandler('click',Config.settingsform + ' .deletedirectorybutton', function(){
                $(this).closest('.directoryrow').remove();
            });
            $(Config.settingsform + ' .adddirectorybutton').trigger('click');  

            //button handlers for watch exclusions        
            Util.registerhandler('click',Config.settingsform + ' .addwatchexclusionbutton', function(){
                //console.log( $('#watchexclusionclone').children().clone(true,true).html() );
                $('#watchexclusionclone').children().clone(true,true).appendTo(Config.settingsform + ' #watchexclusions');
            });
            Util.registerhandler('click',Config.settingsform + ' .deletewatchexclusionbutton', function(){
                $(this).closest('.watchexclusionrow').remove();
            });
            $(Config.settingsform + ' .addwatchexclusionbutton').trigger('click');    

            //add and save settings
            Util.registerhandler('click',Config.settingsform + ' .addbutton',function(){
                var name = $(Config.settingsform + ' #name').val().toString();
                var host = $(Config.settingsform + ' #host').val().toString();
                var port = $(Config.settingsform + ' #port').val();
                var index = $(Config.settingsform + ' #index').val();
        
                if(name.length > 0 && host.length > 0 && port.toString().length > 0){
                    var s = new Settings();
                    //settings
                    s.name = name;
                    s.host = host;
                    s.port = <number>port;
                    s.suspend = <number>$(Config.settingsform + ' #suspend').val();
                    s.stoponchange = <number>$(Config.settingsform + ' #stoponchange').val();
        
                    //directory map
                    s.directorymap = [];
                    //console.log($(Config.settingsform + ' .directoryrow').length);
                    $(Config.settingsform + ' .directoryrow').each(function(v){
                        var root : string = <string>$(this).find('.root').val();
                        var remoteroot : string = <string>$(this).find('.remoteroot').val();
                        if(root.length > 0 && remoteroot.length > 0){
                            var dr = new DirectoryMap();
                            dr.root = root;
                            dr.remoteroot = remoteroot;
                            s.directorymap.push(dr);
                        }
                    });

                    //watch exclusion map
                    s.watchexclusions = [];
                    $(Config.settingsform + ' .watchexclusionrow').each(function(v){
                        var path : string = <string>$(this).find('.path').val();
                        if(path.length > 0){
                            var w = new WatchExclusion();
                            w.path = path;
                            s.watchexclusions.push(w);
                        }
                    });
        
                    if(index > -1) {
                        app.settings[<number>index] = s;
                    }
                    else app.settings.push(s);
        
                    Util.savejsonfile(Config.settingsfile,JSON.stringify(app.settings));
                    Util.loadpage( new TemplatePage() );
                }
            });
        }
    }
}