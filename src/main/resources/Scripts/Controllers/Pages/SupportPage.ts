namespace Coldbug {
    export class SupportPage implements IPage {

        public template : string = `
            <div class="container">
                <br />
                <form id="accountform">
                    <div style="margin: 0px auto; width: 75%;">                        
                        Please contact us using the below email address. Thank you.
                        <br/><br/>
                        support@getcoldbug.com
                    </div>
                </form>
            </div>
        `;

        constructor(){}

        public init(){
            var $this = this;
            $(Config.lowerview).empty().append(this.template);

            Util.registerhandler('click', '#addsettingsbutton', function(){
                Util.loadpage( new AddEditSettingsPage() );
            });

        }
    }
}