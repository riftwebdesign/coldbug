namespace Coldbug {
    export class RegisterPage implements IPage {

        public template : string = `
            <div class="centered">
                <form id="signinform">
                    <h2 class="form-signin-heading">Register</h2>
                    <div id="errors" style="text-align:center; color:red;"></div>
                    <label for="email" class="sr-only">Email Address</label>
                        <input type="email" id="email" class="form-control" placeholder="Email Address" autofocus>
                    <label for="confirmemail" class="sr-only">Confirm Email Address</label>
                        <input type="email" id="confirmemail" class="form-control" placeholder="Confirm Email Address" autofocus>
                    <label for="password" class="sr-only">Password</label>
                        <input type="password" id="password" class="form-control" placeholder="Password">
                    <label for="confirmpassword" class="sr-only">Confirm Password</label>
                        <input type="password" id="confirmpassword" class="form-control" placeholder="Confirm Password">
                    <button id="register" class="btn btn-lg btn-primary btn-block" type="button">Register</button>
                    <br /><br />                    
                    <button id="login" class="btn btn-lg btn-secondary btn-block" type="button">Login</button>
                </form>
            </div>
        `;

        constructor(){}

        public init(){
            var $this = this;
            $(Config.mainview).empty().append(this.template);

            Util.registerhandler('click',Config.signinform + ' #register',function () {
                var email = $(Config.signinform + ' #email').val().toString();
                var confirmemail = $(Config.signinform + ' #confirmemail').val().toString();
                var password = $(Config.signinform + ' #password').val().toString();
                var confirmpassword = $(Config.signinform + ' #confirmpassword').val().toString();
                if(email.length > 0 && password.length > 0 && email.length >= 6 && email == confirmemail && password == confirmpassword){
                    Util.register( email, password, $this.register_handler);
                } else{
                    $('#errors').empty().append("Passwords or emails do not match, or the password is less than 6 characters");
                }
            });

            Util.registerhandler('click',Config.signinform + ' #login',function () {
                Util.loadpage( new LoginPage() );
            });
        }

        public register_handler(obj : any) : void {
            console.log(obj);
            if(obj.success) Util.loadpage( new LoginPage() );
            else{
                $('#errors').empty().append("Failed to register. The account might already exist.");
            }
            //todo show an error message
        }

    }
}