
namespace Coldbug {

    export class BuggerPage implements IShowHide, IPage {

        public template : string = `
            <div id="middle" class="split content">
                <div id="left" class="split split-horizontal">  
                    <input type="text" id="treesearch" class="form-control" placeholder="Search">
                    <div id="tree"></div>
                </div>
                    <div id="center" class="split split-horizontal">
                        <div id="editortabs">
                            <ul></ul>
                        </div>
                    </div>
                <div id="right" class="split split-horizontal">
                    <div id="righttop">    
                        <div id="searchgroup">                    
                            <div class="input-group">
                                <input type="text" id="varsearch" class="form-control" placeholder="Search">
                                <!--<div class="varsettings subitem">
                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                </div>-->
                            </div>
                            <div id="vartree"></div>
                        </div>
                        <div id="settingsgroup" style="display:none;">     
                            <div style="width: 100%;">                           
                                <div class="varsettings subitem" style="float:right;">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </div>
                                <span style="padding:20px; line-height: 200%;">Settings</span>
                            </div>
                            <div id="varsettingspanel" style="padding-left: 30px;">

                            </div> 
                        </div>
                    </div>
                    <div id="rightbottom">
                        <ul>
                            <li><a href="#watchgroup">Watch</a></li>
                            <!-- <li><a href="#tracegroup">Trace</a></li> -->
                        </ul>
                        <div id="watchgroup">                    
                            <div class="input-group">
                                <input type="text" id="watchadd" class="form-control" placeholder="Add Watch">
                                <div id="watchdelete" class="subitem">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </div>
                                <!--
                                <div class="watchsettings subitem">
                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                </div>
                                -->
                            </div>
                            <div id="watchtree"></div>
                        </div>
                        <div id="tracegroup">

                        </div>
                    </div>
                </div>
            </div>
            <div id="bottom" class="split content">
                <div id="bottomleft" class="split split-horizontal"><div id="events"></div></div>
                <div id="bottomcenter" class="split split-horizontal"><div id="breakpoints"></div></div>
                <div id="bottomright" class="split split-horizontal" style="position:relative;">
                    <div id="dumpexpand" class="subitem" style="position:absolute; top: 0; right: 0;">
                        <i class="fa fa-expand" aria-hidden="true"></i>
                    </div>
                    <div id="dumppane"></div>
                </div>
            </div>
            <div id="scopedialog" title="Dump Pane" style="display:none;"></div>
        `;
        public timer : any;

        public unload() : void{
            clearTimeout(this.timer);
        }

        public handle_filechange(obj:any){
            if(obj.result) EditorController.addeditorandselect(obj.path, -1, false);          
        }

        public filechangechecker(){
            var $this = this;
            this.timer = setTimeout(function() {
                if(app.editorfilepath) {
                    var of : OpenFile = app.getopenfile(app.editorfilepath);
                    if(of && of.path.length > 0) 
                        bridgecontroller.comparehash(of.path,of.hash,$this.handle_filechange);
                }
                $this.filechangechecker();
            }, 3000);
        }

        public init() : void{     

            var $this = this;
            
            if(!app.getactivesetting().watchvars)
                app.getactivesetting().watchvars = [];
            
            Util.registerhandler('click','#watchdelete', function(){   
                $.each((<any>$("#watchtree")).jstree("get_checked",true),function(){
                    var node = (<any>$("#watchtree")).jstree(true).get_node(this.id);
                    for(var i=app.getactivesetting().watchvars.length-1; i>=0; i--){
                        var w : WatchVar = app.getactivesetting().watchvars[i];
                        if(w.text == node.text){
                            app.getactivesetting().watchvars.splice(i,1);
                        }
                    }
                });
                Util.savejsonfile(Config.settingsfile,JSON.stringify(app.settings),function(){ Util.getwatch($this.loadwatch) });
            });

            Util.registerhandler('keypress','#watchadd', function(e){
                if(e.which == 13) {
                    var w : WatchVar = new WatchVar();
                    w.text = $('#watchadd').val().toString();
                    $('#watchadd').val('');
                    app.getactivesetting().watchvars.push(w);
                    Util.savejsonfile(Config.settingsfile,JSON.stringify(app.settings),function(){ Util.getwatch($this.loadwatch) });
                }
            });
            
            Util.registerhandler('click','#dumpexpand', function(){
                var $dialog = (<any>$( "#scopedialog" ));
                $dialog.dialog({
                    modal: true,
                    dialogClass: "no-close",
                    minHeight: 450,
                    maxHeight: 850,
                    minWidth: 1200,
                    position: { my: "top", at: "top" },
                    open: function(){
                        $dialog.empty().append($('#dumppane').html());
                    },
                    buttons: [
                        {
                            text: "Close",
                            click: function() {
                                $dialog.dialog( "close" );
                            }
                        }
                    ]
                });
            });            

            Util.registerhandler('click','.scopecheck', function(){
                app.getactivesetting().excludescopes = [];
                $('.scopecheck').each(function(){
                    if(!$(this).prop('checked')){
                        app.getactivesetting().excludescopes.push($(this).data('scope'));
                    }
                });
                Util.savejsonfile(Config.settingsfile,JSON.stringify(app.settings));
            });
            
            Util.registerhandler('click','.varsettings', function(){

                var scopes = [
                    "Application"
                    ,"Arguments"
                    ,"Attributes"
                    ,"Caller"
                    ,"CGI"
                    ,"Client"
                    ,"Cluster"
                    ,"Cookie"
                    ,"Form"  
                    ,"Local"
                    ,"Request"                  
                    ,"Server"
                    ,"Session"
                    ,"This"
                    ,"Thread"
                    ,"URL"
                    ,"Variables"
                ];

                var togglesearch = $('#searchgroup').is(":visible");
                var excludes = app.getactivesetting().excludescopes;

                if(togglesearch){
                    $('#searchgroup').hide();
                    $('#settingsgroup').show();
                    $('#varsettingspanel').empty()
                    $.each(scopes, function () {
                        $('#varsettingspanel').append(`
                            <div class="form-check">
                                <input class="form-check-input scopecheck" data-scope="${this}" type="checkbox" ${ ($.inArray(this,excludes) > -1 ? '' : 'checked') } value="1" id="${this}_check">
                                <label class="form-check-label" for="${this}_check">
                                    ${this}
                                </label>
                            </div>
                        `);
                    });                    
                } else {
                    $('#searchgroup').show();
                    $('#settingsgroup').hide();
                }
            });

            this.filechangechecker();

            $(Config.lowerview).empty().append(this.template);

            (<any>window).Split(['#left', '#center', '#right'], {
                sizes: [10, 80, 10],
                gutterSize: 3,
                cursor: 'col-resize'
            });

            (<any>window).Split(['#middle', '#bottom'], {
            direction: 'vertical',
            sizes: [90, 10],
            gutterSize: 3,
            cursor: 'row-resize'
            });

            (<any>window).Split(['#bottomleft', '#bottomcenter', '#bottomright'], {
            sizes: [33, 34, 33],
            gutterSize: 3,
            cursor: 'col-resize'
            });

            (<any>window).Split(['#righttop', '#rightbottom'], {
            direction: 'vertical',
            sizes: [50, 50],
            gutterSize: 3,
            cursor: 'row-resize'
            });

            (<any>$( "#editortabs" )).tabs();
            (<any>$('#rightbottom')).tabs();

            if(!app.getactivesetting().openfiles) app.getactivesetting().openfiles = [];
            if(!app.getactivesetting().opentree) app.getactivesetting().opentree = [];

            app.getactivesetting().openfiles.forEach(function(v:OpenFile) {
                EditorController.addeditorandselect(v.path, -1, false);
            });

            Util.getprojecttree(this.loadtree);    
            Util.getwatch(this.loadwatch);       

            Util.renderevents();
            Util.renderbreakpoints();
            this.showhide();                        

            Util.registerhandler('click','#editortabs .closetab', function () {
                var path = $(this).siblings('a').data('path');
                var active = (<any>$("#editortabs")).tabs('option', 'active');
                var currentindex = -1;
                var tabslength = $('#editortabs ul li a').length;
                $.each( $('#editortabs ul li a'), function(i:number){
                    if($(this).data('path') == path) currentindex = i;
                });
                var editor = app.geteditor(path);
                var settings = app.getactivesetting();
                var oi = settings.openfiles.findIndex(o => o.path == path);
                var ei = app.editors.findIndex(e => e.path == path);
        
                settings.openfiles.splice(oi,1);
                app.editors.splice(ei,1);
                $("#editortabs li").eq(currentindex).remove();
                $("#editortabs div").eq(currentindex).remove();
        
                Util.savejsonfile(Config.settingsfile,JSON.stringify(app.settings));
        
                (<any>$("#editortabs")).tabs("refresh");
        
                if(tabslength > 1){
                    $('#editortabs a').first().trigger('click');
                }
            });
            Util.registerhandler('click','#editortabs a', function () {
                var path = $(this).data('path');
                app.editorfilepath = path;
                try{
                    //resizeeditor();
                    app.getactiveeditor().resize(true);
                    //BuggerPage.rerenderbreakpoints();
                    Util.togglesave(path);
                } catch(e){
        
                }
                //console.log(path);
            });
            Util.registerhandler('keyup',Config.left + ' #treesearch',function () {
                setTimeout(function () {
                    var v = $(Config.left + ' #treesearch').val();
                    (<any>$('#left #tree')).jstree(true).search(v,true,true);
                }, 250);
            });
        
            Util.registerhandler('keyup',Config.right + ' #varsearch',function () {
                setTimeout(function () {
                    var v = $(Config.right + ' #varsearch').val();
                    (<any>$(Config.right + ' #vartree')).jstree(true).search(v,true,true);
                }, 250);
            });
        
            Util.registerhandler('click',Config.right + ' #vartree a, ' + Config.right + ' #watchtree a',function () {
                if($(this).attr('scope'))
                    BuggerPage.dump($(this).attr('scope'),"#dumppane");
            });
        
            Util.registerhandler('click',Config.left + ' a',function () {
                var path = $(this).attr('path');
                var isdirectory = $(this).attr('isdirectory');        

                if(isdirectory.toString() == "false"){
                    app.editorfilepath = path;
                    EditorController.addeditorandselect(app.editorfilepath,-1,false);
                }
            });
        }

        public loadtree(tree:any) : void {
            BuggerPage.clearprojecttree();
            BuggerPage.staticloadtree(tree);
        }

        public loadwatch(tree:any) : void {
            BuggerPage.staticloadwatch(tree);
        }

        public static staticloadwatch(tree:any) : void {

            if(tree.hasOwnProperty("err") && tree.err) return;

            //console.log(tree);

            BuggerPage.clearwatchtree();
            (<any>$('#watchtree')).jstree({
                'plugins' : [ "search", "checkbox" ],
                "checkbox": {
                    "keep_selected_style":false,
                    "whole_node":false,
                    "three_state":false,
                    "tie_selection":false
                },
                'core' : {                    
                    "themes": {
                        "theme": "default",
                        "dots": false,
                        "icons": false
                        //,"url": Config.jstreelib
                    },
                    'data' : tree
                }
            });
            (<any>$('#watchtree')).on('loaded.jstree', function(e, data) {
                var trees = app.getactivesetting().opentree;         
                for (var i = trees.length - 1; i >= 0; --i) {
                    var $this = trees[i];
                    if($this.type == 'watchtree') {
                        var $tree : any = $('#watchtree');
                        $($tree.jstree().get_json($tree, {
                            flat: true
                        })).each(function(index, value) {
                            var node = $tree.jstree().get_node(this.id);
                            if($this.id == node.text) {                                
                                for (var i = node.parents.length - 1; i >= 0; --i) {
                                    var thisnode = $tree.jstree().get_node(node.parents[i]);
                                    $tree.jstree("open_node", thisnode.id);
                                }      
                                $tree.jstree("open_node", node.id);                          
                            }
                        });         
                    }          
                }                
            });

            (<any>$('#watchtree')).bind("open_node.jstree",function(e,d){
                Util.addopentreeandsave(d.node.text,"watchtree");
            });
            (<any>$('#watchtree')).bind("close_node.jstree",function(e,d){
                Util.deleteopentreeandsave(d.node.text,"watchtree");
            });
        }
        
        public static staticloadtree(tree:any,folder?:string) : void {
            if( (<any>$('#left #tree')).jstree(true).settings ) {
                    (<any>$('#left #tree')).jstree(true).settings.core.data = tree;

                    var wasreloaded = false;
                    if(folder){
                        var node;
                        $('a').each(function(){
                            if( $(this).attr("path") ){
                                //console.log( $(this).attr("path") );
                                if( $(this).attr("path")==folder ){
                                    node = $(this);
                                }
                            }

                        });
                        //console.log(`a[path="${folder}"]`,node);
                        if(node && node.length > 0) {
                            var t_node = (<any>$('#left #tree')).jstree(true).get_node(node.attr('id'));
                            //console.log(node.attr('id'));
                            //(<any>$('#left #tree')).jstree(true).refresh_node(node.attr('id'));
                            (<any>$('#left #tree')).jstree(true).refresh(t_node);
                            //wasreloaded = true;
                        }
                    }    
                    //if(!wasreloaded) (<any>$('#left #tree')).jstree(true).refresh();
                    $(Config.left + ' #treesearch').trigger('keyup');
                
                return;
            }

            (<any>$('#left #tree')).bind('loaded.jstree', function(e, data) {
                //this means it was loaded
            }).jstree({
                'plugins' : [ "search" ],
                'check_callback' : true,
                'core' : {
                    "themes": {
                        "theme": "default",
                        "dots": true,
                        "icons": true
                        //,"url": Config.jstreelib
                    },
                    'data' : tree
                }
            });
            (<any>$('#left #tree')).on('loaded.jstree', function(e, data) {
                var trees = app.getactivesetting().opentree;         
                for (var i = trees.length - 1; i >= 0; --i) {
                    var $this = trees[i];
                    if($this.type == 'lefttree') {
                        var $tree : any = $('#left #tree');
                        $($tree.jstree().get_json($tree, {
                            flat: true
                        })).each(function(index, value) {
                            var node = $tree.jstree().get_node(this.id);
                            if($this.id == node.a_attr.path) {                                
                                for (var i = node.parents.length - 1; i >= 0; --i) {
                                    var thisnode = $tree.jstree().get_node(node.parents[i]);
                                    $tree.jstree("open_node", thisnode.id);
                                }      
                                $tree.jstree("open_node", node.id);                          
                            }
                        });         
                    }          
                }                
            });  
            (<any>$('#left #tree')).on("open_node.jstree",function(e,d){
                Util.addopentreeandsave(d.node.a_attr.path,"lefttree");
            });
            (<any>$('#left #tree')).on("close_node.jstree",function(e,d){
                Util.deleteopentreeandsave(d.node.a_attr.path,"lefttree");
            });          
        }

        public showhide(){
            $('#connectbutton, #breakbutton').show();
        }

        public static updatedumppane(val : string, el : string) : void {
            var $pane = $(el);
            $pane.empty().append(val);
        }

        public static dump(val : string, el: string) : void {
            setTimeout(function(){
                //console.log(val);
                bridgecontroller.dump(val, el)
            },1);
        }

        public static clearwatchtree() : void {
            var $left = $('#watchtree');

            try{
                (<any>$left).jstree('destroy');
            }catch(e){
                //console.log(e);
            }

            $left.empty();
        }

        public static clearprojecttree() : void {
            var $left = $('#left #tree');

            try{
                (<any>$left).jstree('destroy');
            }catch(e){
                //console.log(e);
            }

            $left.empty();
        }

        public static clearvartree() : void {
            var $right = $('#right #vartree');

            try{
                (<any>$right).jstree('destroy');
            }catch(e){
                //console.log(e);
            }

            $right.empty();
        }

        public static updatevartree(tree) : void {
            var $right = $('#right #vartree');

            BuggerPage.clearvartree();

            (<any>$right).jstree({
                'plugins' : [ "search", "sort" ],
                'core' : {
                    "themes": {
                        "theme": "default",
                        "dots": true,
                        "icons": true
                        //,"url": Config.jstreelib
                    },
                    'data' : tree
                }
            });
            /*(<any>$('#right #vartree')).on('loaded.jstree', function(e, data) {
                var trees = app.getactivesetting().opentree;         
                for (var i = trees.length - 1; i >= 0; --i) {
                    var $this = trees[i];
                    if($this.type == 'vartree') {
                        var $tree : any = $('#right #vartree');
                        $($tree.jstree().get_json($tree, {
                            flat: true
                        })).each(function(index, value) {
                            var node = $tree.jstree().get_node(this.id);
                            if($this.id == node.a_attr.scope) {                                
                                for (var i = node.parents.length - 1; i >= 0; --i) {
                                    var thisnode = $tree.jstree().get_node(node.parents[i]);
                                    $tree.jstree("open_node", thisnode.id);
                                }      
                                $tree.jstree("open_node", node.id);                          
                            }
                        });         
                    }          
                }                
            });
            (<any>$('#right #vartree')).bind("open_node.jstree",function(e,d){
                Util.addopentreeandsave(d.node.a_attr.scope,"vartree");
            });
            (<any>$('#right #vartree')).bind("close_node.jstree",function(e,d){
                Util.deleteopentreeandsave(d.node.a_attr.scope,"vartree");
            });*/
        }

        public static rerenderbreakpoints() : void {            
            var $editorfilepath = app.editorfilepath;
            var $editor = app.getactiveeditor();
            app.getactivesetting().breakpoints.forEach(function(v){
                $editor.getSession().removeMarker(v.marker);
                var Range = ace.require('ace/range').Range;
                if(v.path.toLowerCase() == $editorfilepath.toLowerCase())
                    v.marker = $editor.session.addMarker(new Range(v.line-1, 0, v.line-1, 1), "redline", "fullLine");
            });
            Util.renderbreakpoints();
        }

        public static loadfile(path:string,line:number,stepline?:boolean) : void {
            stepline = stepline || false;
            line = line || -1;
            //var c = Util.loadfile(path.toLowerCase());
            
            app.editorfilepath = path;
            EditorController.addeditorandselect(app.editorfilepath,line,stepline);            
        }

        public static refreshEditor($editor:any,path:string,line:number,stepline:boolean){
            if(!$editor) return;
            var mode = "coldfusion";
            var re = /(?:\.([^.]+))?$/;
            var ext = re.exec(path.toLowerCase())[1];
            if(Config.defaultfilemap[ext]) mode = Config.defaultfilemap[ext];

            $editor.getSession().setMode("ace/mode/"+mode);
            //$editor.setValue(c, -1);
            $editor.clearSelection();

            if(line > -1){                
                $editor.resize(true);
                $editor.scrollToLine(line, true, true, function () {});
                $editor.gotoLine(line, 10, true);
                line -= 1;
                if(stepline) {
                    Util.clearline(app.stepline);
                    var Range = ace.require('ace/range').Range;
                    app.stepline = $editor.session.addMarker(new Range(line, 0, line, 1), "yellowline", "fullLine");
                    //BuggerPage.clearvartree();
                }
            }
            //BuggerPage.rerenderbreakpoints();
            if(stepline) {
                (<any>PageTitleNotification).On("BREAK");
                Util.enableBuggingButtons();
            } 
        }

    }

}