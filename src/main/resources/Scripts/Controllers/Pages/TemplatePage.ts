namespace Coldbug {
    export class TemplatePage implements IPage {

        private page : IPage = new SettingsPage();

        public template : string = `
            <div id="top" class="split content"></div>

            <div id="lower"></div>
            
            <div id="leftsidenav" class="sidenav"></div>        
        `;

        constructor(page?:IPage){
            this.page = page || this.page;
        }

        public init(){
            $(Config.mainview).empty().append(this.template);
            Util.loadpage( this.page );
            app.registeractions();
            app.registernavlinks();
        }

    }
}