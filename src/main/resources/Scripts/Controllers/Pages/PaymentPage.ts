namespace Coldbug {

    declare var user:User;

    export class PaymentPage implements IPage {

        public template : string = `
            <div class="container">
                <br />
                <div>
                    <a href="#" id="backtosettings"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                </div>                
                <div style="margin: 0px auto; width: 75%;">
                    <div id="form-container">
                        <div id="sq-ccbox">
                        <form id="nonce-form" novalidate action="${Coldbug.Config.squareendpoint}" method="post">
                            <fieldset>
                                <input type="text" name="billingfirstname" class="form-control" placeholder="First Name">
                                <input type="text" name="billinglastname" class="form-control" placeholder="Last Name">
                                <input type="text" name="billingphone" class="form-control" placeholder="Phone Number - 1-222-333-4444">
                                <input type="text" name="billingline1" class="form-control" placeholder="Address 1">
                                <input type="text" name="billingline2" class="form-control novalidation" placeholder="Address 2">
                                <input type="text" name="billingcity" class="form-control" placeholder="City">
                                <input type="text" name="billingstate" class="form-control" placeholder="State / Province">
                                <input type="text" name="billingcountry" class="form-control" placeholder="Country Code - US">
                            </fieldset>
                            <fieldset>
                                <div id="sq-postal-code"></div>
                                <div id="sq-card-number"></div>                        
                                <div class="third">
                                    <div id="sq-expiration-date"></div>
                                </div>                        
                                <div class="third">
                                    <div id="sq-cvv"></div>
                                </div>   
                            </fieldset>
                            <div id="error"></div>                    
                            <button id="sq-creditcard" class="btn btn-lg btn-primary btn-block" onclick="requestCardNonce(event)">Save Card</button> 
                            <input type="hidden" id="card-nonce" name="nonce">
                            <input type="hidden" name="email" value="${user.email}">
                            <input type="hidden" name="method" value="updateuser">
                        </form>
                        </div>                    
                    </div>
                </div>
            </div>
        `;  

        public init(args : any[]) { 
            var $this = this;

            $(Config.lowerview).empty().append(this.template);
            
            Util.registerhandler('click','#backtosettings', function(){
                Util.loadpage( new TemplatePage(  new AccountPage() ) );
            });

            buildForm();
        }

        public submit(carddata){
            var $this = this;
            var $form = $('#nonce-form');
            var haserror = false;

            $('.form-control').each(function(){
                if((<string>$(this).val()).trim().length == 0 && !$(this).hasClass('novalidation')){
                    $(this).addClass('sq-input--error');
                    haserror = true;
                } else {
                    $(this).removeClass('sq-input--error');
                }
            });

            haserror = haserror || (<string>$('#card-nonce').val()).trim().length == 0;
 
            if(!haserror){                
                var formdata = Util.serializeForm($form);
                formdata.billingzip = carddata.billing_postal_code;
                //console.log(formdata);
                bridgecontroller.updateuser(formdata, $this.handle_update);
            }            
        }

        public handle_update(obj){            
            if(obj.success && Object.keys(obj.errors).length == 0){   
                user = obj;             
                Util.loadpage( new TemplatePage(  new AccountPage() ) );
            } else {
                if(obj.errors.hasOwnProperty("subscriptionerrors")){
                    $('#error').empty().append(obj.errors.subscriptionerrors);
                }
            }
            if(obj.hasOwnProperty("errors")) delete obj["errors"];
            user = obj;
        }
    }
}