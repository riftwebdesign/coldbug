namespace Coldbug {

    declare var user:User;

    export class AccountPage implements IPage {

        public template : string = `
            <div class="container">
                <br />
                <form id="accountform">  
                    <input type="hidden" name="method" value="updateuser">
                    <div style="margin: 0px auto; width: 75%;"> 
                        <div>
                            <h2 class="form-signin-heading">Subscription</h2>
                            <label for="nextbillingdate">Next Bill Date - $5.00 USD / Month</label> <input data-toggle="tooltip" data-placement="bottom" title="This is the date your subscription expires. If you have a card on file, you will be automatically charged at this time. You can remove an active card at anytime after adding it." value="${user.nextbillingdate}" type="text" id="nextbillingdate" class="form-control" placeholder="Next Billing Date" readonly>
                        </div>              
                        <div>
                            <h2 class="form-signin-heading">User Info</h2>
                            <div id="usererrors" style="text-align:center; color:red;"></div>
                            <label for="firstname">First Name</label> <input data-toggle="tooltip" data-placement="top" title="Customer first name." value="${user.firstname}" type="text" name="firstname" class="form-control" placeholder="First Name">
                            <label for="lastname">Last Name</label> <input data-toggle="tooltip" data-placement="top" title="Customer last name." value="${user.lastname}" type="text" name="lastname" class="form-control" placeholder="Last Name">
                            <label for="email">Email</label> <input data-toggle="tooltip" data-placement="top" title="Customer email address." value="${user.email}" type="text" name="email" class="form-control" placeholder="Email" readonly>                            
                        </div>
                        <div>
                            <h2 class="form-signin-heading">Settings</h2>
                            <div id="settingerrors" style="text-align:center; color:red;"></div>
                            <label for="newsletter">Newletter</label> <select data-toggle="tooltip" data-placement="top" title="Receive periodic emails in regards to updates or news related to Coldbug." class="form-control" id="newsletter" name="newsletter">
                                <option value="1">Yes</option>
                                <option value="0" ${user.newsletter == 0 ? 'selected="selected"' : ''}>No</option>
                            </select>
                        </div>
                        <div>
                            <h2 class="form-signin-heading">Password</h2>
                            <div id="passworderrors" style="text-align:center; color:red;"></div>
                            <label for="currentpassword">Current Password</label> <input data-toggle="tooltip" data-placement="top" title="Customer current password." type="text" name="currentpassword" class="form-control" placeholder="Current Password">
                            <label for="newpassword">New Password</label> <input data-toggle="tooltip" data-placement="top" title="Customer new password." type="text" name="newpassword" class="form-control" placeholder="New Password">
                        </div>
                        <div>
                            <div id="subscriptionerrors" style="text-align:center; color:red;"></div>
                            <br />                            
                            <div id="cardgroup" style="${!user.hascard ? 'display:none;' : ''}">
                                <button data-toggle="tooltip" data-placement="top" title="Remove your current card on file. Doing this will prevent the automated billing system from charging your card, and your account will suspended until a payment is made." id="removecardbutton" class="btn btn-lg btn-primary btn-block" type="button">Remove Card</button>
                            </div>
                            <div id="billinggroup" style="${user.hascard ? 'display:none;' : ''}">
                                <button data-toggle="tooltip" data-placement="top" title="Add a new payment method which will be automatically charged at the time of your next billing date. If your billing date is in the past, you will be charged when adding a new payment card. You can remove your payment method anytime after adding it." id="addpaymentbutton" class="btn btn-lg btn-primary btn-block" type="button">Add Payment</button>
                            </div>       
                            <br />
                            <button id="savebutton" class="btn btn-lg btn-primary btn-block" type="button">Save</button>
                        </div>                        
                    </div>
                </form>
            </div>
        `;

        constructor(){}

        public init(){
            var $this = this;
            $(Config.lowerview).empty().append(this.template);
            
            Util.registerhandler('click', '#addpaymentbutton', function(){
                Util.loadpage( new PaymentPage() );
            });

            Util.registerhandler('click', '#addsettingsbutton', function(){
                Util.loadpage( new AddEditSettingsPage() );
            });

            Util.registerhandler('click', '#removecardbutton', function(){
                var $form = $('#accountform');
                var form = Util.serializeForm($form);
                form.removecard = true;
                //console.log(form);
                bridgecontroller.updateuser(form, $this.handle_update);
            });

            Util.registerhandler('click', '#savebutton', function(){
                var $form = $('#accountform');
                //console.log(Util.serializeForm($form));
                bridgecontroller.updateuser(Util.serializeForm($form), $this.handle_update);
            });
        }

        public handle_update(obj){            
            if(obj.success && Object.keys(obj.errors).length == 0){   
                user = obj;             
                Util.loadpage( new TemplatePage(  new AccountPage() ) );
            } else {
                if(obj.errors.hasOwnProperty("subscriptionerrors")){
                    $('#subscriptionerrors').empty().append(obj.errors.subscriptionerrors);
                }
                if(obj.errors.hasOwnProperty("passworderrors")){
                    $('#passworderrors').empty().append(obj.errors.passworderrors);
                }
            }
            if(obj.hasOwnProperty("errors")) delete obj["errors"];
            user = obj;
        }
    }
}