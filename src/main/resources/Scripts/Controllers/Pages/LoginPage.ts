namespace Coldbug {

    declare var user:User;

    export class LoginPage implements IPage {

        public template : string = `
            <div class="centered">
                <form id="signinform">
                    <h2 class="form-signin-heading">Login</h2>
                    <div id="errors" style="text-align:center; color:red;"></div>
                    <label for="email" class="sr-only">Email Address</label>
                        <input type="email" id="email" class="form-control" placeholder="Email Address" autofocus>
                    <label for="password" class="sr-only">Password</label>
                        <input type="password" id="password" class="form-control" placeholder="Password">
                    <button id="login" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    <br /><br />
                    <button id="register" class="btn btn-lg btn-secondary btn-block" type="button">Register</button>
                    <button id="forgotpassword" class="btn btn-lg btn-secondary btn-block" type="button">Forgot Password</button>
                </form>
            </div>
        `;

        constructor(){}

        public init(){
            var $this = this;
            $(Config.mainview).empty().append(this.template);

            Util.registerhandler('submit',Config.signinform,function (e:any) {
                e.preventDefault();
                var email = $(Config.signinform + ' #email').val();
                var password = $(Config.signinform + ' #password').val();
                /*todo form validation*/
                bridgecontroller.login(email,password,$this);
            });
            // Util.registerhandler('click',Config.signinform + ' #login',function () {
            //     $(Config.signinform).submit();
            // });
            Util.registerhandler('click',Config.signinform + ' #register',function () {
                Util.loadpage( new RegisterPage() );
            });
            Util.registerhandler('click',Config.signinform + ' #forgotpassword',function () {
                //console.log('click');
                Util.loadpage( new ForgotPasswordPage() );
            });

            bridgecontroller.login('NA','NA',$this);
        }

        public login_handler(obj : any) : void {   
            if(obj.loggedin) {         
                user = obj;
                Util.loadpage( new TemplatePage() );
            } else {
                $('#errors').empty().append("Failed!");
            }
        }

    }
}