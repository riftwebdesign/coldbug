namespace Coldbug {
    export class StopButton implements IActionButton{
        public icon:string = "fa fa-stop-circle";
        public id:string = "stopbutton";
        public classes:string = "";
        public info:string = "Stop the debugger.";
        public clickhandler:Function = () => {
            if($('#stopbutton').hasClass('disabled')) return;        
            Util.disconnect(Coldbug.StopButton.handle_disconnect);
            Util.clearline(app.stepline);
        };
        public description:string = "Stops the server connection.";
        public defaulthide:boolean = true;
        public styles:{[key:string]:string} = {};
        public static handle_disconnect(){
            $(Config.top + ' #stepoutbutton,' + Config.top + ' #stopbutton,' + Config.top + ' #stepinbutton,' + Config.top + ' #stepoverbutton,' + Config.top + ' #playbutton').hide();
            $(Config.top + ' #connectbutton').show();
            Util.clearline(app.stepline);
        }
    }
}