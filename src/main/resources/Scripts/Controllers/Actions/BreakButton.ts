namespace Coldbug {
    export class BreakButton implements IActionButton{
        public icon:string = "fa fa-circle";
        public id:string = "breakbutton";
        public info:string = "Add a breakpoint to the line with the active cursor.";
        public classes:string = "";
        public clickhandler:Function = () => {
            var $editorfilepath = app.editorfilepath;
            var $editor = app.getactiveeditor();
            var selectionRange = $editor.getSelectionRange();
            var line = selectionRange.start.row;
            var Range = ace.require('ace/range').Range;
            var marker = $editor.session.addMarker(new Range(line, 0, line, 1), "redline", "fullLine");
            var item = {
                "path": $editorfilepath,
                "line": line + 1,
                "marker": marker,
                "hasline":false
            };
            var index = app.getactivesetting().breakpoints.findIndex(function (x) {
                return x.path.toLowerCase() == $editorfilepath.toLowerCase() && x.line == line + 1;
            });
            if (index === -1) {
                app.getactivesetting().breakpoints.push(item);
            }
            else {
                $editor.getSession().removeMarker(app.getactivesetting().breakpoints[index].marker);
                app.getactivesetting().breakpoints.splice(index, 1);
                $editor.getSession().removeMarker(marker);
            }
            Util.savejsonfile(Config.settingsfile, JSON.stringify(app.settings));
            Util.setbreakpoints( JSON.stringify(app.getactivesetting().breakpoints) );
            BuggerPage.rerenderbreakpoints();
        };
        public description:string = "Set / Remove a breakpoint.";
        public defaulthide:boolean = true;
        public styles:{[key:string]:string} = {"color":"red"};
    }
}