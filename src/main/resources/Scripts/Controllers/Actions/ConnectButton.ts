namespace Coldbug {
    export class ConnectButton implements IActionButton{
        public icon:string = "fa fa-play-circle";
        public id:string = "connectbutton";
        public classes:string = "";
        public info:string = "Connect to the target VM using the project settings.";
        public $this:any = this;
        public clickhandler:Function = () => {
            if($('#connectbutton').hasClass('disabled')) return;        
            Util.connect(this.init_connect);
        };
        public description:string = "Connect to the server.";
        public defaulthide:boolean = true;
        public styles:{[key:string]:string} = {};
        public init_connect(obj){
            if(!obj.connected) {
                Util.addevent("Failed to connect: check that the server is on and your settings are correct",EventLevel.error);
            }
        }
        public static handle_connect(obj:any){
            if(!obj.statechange) return;

            if(obj.connected) {
                $(Config.top + ' #stepoutbutton,' + Config.top + ' #stopbutton,' + Config.top + ' #stepinbutton,' + Config.top + ' #stepoverbutton,' + Config.top + ' #playbutton').show();
                Util.disableBuggingButtons();
                $(Config.top + ' #connectbutton').hide();
                //Util.setbreakpoints( JSON.stringify(app.getactivesetting().breakpoints) );
                Util.addevent("Connected to server",EventLevel.success);
            }else {
                Coldbug.StopButton.handle_disconnect();
                Util.addevent("Disconnected",EventLevel.error);
            }
        }
    }
}