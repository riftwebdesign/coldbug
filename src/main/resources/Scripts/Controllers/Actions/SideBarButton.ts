namespace Coldbug {
    export class SideBarButton implements IActionButton{
        public icon:string = "fa fa-bars";
        public id:string = "sidebarbutton";
        public classes:string = "";
        public info:string = "Opens the side navigation panel.";
        public clickhandler:Function = () => {
            $('#leftsidenav').width(250);
        };
        public description:string = "Opens the side navigation panel.";
        public defaulthide:boolean = false;
        public styles:{[key:string]:string} = {};
    }
}