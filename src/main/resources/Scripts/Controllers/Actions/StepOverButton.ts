namespace Coldbug {
    export class StepOverButton implements IActionButton{
        public icon:string = "fa fa-fast-forward";
        public id:string = "stepoverbutton";
        public classes:string = "disabled";
        public info:string = "Step Over.";
        public clickhandler:Function = () => {            
            if($('#stepoverbutton').hasClass('disabled')) return;        
            Util.disableBuggingButtons();
            bridgecontroller.actionbuttonclick("stepover");
        };
        public description:string = "Step over.";
        public defaulthide:boolean = true;
        public styles:{[key:string]:string} = {};
    }
}