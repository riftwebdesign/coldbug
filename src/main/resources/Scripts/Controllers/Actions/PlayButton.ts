namespace Coldbug {
    export class PlayButton implements IActionButton{
        public icon:string = "fa fa-play";
        public id:string = "playbutton";
        public classes:string = "disabled";
        public info:string = "Continue the program execution until the next breakpoint.";
        public clickhandler:Function = () => {
            if($('#playbutton').hasClass('disabled')) return;
            Util.disableBuggingButtons();       
            bridgecontroller.actionbuttonclick("play");
            Util.clearline(app.stepline);
        };
        public description:string = "Play.";
        public defaulthide:boolean = true;
        public styles:{[key:string]:string} = {};
    }
}