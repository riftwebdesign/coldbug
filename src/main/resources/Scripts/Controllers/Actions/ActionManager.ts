namespace Coldbug{
    export class ActionManager {
        private static actions : IActionButton[] = [];

        public static init() : void{
            this.actions = [];
        }
        public static add(action:IActionButton) : void{
            this.actions.push(action);
        }
        public static register() : void{
            var r = "";

            Util.registerhandler('click',"#playbutton, #stepinbutton, #stepoutbutton, #stepoverbutton",function(){
                //$("#playbutton, #stepinbutton, #stepoutbutton, #stepoverbutton").addClass('disabled');
                //Util.disableBuggingButtons(); this is the new function
            }); //disabled code is inside of buggger page for this.

            var counter = 0;
            for(var a of this.actions){
                counter++;
                Util.registerhandler('click',"#"+a.id,a.clickhandler);

                if(a.defaulthide){
                    a.styles["display"] = "none";
                }
                var position = "bottom";
                if(counter <= 2) position = "right";
                
                r += `<div data-toggle="tooltip" data-placement="${position}" title="${a.info}" id="${a.id}" class="actionitem" style="`;
                
                for(var key of Object.keys(a.styles)){
                    //console.log(key, a.styles[key]);
                    r += `${key}:${a.styles[key]};`;
                }

                r += `">
                        <i class="${a.icon} ${a.classes}" aria-hidden="true"></i>
                    </div>
                `;
            }

            $(Config.top).empty().append(r);          
            
        }
    }
}