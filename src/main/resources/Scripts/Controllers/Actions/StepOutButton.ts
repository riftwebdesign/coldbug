namespace Coldbug {
    export class StepOutButton implements IActionButton{
        public icon:string = "fa fa-step-backward";
        public id:string = "stepoutbutton";
        public classes:string = "disabled";
        public info:string = "Step Out.";
        public clickhandler:Function = () => {
            if($('#stepoutbutton').hasClass('disabled')) return;        
            Util.disableBuggingButtons();
            bridgecontroller.actionbuttonclick("stepout");
        };
        public description:string = "Step out.";
        public defaulthide:boolean = true;
        public styles:{[key:string]:string} = {};
    }
}