namespace Coldbug {
    export class StepInButton implements IActionButton{
        public icon:string = "fa fa-step-forward";
        public id:string = "stepinbutton";
        public classes:string = "disabled";
        public info:string = "Step into.";
        public clickhandler:Function = () => {
            if($('#stepinbutton').hasClass('disabled')) return;       
            Util.disableBuggingButtons();
            bridgecontroller.actionbuttonclick("stepin");
        };
        public description:string = "Stops the server connection.";
        public defaulthide:boolean = true;
        public styles:{[key:string]:string} = {};
    }
}