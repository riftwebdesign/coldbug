namespace Coldbug {    
    export class EditorController{
        public static createtab(path:string,contents:string) : string {
            path = path;
            var tabs = (<any>$( "#editortabs" )).tabs();
            var ul = tabs.find( "ul" );
            app.uniqueid++;
            var id = app.uniqueid;
            var namearray = path.split("\\");
            $( `<li><a data-path="${path}" href="#editor-${id}" data-id="${id}">${namearray[namearray.length-1]}</a> <i class="closetab fa fa-times-circle-o" aria-hidden="true"></i> </li>` ).appendTo( ul );
            
            $( `<div data-path="${path}" class="editor" id="editor-${id}"></div>` ).appendTo( tabs );
            /*
            $(`#editor-${id}`).append(`<p>
                //<![CDATA[
                    ${contents}
                //]]>
            </p>`);
            */
            tabs.tabs( "refresh" );

            $('#editortabs a[href="#editor-'+id+'"]').trigger('click');

            return "editor-"+id;
        }

        public static setFile(obj:any){
            if(!obj || !obj.filecontents) return;
            
            ace.config.set("basePath", "./Lib/Ace/");
            
            var c = Util.base64decode(obj.filecontents);
            var path = obj.path;
            var hash = obj.hash;

            var $settings = app.getactivesetting();
            
            var changed = false;
            var openfile = app.getopenfile(path);
            var editor = app.geteditor(path);
            
            if(!$settings.openfiles) $settings.openfiles = [];

            if(openfile == null){
                openfile = new OpenFile();
                openfile.path = path;
                openfile.hash = hash;                
                //openfile.changeddisccontents = Util.base64decode(c);
                //openfile.changedcontents = Util.base64decode(c);
                $settings.openfiles.push(openfile);
                Util.savejsonfile(Config.settingsfile,JSON.stringify(app.settings));
            } else {
                changed = hash != openfile.hash;
                openfile.hash = hash;
            }

            //openfile.disccontents = Util.base64decode(c);

            if(editor == null){
                editor = new Editor();
                editor.path = path;
                editor.id = EditorController.createtab(path, c );
                editor.editor = ace.edit(editor.id);
                var mode = "coldfusion";
                var re = /(?:\.([^.]+))?$/;
                var ext = re.exec(path)[1];
                if(Config.defaultfilemap[ext]) mode = Config.defaultfilemap[ext];
                editor.editor.getSession().setMode("ace/mode/"+mode);
                editor.editor.setValue(c, -1);
                editor.editor.clearSelection();
                editor.editor.setReadOnly(true);
                //editor.$mouseHandler.setOption('scrollSpeed', 3); https://github.com/ajaxorg/ace/wiki/Configuring-Ace
                /*
                editor.editor.on("input", function() {
                    Util.togglesave(path);
                });
                */
                app.editors.push(editor);
                app.editorfilepath = path;
                BuggerPage.rerenderbreakpoints();
            } else if (changed){
                var mode = "coldfusion";
                var re = /(?:\.([^.]+))?$/;
                var ext = re.exec(path)[1];
                if(Config.defaultfilemap[ext]) mode = Config.defaultfilemap[ext];
                editor.editor.getSession().setMode("ace/mode/"+mode);
                editor.editor.setValue(c, -1);
                editor.editor.clearSelection();
                editor.editor.setReadOnly(true);
            }

            $('#editortabs a[href="#'+editor.id+'"]').trigger('click');

        }

        public static addeditorandselect(path:string,line:number,stepline:boolean) : void {
            path = Util.normalPath(path);
            var c = Util.loadfile(path, function(obj:any){
                //console.log(obj);
                var s = stepline;
                var l = line;
                var p = path;
                EditorController.setFile(obj);
                var e = app.getactiveeditor();
                BuggerPage.refreshEditor(e,p,l,s);                
            });
        }
    }
}