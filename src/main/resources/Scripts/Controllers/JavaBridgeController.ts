
//wrapper of bridgecontroller object, which is the javabridgecontroller.class on java side
namespace Coldbug {

    declare var user:User;

    export class JavaBridgeController{

        public getprojecttree(callback:Function) : boolean {
            Util.sendAjaxRequest({"method":"getprojecttree"},callback);
            return false;
        }

        public getwatch(callback:Function) : boolean {
            Util.sendAjaxRequest({"method":"getwatch"},callback);
            return false;
        }

        public forgotpassword(email:String) : boolean {
            Util.sendAjaxRequest({"method":"forgotpassword","email":email},null);
            return false;
        }

        public openproject(index:number,callback:Function) : boolean {
            Util.sendAjaxRequest({"method":"openproject","index":index},callback);
            return false;
        }

        public savejson(path:string,json:string,callback:Function) : boolean {
            Util.sendAjaxRequest({"method":"savejson","file":path,"json":json},callback);
            return false;
        }

        public readjson(path:string,callback:Function) : boolean {
            Util.sendAjaxRequest({"method":"readjson","path":path},callback);
            return false; 
        }

        public login(email:string,password:string,page:LoginPage) : boolean {
            Util.sendAjaxRequest({"method":"login","email":email,"password":password},page.login_handler);
            return false; 
        }

        public register(email:string,password:string,callback:Function) : boolean {
            Util.sendAjaxRequest({"method":"register","email":email,"password":password},callback);
            return false; 
        }

        public disconnect(callback:Function) : boolean {
            Util.sendAjaxRequest({"method":"disconnect",},callback);
            return false; 
        }

        public kill() : boolean {
            //Util.sendAjaxRequest({"method":"kill",},null);
            return false; 
        }

        public connect(callback:Function) : boolean {
            Util.sendAjaxRequest({"method":"connect"},callback);
            return false; 
        }

        public updateuser(form:any,callback:Function) : boolean {
            Util.sendAjaxRequest(form,callback);
            return false; 
        }

        public logout() : void {
            user = new User();
            Util.sendAjaxRequest({"method":"logout"},null); 
        }

        public loadfile(path:string,callback:Function) : boolean{
            Util.sendAjaxRequest({"method":"loadfile","path":path},callback);
            return false; 
        }

        public setbreakpoints(json:string,callback:Function) : boolean{
            Util.sendAjaxRequest({"method":"setbreakpoints","json":json},callback);
            return false; 
        }

        public actionbuttonclick(action:string,callback?:Function) : boolean{
            Util.sendAjaxRequest({"method":"actionbuttonclick","action":action},callback);
            Util.clearline(app.stepline);
            return false; 
        }

        public dump(val:string,elem:string,callback?:Function) : boolean{
            Util.sendAjaxRequest({"method":"dump","action":val,"elem":elem},callback);
            return false; 
        }

        public comparehash(path:string,hash:string,callback:Function) : boolean{
            Util.sendAjaxRequest({"method":"comparehash","path":path,"hash":hash},callback);
            return false; 
        }        
    }
}