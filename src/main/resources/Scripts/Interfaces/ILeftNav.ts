namespace Coldbug {
    export interface ILeftNav {
        icon:string;
        clickhandler:Function;
        id:string;
        name:string;
        href:string;
    }
}