namespace Coldbug {
    export interface IActionButton {
        icon:string;
        id:string;
        clickhandler:Function;
        description:string;
        defaulthide:boolean;
        styles:{[key:string]:string};
        classes:string;
        info:string;
    }
}