namespace Coldbug {
    export interface IShowHide {
        showhide() : void;
    }
}