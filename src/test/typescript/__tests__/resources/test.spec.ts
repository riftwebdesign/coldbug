describe("Example Tests", function () {
    //var CUT: RiftWebDesign.Server;
    beforeEach(function () {
        //CUT = new RiftWebDesign.Server();  
        //CUT.start(); 
    });    

    afterEach(function () {
        
    });   
    
    it("ExampleTest", function () {
        //ARRANGE

        //ACT  

        //ASSERT
        //setTimeout(function(){expect(CUT.server.port).toEqual(8081)},2000);
    });

    it("ExampleCallbackTest", function (done) {
        //ARRANGE

        //ACT  
        var r = async function(){
            return await new Promise<string>((resolve, reject) => { resolve("Hello World"); });
        };

        //ASSERT
        r().then(function(v){
            expect(v).toEqual("Hello World");
            done();
        }).catch(function(e){
        });
    });

    it("ExampleCallbackChain", function (done) {
        //ARRANGE

        //ACT  
        var r1 = async function(){
            return await new Promise<string>((resolve, reject) => { resolve("Hello World"); });
        };
        var r2 = async function(){
            return await new Promise<number>((resolve, reject) => { resolve(1); });
        };
        var r3 = async function(){
            return await new Promise<number>((resolve, reject) => {  reject(new Error('failed')); resolve(2);  });
        };

        //ASSERT
        r1().then(function(v){
            expect(v).toEqual("Hello World");     
            return r2();       
        }).then(function(v){
            expect(v).toEqual(1); 
            return r3();
        }).then(function(v){
            expect(v).toEqual(3); //this should not get hit so we will faile here
        }).catch(function(e:Error){
            expect(e.message).toEqual("failed");
            done();
        });
    });
    
});
