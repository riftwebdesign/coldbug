package com.riftwebdesign.coldbug.tests.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.Image;

import com.riftwebdesign.coldbug.util.Enums;
import com.riftwebdesign.coldbug.util.Helpers;

import org.junit.jupiter.api.Test;

public class helpersTest {
    @Test
    public void logstring_test() throws Exception {
        //ARRANGE
        //ACT
        String r = Helpers.log("test message"); //this must be on line 14 or update the blow line to match this      

        //ASSERT
        assertEquals("test message   [helpersTest.logstring_test():17]", r);
    }
    @Test
    public void getimagefile_test() throws Exception {
        //ARRANGE
        //ACT
        Image r = Helpers.getImageFile(Enums.Paths.Images.TrayIcon);       

        //ASSERT
        assertEquals(true, r != null);
    }
}