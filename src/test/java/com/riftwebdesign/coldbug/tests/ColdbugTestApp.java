package com.riftwebdesign.coldbug.tests;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;

import org.apache.tomcat.util.file.ConfigurationSource;

import lucee.cli.servlet.ServletConfigImpl;
import lucee.cli.servlet.ServletContextImpl;
import lucee.loader.engine.CFMLEngine;
import lucee.loader.engine.CFMLEngineFactory;
import lucee.runtime.PageContext;
import lucee.runtime.type.Collection.Key;


public class ColdbugTestApp {

	public class dynamickey implements lucee.runtime.type.Collection.Key {
		private static final long serialVersionUID = 1L;
		private String s;
		public dynamickey(String v){
			this.s = v;
		}
		@Override
		public String getString() { return s; }
		@Override
		public String getLowerString() { return s.toLowerCase(); }
		@Override
		public String getUpperString() { return s.toUpperCase(); }
		@Override
		public char charAt(int index) { return s.charAt(index); }
		@Override
		public char lowerCharAt(int index) { return s.toLowerCase().charAt(index); }
		@Override
		public char upperCharAt(int index) { return s.toUpperCase().charAt(index); }
		@Override
		public boolean equalsIgnoreCase(Key key) { return key.getLowerString() == s.toLowerCase(); }
		@Override
		public long hash() { return s.hashCode(); }
		@Override
		public int length() { return s.length(); }
	}

	public class myconfig implements ConfigurationSource {
		String path = "file:///C:/Projects/apps/ColdBug/ColdBug/Webs/ColdBugServer";
		public myconfig(){
		}

		@Override
		public Resource getResource(String name) throws IOException {
			URI u = null;
			try {
				u = new URI(this.path + "/" + name);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			Resource r = new Resource(null,u);
			return r;
		}

		@Override
		public URI getURI(String name) {
			try {
				return new URI(this.path + "/" + name);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			return null;
		}

	}

    public static void main(String[] args) throws Exception {       
		//javax.script.ScriptEngineManager tr = new javax.script.ScriptEngineManager();

        final String root = "C:\\Projects\\apps\\ColdBug\\ColdBug\\Webs\\ColdBugWebsite";
        final Map<String, Object> attributes = new HashMap<String, Object>();
        final Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("lucee-server-directory", new File(root, "WEB-INF").getAbsolutePath());
        ServletContextImpl servletContext = new ServletContextImpl(new File(root), attributes, parameters, 1, 0);
        ServletConfigImpl servletConfig = new ServletConfigImpl(servletContext, "CFMLServlet");
        CFMLEngine engine = CFMLEngineFactory.getInstance();
        engine.addServletConfig(servletConfig);
    
		javax.servlet.http.Cookie[] cookies = new Cookie[]{new Cookie("myCookie","myCookieValue")};
		Map<String, Object> headers=new HashMap<String, Object>();
		headers.put("accept","text/html");
		PageContext pc = engine.createPageContext(
            null
            ,"localhost" // host
            ,"/index.cfm" // script name
            ,"" // query string
            ,cookies
            ,headers
            ,parameters
            ,attributes
            ,System.out // response stream where the output is written to
            ,50000 // timeout for the simulated request in milli seconds
            ,true // register the pc to the thread
        );
		try{
			ColdbugTestApp x = new com.riftwebdesign.coldbug.tests.ColdbugTestApp();
			dynamickey k = x.new dynamickey("REMOTE_HOST"); //todo fix this wierd shit
            System.out.println( pc.cgiScope().get(k) ); //"REMOTE_HOST") );
            //System.out.println( pc.urlScope().get("test") );
            System.out.println( pc.getConfig().getRootDirectory() );
            pc.executeCFML("\\index.cfm", false, false);
            //System.out.println( pc.getConfig(). );
		} catch(Exception e){
            e.printStackTrace();
        }
		finally{
			engine.releasePageContext(pc, true/* unregister the pc from the thread*/);
        }
}









// int t = 1;
        // System.out.println(t);
		// if(1==1) return;

		//Tomcat tomcat = new Tomcat();
		// tomcat.setPort(8889);
		// tomcat.setHostname("coldzapper");
		//tomcat.getHost().setAppBase("");

		//File docBase = new File(System.getProperty("java.io.tmpdir"));
		//Context context = tomcat.addContext("", "C:\\Projects\\apps\\ColdBug\\ColdBug\\Webs\\ColdBugWebsite");//docBase.getAbsolutePath());

		// Tomcat.addServlet( context, S(/*
		// 	<servlet-mapping>
		// 		<servlet-name>CFMLServlet</servlet-name>
		// 		<url-pattern>*.cfc</url-pattern>
		// 		<url-pattern>*.cfm</url-pattern>
		// 		<url-pattern>*.cfml</url-pattern>   
		// 		<url-pattern>/index.cfc/*</url-pattern>
		// 		<url-pattern>/index.cfm/*</url-pattern>
		// 		<url-pattern>/index.cfml/*</url-pattern>
		// 	</servlet-mapping>
		// */));

		// Class servletClass = lucee.loader.servlet.CFMLServlet.class;
		// Tomcat.addServlet( context, servletClass.getSimpleName(), new lucee.loader.servlet.CFMLServlet());

		// context.addServletMappingDecoded("*.cfc", servletClass.getSimpleName(), true);
		// context.addServletMappingDecoded("*.cfm", servletClass.getSimpleName(), true);
		// context.addServletMappingDecoded("*.cfml", servletClass.getSimpleName(), true);
		// context.addServletMappingDecoded("/index.cfc/*", servletClass.getSimpleName(), true);
		// context.addServletMappingDecoded("/index.cfm/*", servletClass.getSimpleName(), true);
		// context.addServletMappingDecoded("/index.cfml/*", servletClass.getSimpleName(), true);


		
		// ConfigurationSource c = new ColdbugTestApp().new myconfig();
		// tomcat.init(c);
		// tomcat.start();
        // tomcat.getServer().await();

		// if(1==1) return;



		// From: http://blog.efftinge.de/2008/10/multi-line-string-literals-in-java.html
// Takes a comment (/**/) and turns everything inside the comment to a string that is returned from S()
public static String S() {
    StackTraceElement element = new RuntimeException().getStackTrace()[1];
    String name = element.getClassName().replace('.', '/') + ".java";
    //StringBuilder sb = new StringBuilder();
    //String line = null;
    InputStream in = ColdbugTestApp.class.getClassLoader().getResourceAsStream(name);
    String s = convertStreamToString(in, element.getLineNumber());
    return s.substring(s.indexOf("/*")+2, s.indexOf("*/"));
}

// From http://www.kodejava.org/examples/266.html
private static String convertStreamToString(InputStream is, int lineNum) {
    /*
     * To convert the InputStream to String we use the BufferedReader.readLine()
     * method. We iterate until the BufferedReader return null which means
     * there's no more data to read. Each line will appended to a StringBuilder
     * and returned as String.
     */
    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
    StringBuilder sb = new StringBuilder();

    String line = null; int i = 1;
    try {
        while ((line = reader.readLine()) != null) {
            if (i++ >= lineNum) {
                sb.append(line + "\n");
            }
        }
    } catch (IOException e) {
        e.printStackTrace();
    } finally {
        try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    return sb.toString();
}
	
}
