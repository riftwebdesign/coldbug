package com.riftwebdesign.coldbug.tests;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;

import com.sun.jdi.Bootstrap;
import com.sun.jdi.LocalVariable;
import com.sun.jdi.StackFrame;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.Value;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.VirtualMachineManager;
import com.sun.jdi.connect.Connector.Argument;
import com.sun.jdi.connect.LaunchingConnector;
import com.sun.jdi.event.BreakpointEvent;
import com.sun.jdi.event.ClassPrepareEvent;
import com.sun.jdi.event.Event;
import com.sun.jdi.event.EventIterator;
import com.sun.jdi.event.EventQueue;
import com.sun.jdi.event.EventSet;
import com.sun.jdi.request.ClassPrepareRequest;
import com.sun.jdi.request.EventRequestManager;

import org.apache.commons.io.IOUtils;

import lucee.cli.servlet.ServletConfigImpl;
import lucee.cli.servlet.ServletContextImpl;
import lucee.loader.engine.CFMLEngine;
import lucee.loader.engine.CFMLEngineFactory;
import lucee.runtime.PageContext;

/**
 * Created by sid on 11/19/2017.
 */
public class JustTestingTest {
    public class dynamickey implements lucee.runtime.type.Collection.Key {
		private static final long serialVersionUID = 1L;
		private String s;
		public dynamickey(String v){
			this.s = v;
		}
		@Override
		public String getString() { return s; }
		@Override
		public String getLowerString() { return s.toLowerCase(); }
		@Override
		public String getUpperString() { return s.toUpperCase(); }
		@Override
		public char charAt(int index) { return s.charAt(index); }
		@Override
		public char lowerCharAt(int index) { return s.toLowerCase().charAt(index); }
		@Override
		public char upperCharAt(int index) { return s.toUpperCase().charAt(index); }
		@Override
		public boolean equalsIgnoreCase(lucee.runtime.type.Collection.Key key) { return key.getLowerString() == s.toLowerCase(); }
		@Override
		public long hash() { return s.hashCode(); }
		@Override
		public int length() { return s.length(); }
    }
    
    //@Test
    public void testGetthread5() throws Exception {
        ColdbugTestApp.main(null);
    }
    //@Test
    public void testGetthread() throws Exception {
        // System.out.println( System.getProperty("java.class.path") );
        // System.out.println(this.getClass().getProtectionDomain().getCodeSource().getLocation());
        // System.out.println(System.getProperty("user.dir"));

        //CFMLEngine e = CFMLEngineImpl.getInstance();
        final String root = "C:\\Projects\\apps\\ColdBug\\ColdBug\\Webs\\ColdBugWebsite";
        final Map<String, Object> attributes = new HashMap<String, Object>();
        final Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("lucee-server-directory", new File(root, "WEB-INF").getAbsolutePath());
        ServletContextImpl servletContext = new ServletContextImpl(new File(root), attributes, parameters, 1, 0);
        ServletConfigImpl servletConfig = new ServletConfigImpl(servletContext, "CFMLServlet");
        CFMLEngine engine = CFMLEngineFactory.getInstance();
        engine.addServletConfig(servletConfig);
    
		javax.servlet.http.Cookie[] cookies = new Cookie[]{new Cookie("myCookie","myCookieValue")};
		Map<String, Object> headers=new HashMap<String, Object>();
		headers.put("accept","text/html");
		PageContext pc = engine.createPageContext(
            null
            ,"localhost" // host
            ,"/index.cfm" // script name
            ,"" // query string
            ,cookies
            ,headers
            ,parameters
            ,attributes
            ,System.out // response stream where the output is written to
            ,50000 // timeout for the simulated request in milli seconds
            ,true // register the pc to the thread
        );
		try{

            
            JustTestingTest x = new JustTestingTest();
            System.out.println( pc.cgiScope().get(x.new dynamickey("REMOTE_HOST"))   );
            //System.out.println( pc.urlScope().get("test") );
            System.out.println( pc.getConfig().getRootDirectory() );
            pc.executeCFML("\\index.cfm", false, false);
            //System.out.println( pc.getConfig(). );
		} catch(Exception e){
            e.printStackTrace();
        }
		finally{
			engine.releasePageContext(pc, true/* unregister the pc from the thread*/);
        }
        
        //
    }








    //@Test
    public void testGetthread2() throws Exception {        
        new VMRunner().start();        
        for(;;){ }
    }

    class VMRunner extends Thread {
        public void run () {
            try{
                safeStart();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public VirtualMachine safeStart() throws Exception
    {
        VirtualMachineManager vmm = Bootstrap.virtualMachineManager();
        LaunchingConnector connector = vmm.defaultConnector();
        Map<String, Argument> cArgs = connector.defaultArguments();       

        cArgs.get("options").setValue("-cp \""+System.getProperty("java.class.path")+"\"");
        cArgs.get("suspend").setValue("true");
        //cArgs.get("main").setValue("com.riftwebdesign.coldbug.ColdBug");
        cArgs.get("main").setValue("com.riftwebdesign.coldbug.tests.ColdbugTestApp");

        final VirtualMachine vm = connector.launch(cArgs);
        //connect to vm
        //set a known breakpoint
        //if not hit, then fail
        //at BP get the variable
        //test the variable
        //always release the vm

        final Thread outThread = new Thread() { 
            public void run () {
                try {                
                    IOUtils.copy(vm.process().getInputStream(), System.out);
                } catch (Exception e) {
                    System.err.println ("StreamRedirecter: " + e);
                }
            }
        };
        final Thread errThread = new Thread() { 
            public void run () {
                try {                
                    IOUtils.copy(vm.process().getErrorStream(), System.err);
                } catch (Exception e) {
                    System.err.println ("StreamRedirecter: " + e);
                }
            }
        };
        outThread.setDaemon(true);
        outThread.start();
        errThread.setDaemon(true);
        errThread.start();
        //

        // create a class prepare request
        EventRequestManager erm = vm.eventRequestManager();
        ClassPrepareRequest r = erm.createClassPrepareRequest();
        r.addClassFilter("common.template_cfm$cf");
        r.enable();
        vm.resume();

        EventQueue queue = vm.eventQueue();
        while (true) {
            EventSet eventSet = queue.remove();
            EventIterator it = eventSet.eventIterator();
            while (it.hasNext()) {
                Event event = it.nextEvent();
                if (event instanceof ClassPrepareEvent) {
                    ClassPrepareEvent evt = (ClassPrepareEvent) event;
                    System.out.println( evt.referenceType().name() );
                    
                    //evt.referenceType().allLineLocations()

                    // classType.methodsByName(methodName).forEach(new Consumer<Method>() {
                    //     @Override
                    //     public void accept(Method m) {
                    //         List<Location> locations = null;
                    //         try {
                    //             locations = m.allLineLocations();
                    //         } catch (AbsentInformationException ex) {
                    //             ex.printStackTrace();
                    //         }
                    //         Location location = locations.get(locations.size() - 1);
                    //         BreakpointRequest bpReq = erm.createBreakpointRequest(location);
                    //         bpReq.enable();
                    //     }
                    // });

                }
                if (event instanceof BreakpointEvent) {
                    // disable the breakpoint event
                    event.request().disable();

                    ThreadReference thread = ((BreakpointEvent) event).thread();
                    StackFrame stackFrame = thread.frame(0);

                    // print all the visible variables with the respective values
                    Map<LocalVariable, Value> visibleVariables = (Map<LocalVariable, Value>) stackFrame.getValues(stackFrame.visibleVariables());
                    for (Map.Entry<LocalVariable, Value> entry : visibleVariables.entrySet()) {
                        System.out.println(entry.getKey() + ":" + entry.getValue());
                    }
                }
                vm.resume();
            }
        }

        // Runtime.getRuntime().addShutdownHook(new Thread() {
        //     public void run() {
        //         outThread.interrupt();
        //         errThread.interrupt();
        //         vm.process().destroyForcibly();
        //     }
        // });

        //return vm;
    }


}


