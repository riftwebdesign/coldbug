package com.riftwebdesign.coldbug.tests.resources.models;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.riftwebdesign.coldbug.handlers.routehandler;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import spark.Spark;

public class userTest {

    private static routehandler rh;

    @BeforeAll
    private static void beforeAll() {
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(java.util.logging.Level.OFF);
        userTest.rh = new routehandler();
        rh.register();
    }

    @AfterAll
    private static void afterAll() {
        Spark.stop();
    }

    //@Test
    public void properties_test() throws Exception {
        //ARRANGE
        WebClient webClient = new WebClient(BrowserVersion.FIREFOX_60);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        // webClient.setJavaScriptErrorListener(new JavaScriptErrorListener(){        
        //     @Override
        //     public void warn(String message, String sourceName, int line, String lineSource, int lineOffset) {
                
        //     }
        
        //     @Override
        //     public void timeoutError(HtmlPage page, long allowedTime, long executionTime) {
                
        //     }
        
        //     @Override
        //     public void scriptException(HtmlPage page, ScriptException scriptException) {
                
        //     }
        
        //     @Override
        //     public void malformedScriptURL(HtmlPage page, String url, MalformedURLException malformedURLException) {
                
        //     }
        
        //     @Override
        //     public void loadScriptError(HtmlPage page, URL scriptUrl, Exception exception) {
                
        //     }
        // });
        //HtmlPage page = webClient.getPage("http://localhost:4567/?&testsite=1&log=1");
       webClient.getPage("http://localhost:4567/?&testsite=1&log=1");

        //ACT        
        webClient.waitForBackgroundJavaScript(10000); 
        webClient.close();

        //ASSERT
    }

}