package com.riftwebdesign.coldbug.tests.engines;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.riftwebdesign.coldbug.ColdBug;
import com.riftwebdesign.coldbug.handlers.clientsidehandler;
import com.riftwebdesign.coldbug.pojo.*;
import com.riftwebdesign.coldbug.controllers.pagecontextcontroller;
import com.riftwebdesign.coldbug.engines.*;
import com.riftwebdesign.coldbug.handlers.*;

public class factoryTest {
    @Test
    public void systemtray_test() throws Exception {
        //ARRANGE
        factory cut = new factory();

        //ACT
        systemtrayhandler r = cut.createsystemtray();
        boolean flag = r instanceof systemtrayhandler;

        //ASSERT
        assertEquals(true, flag);
    }
    @Test
    public void coldbug_test() throws Exception {
        //ARRANGE
        factory cut = new factory();

        //ACT
        ColdBug r = cut.createcoldbug();
        boolean flag = r instanceof ColdBug;

        //ASSERT
        assertEquals(true, flag);
    }
    @Test
    public void staticfactory_test() throws Exception {
        //ARRANGE
        factory cut = new factory();

        //ACT
        factory r = cut.getinstance();
        boolean flag = r instanceof factory;

        //ASSERT
        assertEquals(true, flag);
    }
    @Test
    public void pagecontextcontroller_test() throws Exception {
        //ARRANGE
        factory cut = new factory();

        //ACT
        pagecontextcontroller r = cut.createpagecontextcontroller();
        boolean flag = r instanceof pagecontextcontroller;

        //ASSERT
        assertEquals(true, flag);
    }
    @Test
    public void clientsidehandler_test() throws Exception {
        //ARRANGE
        factory cut = new factory();

        //ACT
        clientsidehandler r = cut.createclientsidehandler();
        boolean flag = r instanceof clientsidehandler;

        //ASSERT
        assertEquals(true, flag);
    }
    @Test
    public void routehandler_test() throws Exception {
        //ARRANGE
        factory cut = new factory();

        //ACT
        routehandler r = cut.createroutehandler();
        boolean flag = r instanceof routehandler;

        //ASSERT
        assertEquals(true, flag);
    }
    @Test
    public void browserhandler_test() throws Exception {
        //ARRANGE
        factory cut = new factory();

        //ACT
        browserhandler r = cut.createbrowserhandler();
        boolean flag = r instanceof browserhandler;

        //ASSERT
        assertEquals(true, flag);
    }

    @Test
    public void heartbeathandler_test() throws Exception {
        //ARRANGE
        factory cut = new factory();

        //ACT
        heartbeathandler r = cut.createheartbeathandler();
        boolean flag = r instanceof heartbeathandler;

        //ASSERT
        assertEquals(true, flag);
    }
    @Test
    public void jsonresponse_test() throws Exception {
        //ARRANGE
        factory cut = new factory();

        //ACT
        jsonresponse r = cut.createjsonresponse();
        boolean flag = r instanceof jsonresponse;

        //ASSERT
        assertEquals(true, flag);
    }
    @Test
    public void login_test() throws Exception {
        //ARRANGE
        factory cut = new factory();

        //ACT
        login r = cut.createlogin();
        boolean flag = r instanceof login;

        //ASSERT
        assertEquals(true, flag);
    }
    @Test
    public void socketresponse_test() throws Exception {
        //ARRANGE
        factory cut = new factory();

        //ACT
        socketresponse r = cut.createsocketresponse();
        boolean flag = r instanceof socketresponse;

        //ASSERT
        assertEquals(true, flag);
    }
    @Test
    public void filechangeresponse_test() throws Exception {
        //ARRANGE
        factory cut = new factory();

        //ACT
        filechangeresponse r = cut.createfilechangeresponse();
        boolean flag = r instanceof filechangeresponse;

        //ASSERT
        assertEquals(true, flag);
    }
}