package com.riftwebdesign.coldbug.tests.handlers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.riftwebdesign.coldbug.handlers.apihandler;
import com.riftwebdesign.coldbug.handlers.heartbeathandler;
import com.riftwebdesign.coldbug.tests.testclasses.testabstract;

import org.junit.jupiter.api.Test;

public class heartbeathandlerTest extends testabstract {

    @Test
    public void callapiheartbeat_loggedin_test(){
        //ARRANGE
        booltester flag = new booltester();
        heartbeathandler cut = new heartbeathandler();
        apihandler a = new apihandler(){
            @Override
            public String serviceRequest(String query) throws Exception {
                flag.setVal("method=heartbeat".equals(query));
                return null;
            }
        };
        cut.apicontroller = a;

        //ACT
        cut.callapiheartbeat(true);

        //ASSERT
        assertEquals(true, flag.getVal());
    }

    @Test
    public void callapiheartbeat_notloggedin_test(){
        //ARRANGE
        booltester flag = new booltester();
        heartbeathandler cut = new heartbeathandler();
        apihandler a = new apihandler(){
            @Override
            public String serviceRequest(String query) throws Exception {
                flag.setVal("method=heartbeat".equals(query));
                return null;
            }
        };

        //ACT
        cut.callapiheartbeat(false);
        cut.apicontroller = a;

        //ASSERT
        assertEquals(false, flag.getVal());
    }

    //todo test all methods

}