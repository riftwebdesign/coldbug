package com.riftwebdesign.coldbug.tests.handlers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.MenuItem;
import java.awt.PopupMenu;

import com.riftwebdesign.coldbug.tests.testclasses.testabstract;
import com.riftwebdesign.coldbug.handlers.systemtrayhandler;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class systemtrayhandlerTest extends testabstract {
    @Test
    public void issupported_test() throws Exception {
        //ARRANGE
        systemtrayhandler cut = new systemtrayhandler();

        //ACT
        boolean r = cut.issupported();

        //ASSERT
        assertEquals(true, r);
    }
    @Test
    public void getsystemtray_test() throws Exception {
        //ARRANGE
        systemtrayhandler cut = new systemtrayhandler();

        //ACT
        //ASSERT
        Assertions.assertThrows(NullPointerException.class, () ->{
            cut.add(null);  
        });
    }
    @Test
    public void gettrayicon_test() throws Exception {
        //ARRANGE
        systemtrayhandler cut = new systemtrayhandler();

        //ACT
        //ASSERT
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            cut.gettrayicon(null,null); 
        });
    }
    @Test
    public void getclose_test() throws Exception {
        //ARRANGE
        systemtrayhandler cut = new systemtrayhandler();

        //ACT
        MenuItem r = cut.getclose();

        //ASSERT
        assertEquals(true, r instanceof MenuItem);
    }
    @Test
    public void getpopupmenu_test() throws Exception {
        //ARRANGE
        systemtrayhandler cut = new systemtrayhandler();

        //ACT
        PopupMenu r = cut.getpopupmenu();

        //ASSERT
        assertEquals(true, r instanceof PopupMenu);
    }
    @Test
    public void addtrayicon_test() throws Exception {
        //ARRANGE
        final booltester flag = new booltester();
        systemtrayhandler cut = new systemtrayhandler(){
            @Override
            public void addTrayIcon() {
                flag.setVal(true);
            }
        };

        //ACT
        cut.addTrayIcon();

        //ASSERT
        assertEquals(true, flag.getVal());
    }
}