package com.riftwebdesign.coldbug.tests.handlers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.InputStream;

import com.google.gson.JsonObject;
import com.riftwebdesign.coldbug.handlers.apihandler;
import com.riftwebdesign.coldbug.pojo.interfaces.ihttpurlconnection;
import com.riftwebdesign.coldbug.pojo.interfaces.iurl;
import com.riftwebdesign.coldbug.tests.testclasses.testabstract;

import org.junit.jupiter.api.Test;


public class apihandlerTest extends testabstract {
    @Test
    public void getconnection_test() throws Exception {
        //ARRANGE
        booltester flag = new booltester();
        apihandler cut = new apihandler(){
            @Override
            public iurl geturl(String url) {
                iurl r = new iurl(){
                    @Override
                    public ihttpurlconnection openConnection() {
                        flag.setVal(true);
                        return null;
                    }
                };
                return r;
            }
        };
        
        //ACT        
        ihttpurlconnection r = cut.getconnection("method=heartbeat");

        //ASSERT
        assertEquals(true, flag.getVal());
        assertEquals(true, r == null);
    }
    @Test
    public void setconnectionproperties_test() throws Exception {
        //ARRANGE
        booltester getflag = new booltester();
        booltester cookieflag = new booltester();
        apihandler cut = new apihandler();
        apihandler.cookie = "12345";
        ihttpurlconnection conn = new ihttpurlconnection(){        
            @Override
            public void setRequestProperty(String key, String val) {
                cookieflag.setVal( key.equals("Cookie") && val.equals("12345") );
            }
        
            @Override
            public void setRequestMethod(String method) {
                getflag.setVal( method.equals("GET") );
            }
        
            @Override
            public InputStream getInputStream() {
                return null;
            }
        };
        
        //ACT        
        cut.setconnectionproperties(conn);
        apihandler.cookie = "";

        //ASSERT
        assertEquals(true, getflag.getVal());
        assertEquals(true, cookieflag.getVal());
    }
    @Test
    public void setcookie_test() throws Exception {
        //ARRANGE
        apihandler cut = new apihandler();
        apihandler.cookie = ""; 
        String cookie = "{\"cfid\":\"12345\",\"cftoken\":\"54321\"}";       
        
        //ACT        
        cut.setcookie(cookie);
        boolean flag = apihandler.cookie.equals("cfid=12345; cftoken=54321;");
        apihandler.cookie = "";        

        //ASSERT
        assertEquals(true, flag);
    }
    @Test
    public void servicerequest_test() throws Exception {
        //ARRANGE
        countertester flag = new countertester();
        apihandler cut = new apihandler(){
            @Override
            public ihttpurlconnection getconnection(String query) {
                flag.increment();
                return null;
            }
            @Override
            public void setconnectionproperties(ihttpurlconnection conn) {
                flag.increment();
            }
            @Override
            public String readconnection(ihttpurlconnection conn) {
                flag.increment();
                return null;
            }
            @Override
            public void setcookie(String result) {
                flag.increment();
            }
        };    
        
        //ACT        
        String r = cut.serviceRequest("method=heartbeat");  

        //ASSERT
        assertEquals(true, r == null);
        assertEquals(true, flag.getVal() == 4);
    }
    @Test
    public void servicerequestjson_test() throws Exception {
        //ARRANGE
        booltester flag = new booltester();
        apihandler cut = new apihandler(){
            @Override
            public String serviceRequest(String query) {
                flag.setVal(true);
                return "";
            }
        };    
        
        //ACT        
        JsonObject r = cut.serviceRequestoJson("method=heartbeat");  

        //ASSERT
        assertEquals(true, r == null);
        assertEquals(true, flag.getVal());
    }
}