package com.riftwebdesign.coldbug.tests.handlers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;

import com.riftwebdesign.coldbug.handlers.browserhandler;

import org.junit.jupiter.api.Test;


public class browserhandlerTest {
    @Test
    public void geturl_test() throws Exception {
        //ARRANGE
        browserhandler cut = new browserhandler();
        
        //ACT        
        URI r = cut.geturl();

        //ASSERT
        assertEquals(true, r.toString().equals("http://localhost:4567/?&debug=1&log=1"));
    }
}