package com.riftwebdesign.coldbug.tests.handlers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;

import com.riftwebdesign.coldbug.handlers.routehandler;
import com.riftwebdesign.coldbug.pojo.interfaces.isparkrequest;
import com.riftwebdesign.coldbug.pojo.interfaces.isparkresponse;

import org.junit.jupiter.api.Test;

public class routehandlerTest {
    
    public isparkresponse mockisparkresponse(){
        return new isparkresponse(){
            public HashMap<String,String> headers = new HashMap<>();
            @Override
            public void header(String header, String value) {
                this.headers.put(header, value);
            }

            @Override
            public HashMap<String, String> getheaders() {
                return headers;
            }
        };
    }

    public isparkrequest mockisparkrequest(String uri, String queryString){
        return new isparkrequest(){        
            @Override
            public String uri() {
                return uri;
            }
        
            @Override
            public String queryString() {
                return queryString;
            }
        
            @Override
            public String queryParams(String key) {
                String r = null;
                for(String p : this.queryString().split("&")){
                    String[] a = p.split("=");
                    if(a[0].equals(key))
                        r=a[1];
                }
                return r;
            }
        };
    }

    @Test
    public void appendwildcard_test() throws Exception {
        //ARRANGE
        routehandler cut = new routehandler();

        //ACT
        String r = cut.appendwildcard("/test");   

        //ASSERT
        assertEquals("/test/*", r);
    }

    @Test
    public void handleroot_test() throws Exception {
        //ARRANGE
        routehandler cut = new routehandler();

        //ACT
        String r = cut.handleroot(mockisparkrequest(null, null),mockisparkresponse());   

        //ASSERT
        assertEquals(true, r.contains("<!DOCTYPE html>"));
    }

    @Test
    public void handlegeneric_test() throws Exception {
        //ARRANGE
        routehandler cut = new routehandler();

        //ACT
        isparkresponse res = mockisparkresponse();
        String r = cut.handlegeneric(mockisparkrequest("/Scripts/bridge.js", null),res);   

        //ASSERT
        assertEquals(true, r != null);
        assertEquals(true, res.getheaders().containsKey("content-type"));
    }

    @Test
    public void handleapi_test() throws Exception {
        //ARRANGE
        routehandler cut = new routehandler(){
            @Override
            public String callclientmethod(isparkrequest req) throws Exception {
                return "method called";
            }
        };

        //ACT
        isparkresponse res = mockisparkresponse();
        String r = cut.handleapi(mockisparkrequest(null, null),res);   

        //ASSERT
        assertEquals(true, r.equals("method called"));
        assertEquals(true, res.getheaders().get("content-type").equals("application/json"));
    }

    @Test
    public void callclientmethod_test() throws Exception {
        //ARRANGE
        routehandler cut = new routehandler();

        //ACT
        String r = cut.callclientmethod(mockisparkrequest(null, "&method=echo"));   

        //ASSERT
        assertEquals(true, r.equals("\"echo\""));
    }
}