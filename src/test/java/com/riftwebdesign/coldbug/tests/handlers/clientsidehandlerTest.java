package com.riftwebdesign.coldbug.tests.handlers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.google.gson.JsonObject;
import com.riftwebdesign.coldbug.controllers.WatchController;
import com.riftwebdesign.coldbug.engines.factory;
import com.riftwebdesign.coldbug.handlers.apihandler;
import com.riftwebdesign.coldbug.handlers.clientsidehandler;
import com.riftwebdesign.coldbug.models.Application;
import com.riftwebdesign.coldbug.pojo.jsonresponse;
import com.riftwebdesign.coldbug.pojo.interfaces.isparkrequest;
import com.riftwebdesign.coldbug.tests.testclasses.testabstract;
import com.riftwebdesign.coldbug.util.Helpers;

import org.junit.jupiter.api.Test;


public class clientsidehandlerTest extends testabstract {

    private isparkrequest makerequest(String uri, String queryparams, String queryString){
        return new isparkrequest(){
            @Override
            public String uri() {
                return uri;
            }

            @Override
            public String queryParams(String key) {
                return queryparams;
            }

            @Override
            public String queryString() {
                return queryString;
            }
        };
    }

    @Test
    public void login_true_test() throws Exception {
        //ARRANGE
        clientsidehandler cut = new clientsidehandler();
        apihandler api = new apihandler(){
            @Override
            public String serviceRequest(String query){
                return "{\"success\":true}";
            }
        };
        cut.apicontroller = api;
        isparkrequest req = this.makerequest(null, null, null);
        
        //ACT        
        cut.login(req);
        boolean flag = Application.getInstance().loggedin;
        Application.getInstance().loggedin = false;

        //ASSERT
        assertEquals(true, flag);
    }
    @Test
    public void login_false_test() throws Exception {
        //ARRANGE
        clientsidehandler cut = new clientsidehandler();
        apihandler api = new apihandler(){
            @Override
            public String serviceRequest(String query){
                return "{\"success\":false}";
            }
        };
        cut.apicontroller = api;
        isparkrequest req = this.makerequest(null, null, null);
        
        //ACT        
        cut.login(req);
        boolean flag = Application.getInstance().loggedin;
        Application.getInstance().loggedin = false;

        //ASSERT
        assertEquals(false, flag);
    }
    @Test
    public void logout_test() throws Exception {
        //ARRANGE
        booltester flag = new booltester();
        clientsidehandler cut = new clientsidehandler();
        apihandler api = new apihandler(){
            @Override
            public JsonObject serviceRequestoJson(String query){
                flag.setVal(true);
                return null;
            }
        };
        cut.apicontroller = api;
        isparkrequest req = this.makerequest(null, null, null);
        
        //ACT    
        Application.getInstance().loggedin = true;    
        Object r = cut.logout(req);
        boolean loggedin = Application.getInstance().loggedin;
        Application.getInstance().loggedin = false;       

        //ASSERT
        assertEquals(true, r == null);        
        assertEquals(true, flag.getVal());
        assertEquals(false, loggedin);
    }
    @Test
    public void updateuser_test() throws Exception {
        //ARRANGE
        booltester flag = new booltester();
        clientsidehandler cut = new clientsidehandler();
        apihandler api = new apihandler(){
            @Override
            public String serviceRequest(String query){
                flag.setVal(true);
                return null;
            }
        };
        cut.apicontroller = api;
        isparkrequest req = this.makerequest(null, null, null);
        
        //ACT       
        cut.updateuser(req);      

        //ASSERT
        assertEquals(true, flag.getVal());
    }
    @Test
    public void register_test() throws Exception {
        //ARRANGE
        booltester flag = new booltester();
        clientsidehandler cut = new clientsidehandler();
        apihandler api = new apihandler(){
            @Override
            public String serviceRequest(String query){
                flag.setVal(true);
                return null;
            }
        };
        cut.apicontroller = api;
        isparkrequest req = this.makerequest(null, null, null);
        
        //ACT       
        cut.register(req);      

        //ASSERT
        assertEquals(true, flag.getVal());
    }
    @Test
    public void forgotpassword_test() throws Exception {
        //ARRANGE
        booltester flag = new booltester();
        clientsidehandler cut = new clientsidehandler();
        apihandler api = new apihandler(){
            @Override
            public String serviceRequest(String query){
                flag.setVal(true);
                return null;
            }
        };
        cut.apicontroller = api;
        isparkrequest req = this.makerequest(null, null, null);
        
        //ACT       
        cut.forgotpassword(req);      

        //ASSERT
        assertEquals(true, flag.getVal());
    }
    @Test
    public void readjson_test() throws Exception {
        //ARRANGE
        clientsidehandler cut = new clientsidehandler();
        Helpers helper = new Helpers(){
            @Override
            public String readjson(String file) {
                return "test";
            }
        };
        cut.helper = helper;
        isparkrequest req = this.makerequest(null, "testfile", null);
        
        //ACT       
        jsonresponse r = (jsonresponse)cut.readjson(req);      

        //ASSERT
        assertEquals("test",r.json);
    }
    @Test
    public void savejson_test() throws Exception {
        //ARRANGE
        booltester flag = new booltester();
        clientsidehandler cut = new clientsidehandler(){
            @Override
            public void savejsonfile(String file, String json) {
                flag.setVal(true);
            }
        };        
        isparkrequest req = this.makerequest(null, null, null);
        
        //ACT       
        cut.savejson(req);      

        //ASSERT
        assertEquals(true, flag.getVal());
    }
    @Test
    public void savejsonfile_test() throws Exception {
        //ARRANGE
        booltester flag = new booltester();
        clientsidehandler cut = new clientsidehandler();
        Helpers helper = new Helpers(){
            @Override
            public void savejsonfile(String file, String json) {
                flag.setVal(true);
            }
        };
        cut.helper = helper;
        this.makerequest(null, null, null);
        
        //ACT       
        cut.savejsonfile("test","test");      

        //ASSERT
        assertEquals(true, flag.getVal());
    }
    @Test
    public void openproject_test() throws Exception {
        //ARRANGE
        countertester flag = new countertester();
        clientsidehandler cut = new clientsidehandler();
        factory f = new factory(){
            @Override
            public WatchController watchcontroller() {
                WatchController w = new WatchController(){
                    @Override
                    public void stop_watching() throws Exception {
                        flag.increment();
                    }
                    @Override
                    public void start_watching() throws Exception {
                        flag.increment();
                    }
                };
                return w;
            }
        };
        cut.factory = f;
        isparkrequest req = this.makerequest(null, "1", null);
        
        //ACT       
        cut.openproject(req); 
        Integer r = Application.getInstance().activesettingindex;
        Application.getInstance().activesettingindex = -1;

        //ASSERT
        assertEquals(2, flag.getVal());
        assertEquals(1, r);
    }    

    @Test
    public void connect_test() throws Exception {
        //ARRANGE

        //ACT       

        //ASSERT
    }
    public void setbreakpoints_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void loadfile_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void comparehash_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void getwatch_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void getprojecttree_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void kill_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void getfullprojecttree_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void sortdirtree_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void getallfiles_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void disconnect_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void updatewatches_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void updateline_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void updatevariablesbar_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void generatedirtree_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void actionbuttonclick_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void dump_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
    public void updatedumppane_test() throws Exception {
        //ARRANGE
        throw new UnsupportedOperationException("not yet implemented");
                
        //ACT       

        //ASSERT
    }
}