package com.riftwebdesign.coldbug.tests.testclasses;

public class testabstract {
    public class booltester {
        private boolean _val = false;
        public boolean getVal(){
            return _val;
        }
        public void setVal(boolean v){
            _val = v;
        }
    }
    public class countertester {
        private Integer _val = 0;
        public Integer getVal(){
            return _val;
        }
        public void increment(){
            _val++;
        }
    }
}