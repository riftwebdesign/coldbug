package com.riftwebdesign.coldbug.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.riftwebdesign.coldbug.ColdBug;
import com.riftwebdesign.coldbug.engines.factory;
import com.riftwebdesign.coldbug.handlers.browserhandler;
import com.riftwebdesign.coldbug.handlers.heartbeathandler;
import com.riftwebdesign.coldbug.handlers.routehandler;
import com.riftwebdesign.coldbug.handlers.systemtrayhandler;
import com.riftwebdesign.coldbug.tests.testclasses.testabstract;

import org.junit.jupiter.api.Test;

public class ColdBugTest extends testabstract {
    @Test
    public void start_test() throws Exception {
        //ARRANGE
        ColdBug cut = new ColdBug();
        final countertester flag = new countertester();
        factory factory = new factory(){
            @Override
            public systemtrayhandler createsystemtray() {
                systemtrayhandler s = new systemtrayhandler(){
                    @Override
                    public void addTrayIcon() {
                        flag.increment();
                    }
                };
                return s;
            }
            @Override
            public browserhandler createbrowserhandler() {
                browserhandler s = new browserhandler() {
                    @Override
                    public void openbrowser() {
                        flag.increment();
                    }
                };
                return s;
            }
            @Override
            public routehandler createroutehandler() {
                routehandler s = new routehandler() {
                    @Override
                    public void register() {
                        flag.increment();
                    }
                };
                return s;
            }
            @Override
            public heartbeathandler createheartbeathandler() {
                heartbeathandler s = new heartbeathandler() {
                    @Override
                    public void monitor() {
                        flag.increment();
                    }
                };
                return s;
            }
        };
        cut.factory = factory;
        
        //ACT
        cut.start();

        //ASSERT
        assertEquals(4, flag.getVal());

    }
    @Test
    public void main_startiscalled_test() throws Exception {
        //ARRANGE
        ColdBug cut = new ColdBug();
        final booltester flag = new booltester();
        factory factory = new factory(){
            @Override
            public ColdBug createcoldbug() {
                ColdBug s = new ColdBug(){
                    @Override
                    public void start() {
                        flag.setVal(true);
                    }
                };
                return s;
            }
        };
        factory.setinstance( factory ); //sets the static context for the factory
        cut.factory = factory;
        String[] s = new String[]{};
        
        //ACT
        ColdBug.main(s);

        //reset static instance
        factory.setinstance( new factory() );

        //ASSERT
        assertEquals(true, flag.getVal());
    }    
}