==SETUP==
--use pom and run "clean" to install dependencies
--use node to install packages
--run the debugger for "Coldfusion"

--make sure to run the typescript watch compile task while developing, the typescript compiles into a single bridge.js file
--java project files -> src/main/java/com/riftwebdesign/coldbug/
--typescript project files -> src/main/resources

--prod.jks is a javakey store for running the pom build. the program should compile and debug locally just fine from VS Code with the correct java extensions installed.
the webstart files have not been included